import math
import numpy as np 
def main():
  #clear the file
  theFile = open('trace.tr','w')
  theFile.write("#Starting trace\n")
  theFile.close()
  
  #open the file to append
  theFile = open('trace.tr','a')
  
  #write the starting statements
  theFile.write('#send:  s  rst=0    src   data\n')
  theFile.write('0001____1__0000110__0000__00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000\n')
  theFile.write('#send:  s  en=1     src   data\n')
  theFile.write('0001____1__0000010__0000__00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000\n')
  
  
  np.random.seed(0)
  a = np.random.randint(2**32, size=1024)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, a, theFile)
  
  a = np.random.randint(2**32, size=1024)
  b = np.random.randint(2**32, size=1024)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=1024)
  b = np.random.randint(2**32, size=1024)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=2)
  b = np.random.randint(2**32, size=2)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=4)
  b = np.random.randint(2**32, size=4)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=8)
  b = np.random.randint(2**32, size=8)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=16)
  b = np.random.randint(2**32, size=16)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=32)
  b = np.random.randint(2**32, size=32)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=64)
  b = np.random.randint(2**32, size=64)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=128)
  b = np.random.randint(2**32, size=128)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=256)
  b = np.random.randint(2**32, size=256)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=512)
  b = np.random.randint(2**32, size=512)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)

  a = np.random.randint(2**32, size=1024)
  b = np.random.randint(2**32, size=1024)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=512)
  b = np.random.randint(2**32, size=512)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=256)
  b = np.random.randint(2**32, size=256)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=128)
  b = np.random.randint(2**32, size=128)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=64)
  b = np.random.randint(2**32, size=64)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=32)
  b = np.random.randint(2**32, size=32)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=16)
  b = np.random.randint(2**32, size=16)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=8)
  b = np.random.randint(2**32, size=8)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  a = np.random.randint(2**32, size=4)
  b = np.random.randint(2**32, size=4)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)

  a = np.random.randint(2**32, size=2)
  b = np.random.randint(2**32, size=2)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  listSorted = []
  for i in range (0, 1024):
    listSorted.append(i)
  
  traceCreation(listSorted, listSorted, theFile)
  
  listSorted1 = []
  for i in range (0, 1024):
    listSorted1.append(1024 - i)
  
  traceCreation(listSorted1, listSorted1, theFile)

  #a = np.random.randint(2**32, size=7)
  #b = np.random.randint(2**32, size=7)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation([7, 6, 2, 4, 3, 6, 1], [7, 6, 2, 4, 3, 6, 1], theFile)

  a = np.random.randint(2**32, size=22)
  b = np.random.randint(2**32, size=22)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)

  #some random list lengths
  a = np.random.randint(2**32, size=170)
  b = np.random.randint(2**32, size=170)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)

  a = np.random.randint(2**32, size=904)
  b = np.random.randint(2**32, size=904)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)

  a = np.random.randint(2**32, size=716)
  b = np.random.randint(2**32, size=716)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)

  a = np.random.randint(2**32, size=581)
  b = np.random.randint(2**32, size=581)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)

  a = np.random.randint(2**32, size=135)
  b = np.random.randint(2**32, size=135)
  #the test cases: expected output key[0,2,4,6,8,9] data[0,5,2,1,3,4]
  traceCreation(a, b, theFile)
  
  #write the closing statements
  theFile.write('#done' + '\n')
  theFile.write('0011____0__0000000__0000__00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000' + '\n')
  theFile.close()

def traceCreation(keyList, dataList, theFile):
   #get the length of the list
   listLen = len(dataList)
   
   #loop through the lists creating the trace lines
   mydict = {}
   
   #add the length of the list and the size of the sorted lists to the start
   theFile.write("#Send length\n")
   zeros = '00000000000000000000000000000000'
   lengthInBinary = "{0:b}".format(listLen)
   logInBinary = "{0:b}".format(int(math.ceil(np.log2(listLen))))
     
   #merge the string
   lB1 = zeros + lengthInBinary
   mS1 = zeros + logInBinary
   numLB = len(lB1)
   numMS = len(mS1)
     
   #strip them to the correct length
   numLB1 = numLB - 32
   numMS1 = numMS - 32
   lB = lB1[numLB1:]
   mS = mS1[numMS1:]
     
   #combine them
   lBandMS = mS + '_' + lB
   
   #form the full string and write
   totStringStart = "0001_0_0000000__0000_" + lBandMS
   theFile.write(totStringStart + '\n')
   
   for i in range (0, listLen):
     theFile.write("#Send\n")
     currentKey = keyList[i]
     currentData = dataList[i]
     
     #fill the dictionary
     mydict[currentKey] = currentData
     
     #convert the values to binary
     zeroString = '00000000000000000000000000000000'
     cKS0 = "{0:b}".format(currentKey)
     cDS0 = "{0:b}".format(currentData)
     
     #merge the string
     cKS1 = zeroString + cKS0
     cDS1 = zeroString + cDS0
     numCKS = len(cKS1)
     numCDS = len(cDS1)
     
     #strip them to the correct length
     numCKS1 = numCKS - 32
     numCDS1 = numCDS - 32
     cKS = cKS1[numCKS1:]
     cDS = cDS1[numCDS1:]
     
     #combine them
     cDandKS = cKS + '_' + cDS

     #if this is the end of the list add the done signal otherwise don't
     totString = "0001_0_0000000__0001_" + cDandKS
       
     #append the line
     theFile.write(totString + '\n')
       
   sortedKey, sortedData = mergeSorter(keyList, dataList)

   for i in range (0, listLen):
     theFile.write("#Receive\n")
     currentKey = sortedKey[i]
     currentData = sortedData[i]
     
     #fill the dictionary
     #mydict[currentKey] = currentData
     
     #convert the values to binary
     zeroString = '00000000000000000000000000000000'
     cKS0 = "{0:b}".format(currentKey)
     cDS0 = "{0:b}".format(currentData)
     
     #merge the string
     cKS1 = zeroString + cKS0
     cDS1 = zeroString + cDS0
     numCKS = len(cKS1)
     numCDS = len(cDS1)
     
     #strip them to the correct length
     numCKS1 = numCKS - 32
     numCDS1 = numCDS - 32
     cKS = cKS1[numCKS1:]
     cDS = cDS1[numCDS1:]
     
     #combine them
     cDandKS = cKS + '_' + cDS

     #if this is the end of the list add the done signal otherwise don't
     totString = "0010_0_0000000__0001_" + cDandKS +'\n'
       
     #append the line
     theFile.write(totString)
     #theFile.write('test\n')
   
   #theFile.close()   


#21212abe21212abe
#655


#send:
#0001____0__0000000__0000__00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000

#receive:
#0010____0__0000000__0000__00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000001

def mergeSorter(keyList, dataList):
  #create a list of random variables and a list of keys
  lengthOfList = len(keyList)
  
  #merge sort
  if lengthOfList == 1:
    return keyList, dataList
  else:
    #split the list and call merge sort of the halves
    frontKeyList, frontDataList = mergeSorter(keyList[0:math.floor(lengthOfList/2)], dataList[0:math.floor(lengthOfList/2)])
    backKeyList, backDataList = mergeSorter(keyList[math.floor(lengthOfList/2):], dataList[math.floor(lengthOfList/2):])
    
    #do the merge
    mergedKeyList = []
    mergedDataList = []
    pointerFront = 0
    pointerBack = 0
    while pointerFront < len(frontKeyList) and pointerBack < len(backKeyList):
      #check which value of the pointers is larger
      if frontKeyList[pointerFront] < backKeyList[pointerBack]:
        #if the front list value is smaller add it to the merged list and update its pointer one forward
        mergedKeyList.append(frontKeyList[pointerFront])
        mergedDataList.append(frontDataList[pointerFront])
        pointerFront += 1
      else:
        #if the back list value is smaller add it to the merged list and update its pointer one forward
        mergedKeyList.append(backKeyList[pointerBack])
        mergedDataList.append(backDataList[pointerBack])
        pointerBack += 1
        
    #append the list that is not finished
    while pointerFront < len(frontKeyList):
      mergedKeyList.append(frontKeyList[pointerFront])
      mergedDataList.append(frontDataList[pointerFront])
      pointerFront += 1
    while pointerBack < len(backKeyList):
      mergedKeyList.append(backKeyList[pointerBack])
      mergedDataList.append(backDataList[pointerBack])
      pointerBack += 1
      
    #return the joined lists
    return mergedKeyList, mergedDataList
  
  
if __name__=='__main__':
    main()   
