//addr_size_p is based on memory length
module FSB_control #(parameter addr_size_p = -1, ring_width_p = -1)
(input  clk_i
,input reset_i

,input finish_i

,input [ring_width_p - 1:0] input_data_i
,input v_i,
,output ready_o

,output                    v_o
,output [ring_width_p-1:0] data_o
,input                     yumi_i

,output list_done_o

,output wr_en1_o,
,output wr_en2_o,
,output [addr_size_p - 1:0] wr_addr1_o,
,output [addr_size_p - 1:0] wr_addr2_o
);

  //some logic
  logic start;
  logic [addr_size_p - 1:0] length_of_list_r, counter_data_in_r;
  
  //on the start get the length of the list
  bsg_mux #(.width_p(addr_size_p), .els_p(2)) output_mux
  (.data_i({input_data_i[addr_size_p - 1:0], length_of_list_r})
  ,.sel_i(start)
  ,.data_o(length_of_list_r));
  
  logic reset_counter_r, list_done_r;
  bsg_counter_en_overflow #(.width_p(addr_size_p))

            (.clk_i(clk_i)
            ,.reset_i(reset_i | reset_counter_r)

            ,.en_i(v_i & (~list_done_r) & (~start))
            ,.limit_i(length_of_list_r)
            ,counter_o(counter_data_in_r)
            ,.overflowed_o(list_done_r)
            );
  
  //buffered list done to get final stuff through
  always_ff @ (posedge clk_i) begin
    list_done_o <= list_done_r;
  end
  
  
  //two bit counter
  logic two_count_r;
  bsg_counter_en_overflow #(.width_p(1))

            (.clk_i(clk_i)
            ,.reset_i(reset_i | reset_counter_r)

            ,.en_i(v_i & (~list_done_o))
            ,.limit_i(1'b1)
            ,counter_o(two_count_r)
            ,.overflowed_o()
  );
  
  bsg_mux #(.width_p(1), .els_p(2)) output_mux
  (.data_i({input_data_i[addr_size_p - 1:0], length_of_list_r})
  ,.sel_i(start)
  ,.data_o(length_of_list_r));
  
  
  logic [65:0] data1_r, data2_r;
  
  //loop in pieces of data in twos
  bsg_mux #(.width_p(66), .els_p(2)) output_mux
  (.data_i({input_data_i[65:0], data1_r})
  ,.sel_i(~two_count_r)
  ,.data_o(data1_r));
  
  bsg_mux #(.width_p(66), .els_p(2)) output_mux
  (.data_i({input_data_i[65:0], data2_r})
  ,.sel_i(two_count_r)
  ,.data_o(data2_r));
  
  //comparator and swapper
  logic [65:0] data1_swapped_r, data2_swapped_r;
  bsg_compare_and_swap #(.width_p(66)) compareAndSwapper
    (.data_i({data2_r, data1_r})
    ,.swap_on_equal_i(1'b0)
    ,.data_o({data2_swapped_r, data1_swapped_r})
    ,.swapped_o()
    );
	
  //determine where and when data should be written into memory
  logic wr_en1, wr_en2;
  wire [addr_size_p - 1:0] wr_addr1 = counter_data_in_r - 2
  wire [addr_size_p - 1:0] wr_addr2 = counter_data_in_r - 1;
  assign wr_en1 = (v_i & (~list_done_o)) & ~two_count_r;
  assign wr_en2 = (v_i & (~list_done_o)) & two_count_r;
  
  //control logic state machine
  typedef enum[2:0] {eWaitingForStart, eLength, eGettingData, eCompute} state_e;

  state_e state_r, state_n;

  //update the state on the clock edge
  always_ff @(posedge clk_i) begin
    state_r <= reset_i ? eWaiting : state_n;
  end
  //depending on the current state and control logic decide what the next state is
  always_comb begin
    unique case (state_r)
      eWaitingForStart: state_n = v_i ? eLength : eWaitingForStart;
      eLength: state_n = eGettingData;
      eGettingData: state_n = list_done_o ? eCompute: eGettingData;
	  eCompute: state_n = eCompute;
    endcase
  end

  logic v_oBetween, ready_oBetween;
  //based on the current state set the control logic
  always_comb begin
    unique case (state_r)
      eWaiting: begin
        v_oBetween = 0;
        ready_oBetween = 1;
		start = 0;
      end eLength: begin
        v_oBetween = 0;
        ready_oBetween = 0;
		start = 1
      end eGettingData: begin
        v_oBetween = 0;
        ready_oBetween = 0;
		start = 0
      end eCompute: begin
        v_oBetween = 0;
        ready_oBetween = 0;
		start = 0
      end
    endcase
  end
  
  assign wr_en1_o = wr_en1;
  assign wr_en2_o = wr_en2;
  assign wr_addr1_o = wr_addr1;
  assign wr_addr2_o = wr_addr2;

endmodule