//addr_size_p is based on memory length
//This module manages input and output of the FSB. It starts in a waiting state until it gets valid data. The first piece of data will contain the size of the list. It will then slowly get the list and put it into memory. When the full list is received it will pulse the system to start computing and will wait until computation is done. It will then output the sorted list and go back to waiting. 
`include "bsg_defines.v"
module FSB_control_simple_parallelized #(parameter addr_size_p = -1, ring_width_p = -1, e_height_p = -1, e_height_lp = $clog2(e_height_p))
(input  clk_i
,input reset_i

,input finish_i

,input [ring_width_p - 1:0] input_data_i //I should add a buffer to the input data
,input v_i
,output ready_o

,output                    v_o
,output [ring_width_p-1:0] data_o
,input                     yumi_i

,output list_done_o
,output FSB_reset_o

,output [e_height_p - 1:0] wr_en_o
//,output [addr_size_p - 1:0] wr_addr_o
,output [65:0] wr_data_o

,input [e_height_p - 1:0][65:0] read_data_i
//,output [addr_size_p - 1:0] rd_addr_o
,output [e_height_p - 1:0] rd_en_o

,output [31:0] length_of_list_o
,output [31:0] max_sorted_o
);

  //some logic
  logic start;
  logic [addr_size_p - 1:0] counter_data_in_r, counter_data_going_out_r;
  logic [31:0] length_of_list_r, max_sorted_r;
  
  logic [ring_width_p - 1:0] input_data_r, input_data_r_r;
  logic reset_counter, list_done_r;
  //buffer the input data one cycle
  always_ff @(posedge clk_i) begin
    input_data_r <= input_data_i;

  end

  //always_ff @(posedge clk_i) begin
    //input_data_r_r = input_data_r;
  //end
  
  //on the start get the length of the list
  always_ff @(posedge clk_i) begin
    if (reset_i | reset_counter) begin
	  length_of_list_r <= 15; //some number that is not zero
	end
	else if (start) begin
	  length_of_list_r <= input_data_r[31:0];
	end
	else begin
	  length_of_list_r <= length_of_list_r;
	end
  end

  always_ff @(posedge clk_i) begin
    if (reset_i | reset_counter) begin
	  max_sorted_r <= 0; //some number that is not zero
	end
	else if (start) begin
	  max_sorted_r <= input_data_r[63:32];
	end
	else begin
	  max_sorted_r <= max_sorted_r;
	end
  end
  //bsg_mux #(.width_p(32), .els_p(2)) output_mux_length
  //(.data_i({input_data_r[31:0], length_of_list_r})
  //,.sel_i(start)
  //,.data_o(length_of_list_r));
  
  //bsg_mux #(.width_p(32), .els_p(2)) output_mux_sorted
  //(.data_i({input_data_r[63:32], max_sorted_r})
  //,.sel_i(start)
  //,.data_o(max_sorted_r));
  
  //counter for data coming in
  //bsg_counter_en_overflow #(.width_p(addr_size_p)) in_counter
  //(.clk_i(clk_i)
  //,.reset_i(reset_i | reset_counter)
  //,.en_i(v_i & (~list_done_r) & (~start))
  //,.limit_i(length_of_list_r)
  //,.counter_o(counter_data_in_r)
  //,.overflowed_o(list_done_r));
  
  assign list_done_r = (length_of_list_r == counter_data_in_r);
  
  always_ff @(posedge clk_i)
  begin
    if (reset_i | reset_counter) begin //reset case
      counter_data_in_r <= 0;
	end
	else if (v_i & (~list_done_r) & (~start)) begin //looping case
      counter_data_in_r <= counter_data_in_r + 1;
	end
	else begin //waiting case
	  counter_data_in_r <= counter_data_in_r;
	end
  end
  
  
  //buffered list done to get final stuff through
  //always_ff @ (posedge clk_i) begin
    //list_done_o <= list_done_r;
  //end
  
  
  //counter for data going out
  logic list_sent_r;
  assign list_sent_r = (length_of_list_r - 1 == counter_data_going_out_r);
  
  always_ff @(posedge clk_i)
  begin
    if (reset_i | reset_counter) begin //reset case
      counter_data_going_out_r <= 0;
	end
	else if (v_o & yumi_i & (~list_sent_r)) begin //looping case, had v_o here, need to work it back in
      counter_data_going_out_r <= counter_data_going_out_r + 1;
	end
	else begin //waiting case
	  counter_data_going_out_r <= counter_data_going_out_r;
	end
  end
  
  //control logic state machine
  typedef enum {eWaitingForStart, eLength, eWaitingForData, eGettingData, eStartCompute, eCompute, eWaitToSend, eFetchingSend, eSend} state_e; //deleted [2:0]

  state_e state_r, state_n;

  //update the state on the clock edge
  always_ff @(posedge clk_i) begin
    state_r <= reset_i ? eWaitingForStart : state_n;
  end
  //depending on the current state and control logic decide what the next state is
  always_comb begin
    unique case (state_r)
      eWaitingForStart: state_n = v_i ? eLength : eWaitingForStart;
      eLength: state_n = v_i ? eGettingData : eWaitingForData;
	  eWaitingForData: state_n = v_i ? eGettingData : eWaitingForData;
      eGettingData: state_n = list_done_r ? eStartCompute: v_i ? eGettingData : eWaitingForData;
	  eStartCompute: state_n = eCompute;
	  //eCompute: state_n = (finish_i & yumi_i) ? eSend : finish_i ? eWaitToSend : eCompute;
	  eCompute: state_n = finish_i ? eFetchingSend : eCompute;
	  //eWaitToSend: state_n = yumi_i ? eSend : eWaitToSend;
	  //eSend: state_n = list_sent_r ? eWaitingForStart : yumi_i ? eSend : eWaitToSend;
          eFetchingSend: state_n = eSend;
	  eSend: state_n = list_sent_r & yumi_i ? eWaitingForStart : yumi_i ? eFetchingSend : eSend;
    endcase
  end

  logic v_oBetween, ready_oBetween, wr_en, start_comp, rd_en;
  //based on the current state set the control logic
  always_comb begin
    unique case (state_r)
      eWaitingForStart: begin
        reset_counter = 1;
		start_comp = 0;
		v_oBetween = 0;
        ready_oBetween = 1;
		start = 0;
		wr_en = 0;
		rd_en = 0;
      end eLength: begin
        reset_counter = 0;
		start_comp = 0;
		v_oBetween = 0;
        ready_oBetween = 1;
		start = 1;
		wr_en = 0;
		rd_en = 0;
      end eWaitingForData: begin
        reset_counter = 0;
		start_comp = 0;
		v_oBetween = 0;
        ready_oBetween = 1;
		start = 0;
		wr_en = 0;
		rd_en = 0;
      end eGettingData: begin
        reset_counter = 0;
		start_comp = 0;
		v_oBetween = 0;
        ready_oBetween = 1;
		start = 0;
		wr_en = 1;
		rd_en = 0;
      end eStartCompute: begin
        reset_counter = 0;
		start_comp = 1;
		v_oBetween = 0;
        ready_oBetween = 0;
		start = 0;
		wr_en = 0;
		rd_en = 0;
      end eCompute: begin
        reset_counter = 0;
		start_comp = 0;
		v_oBetween = 0;
        ready_oBetween = 0;
		start = 0;
		wr_en = 0;
		rd_en = 0;
      end eFetchingSend: begin
        reset_counter = 0;
		start_comp = 0;
		v_oBetween = 0;
        ready_oBetween = 0;
		start = 0;
		wr_en = 0;
		rd_en = 1;
      end eSend: begin
        reset_counter = 0;
		start_comp = 0;
		v_oBetween = 1;
        ready_oBetween = 0;
		start = 0;
		wr_en = 0;
		rd_en = 0;
      end
    endcase
  end
  
  
  //count through the write FIFOS
  logic [e_height_lp - 1:0] which_fifo_to_write_counter_r, which_fifo_to_write;
  always_ff @(posedge clk_i)
    begin
      if (reset_i | reset_counter) begin //reset case
        which_fifo_to_write_counter_r <= 0;
    end
    else if (v_i & (~list_done_r) & (~start)) begin //looping case
        which_fifo_to_write_counter_r <= which_fifo_to_write_counter_r + 1;
    end
    else begin //waiting case
      which_fifo_to_write_counter_r <= which_fifo_to_write_counter_r;
    end
  end
  
  assign which_fifo_to_write = which_fifo_to_write_counter_r - 1;
  
  //assign the write enables based on the counter
  logic [e_height_p - 1:0] wr_ens;
  bsg_decode #(.num_out_p(e_height_p)) decoder_wr
  (.i(which_fifo_to_write)
  ,.o(wr_ens));
  
  /**always_comb begin
    for (i=0; i<e_height_p; i++) begin
      if ((which_fifo_to_write == i) & wr_en) begin
        wr_ens[i] = 1
      end
      else
        wr_ens[i] = 0
      end
    end
  end**/
  
  //count through the read fifos
  logic [e_height_lp - 1:0] which_fifo_to_read_counter_r;
  always_ff @(posedge clk_i)
  begin
    if (reset_i | reset_counter) begin //reset case
      which_fifo_to_read_counter_r <= 0;
	end
	else if (v_o & yumi_i & (~list_sent_r)) begin //looping case, had v_o here, need to work it back in
      which_fifo_to_read_counter_r <= which_fifo_to_read_counter_r + 1;
	end
	else begin //waiting case
	  which_fifo_to_read_counter_r <= which_fifo_to_read_counter_r;
	end
  end
  
  //assign the read enables based on the counter, can replace with decoders
  logic [e_height_p - 1:0] rd_ens;
  bsg_decode #(.num_out_p(e_height_p)) decoder_rd
  (.i(which_fifo_to_read_counter_r)
  ,.o(rd_ens));
  
  //keep a copy of the rd_ens
  logic [e_height_p - 1:0] rd_ens_r;
  always_ff @(posedge clk_i)
    rd_ens_r <= rd_ens;
    
  //select which fifo the read data from
  //USE A BSG MUX, select needs to be minus one though of which_fifo_to_read
  logic [65:0] read_data;
  bsg_mux #(.width_p(66), .els_p(e_height_p)) rd_mux
  (.data_i(read_data_i)
  ,.sel_i(which_fifo_to_read_counter_r)
  ,.data_o(read_data));
  
  //assign the unassigned outputs
  genvar i;
  for (i=0; i<e_height_p; i++) begin
    assign wr_en_o[i] = wr_ens[i] & wr_en;
    assign rd_en_o[i] = rd_ens[i] & rd_en;
  end
  
  assign wr_addr_o = counter_data_in_r - 1;
  assign list_done_o = start_comp;
  //assign rd_addr_o = counter_data_going_out_r;
  assign wr_data_o = input_data_r[65:0];
  assign ready_o = ready_oBetween & ~list_done_r;//added an and here, clips ready_o
  
  assign data_o = {11'b0, read_data};
  assign v_o = v_oBetween;
  //assign rd_en_o = rd_ens_r;
  assign max_sorted_o = max_sorted_r;
  assign length_of_list_o = length_of_list_r;
  assign FSB_reset_o = reset_counter;

endmodule

module FSB_control_simple_parallelized_testbench();
  //ring width is 75, addr_size_p = 8
  logic clk_i, reset_i, finish_i;
  logic [74:0] input_data_i;
  logic v_i, ready_o, v_o;
  logic [74:0] data_o;
  logic yumi_i, list_done_o,  FSB_reset_o;
  logic [1:0] wr_en_o;
  logic [7:0] wr_addr_o;
  logic [1:0][65:0] read_data_i;
  logic [65:0] wr_data_o;
  logic [7:0] rd_addr_o;
  logic [1:0] rd_en_o;
  logic [31:0] length_of_list_o, max_sorted_o;
  
  FSB_control_simple_parallelized #(.addr_size_p(8), .ring_width_p(75), .e_height_p(2)) DUT
  (.clk_i
  ,.reset_i
  ,.finish_i
  ,.input_data_i
  ,.v_i
  ,.ready_o
  ,.v_o
  ,.data_o
  ,.yumi_i
  ,.list_done_o
  ,.FSB_reset_o
  ,.wr_en_o
  //,.wr_addr_o
  ,.wr_data_o
  ,.read_data_i
  //,.rd_addr_o
  ,.rd_en_o
  ,.length_of_list_o
  ,.max_sorted_o);
  
  
  //the two fifos
  logic full[1:0], empty[1:0];
  bsg_fifo_1rw_large #(.width_p(66), .els_p(16)) fifo0
   (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.data_i(wr_data_o)
    ,.v_i(rd_en_o[0] | wr_en_o[0])
    ,.enq_not_deq_i(wr_en_o[0])
    ,.full_o(full[0])
    ,.empty_o(empty[0])
    ,.data_o(read_data_i[0])
    );
    
    bsg_fifo_1rw_large #(.width_p(66), .els_p(16)) fifo1
   (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.data_i(wr_data_o)
    ,.v_i(rd_en_o[1] | wr_en_o[1])
    ,.enq_not_deq_i(wr_en_o[1])
    ,.full_o(full[1])
    ,.empty_o(empty[1])
    ,.data_o(read_data_i[1])
    );
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  integer i, j;
	
  initial begin
	reset_i <= 1; finish_i <= 0;
	input_data_i <= 0;
	v_i <= 0;
	yumi_i <= 0;
	i <= 1;
	j <= 1;
									@(posedge clk_i);
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	                                @(posedge clk_i);
	v_i <= 1; input_data_i <= 16 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	@(posedge clk_i);
	repeat(16) begin 
	v_i <= 1; input_data_i <= input_data_i + i;@(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
    @(posedge clk_i);	end
	
	repeat(5) begin					@(posedge clk_i); end
	
	finish_i <= 1; @(posedge clk_i);
  finish_i <= 0;@(posedge clk_i);
	repeat(16) begin finish_i <= 0; yumi_i <= 1; @(posedge clk_i);
	yumi_i <= 0; @(posedge clk_i);
	@(posedge clk_i);end
	
	@(posedge clk_i);
	@(posedge clk_i);

	
				
		$stop(); // end the simulation
  end
  
endmodule
  
  
