# Create work library
vlib work

# Compile Verilog
#     All Verilog files that are part of this design should have
#     their own "vlog" line below.
vlog "./bsg_defines.v"
vlog "./FSB_control_simple_parallelized.v"
vlog "./bsg_counter_en_overflow.v"
vlog "./bsg_mux.v"
vlog "./bsg_circular_ptr.v"
vlog "./bsg_decode.v"
vlog "./bsg_fifo_1rw_large.v"
vlog "./bsg_mem_1rw_sync.v"
vlog "./bsg_mem_1rw_sync_synth.v"



# Call vsim to invoke simulator
#     Make sure the last item on the line is the name of the
#     testbench module you want to execute.
vsim -voptargs="+acc" -t 1ps -lib work FSB_control_simple_parallelized_testbench

# Source the wave do file
#     This should be the file that sets up the signal window for
#     the module you are testing.
do FSB_control_simple_parallelized_wave.do

# Set the window types
view wave
view structure
view signals

# Run the simulation
run -all

# End
