onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /FSB_control_simple_parallelized_testbench/clk_i
add wave -noupdate /FSB_control_simple_parallelized_testbench/reset_i
add wave -noupdate /FSB_control_simple_parallelized_testbench/finish_i
add wave -noupdate /FSB_control_simple_parallelized_testbench/input_data_i
add wave -noupdate /FSB_control_simple_parallelized_testbench/v_i
add wave -noupdate /FSB_control_simple_parallelized_testbench/ready_o
add wave -noupdate /FSB_control_simple_parallelized_testbench/v_o
add wave -noupdate -radix unsigned /FSB_control_simple_parallelized_testbench/data_o
add wave -noupdate /FSB_control_simple_parallelized_testbench/yumi_i
add wave -noupdate /FSB_control_simple_parallelized_testbench/list_done_o
add wave -noupdate /FSB_control_simple_parallelized_testbench/FSB_reset_o
add wave -noupdate /FSB_control_simple_parallelized_testbench/wr_en_o
add wave -noupdate -radix unsigned -childformat {{{/FSB_control_simple_parallelized_testbench/read_data_i[1]} -radix unsigned} {{/FSB_control_simple_parallelized_testbench/read_data_i[0]} -radix unsigned}} -expand -subitemconfig {{/FSB_control_simple_parallelized_testbench/read_data_i[1]} {-height 15 -radix unsigned} {/FSB_control_simple_parallelized_testbench/read_data_i[0]} {-height 15 -radix unsigned}} /FSB_control_simple_parallelized_testbench/read_data_i
add wave -noupdate -radix unsigned /FSB_control_simple_parallelized_testbench/wr_data_o
add wave -noupdate /FSB_control_simple_parallelized_testbench/rd_en_o
add wave -noupdate -radix unsigned /FSB_control_simple_parallelized_testbench/length_of_list_o
add wave -noupdate -radix unsigned /FSB_control_simple_parallelized_testbench/max_sorted_o
add wave -noupdate /FSB_control_simple_parallelized_testbench/fifo0/mem_1srw/synth/mem
add wave -noupdate /FSB_control_simple_parallelized_testbench/fifo1/mem_1srw/synth/mem
add wave -noupdate /FSB_control_simple_parallelized_testbench/full
add wave -noupdate /FSB_control_simple_parallelized_testbench/empty
add wave -noupdate /FSB_control_simple_parallelized_testbench/DUT/state_r
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {91673 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {83945 ps} {112951 ps}
