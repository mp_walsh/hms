onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /FSB_control_simple_testbench/clk_i
add wave -noupdate /FSB_control_simple_testbench/reset_i
add wave -noupdate /FSB_control_simple_testbench/finish_i
add wave -noupdate -radix unsigned /FSB_control_simple_testbench/input_data_i
add wave -noupdate /FSB_control_simple_testbench/v_i
add wave -noupdate /FSB_control_simple_testbench/ready_o
add wave -noupdate /FSB_control_simple_testbench/v_o
add wave -noupdate -radix unsigned /FSB_control_simple_testbench/data_o
add wave -noupdate /FSB_control_simple_testbench/yumi_i
add wave -noupdate /FSB_control_simple_testbench/list_done_o
add wave -noupdate /FSB_control_simple_testbench/wr_en_o
add wave -noupdate -radix unsigned /FSB_control_simple_testbench/wr_addr_o
add wave -noupdate -radix unsigned /FSB_control_simple_testbench/read_data_i
add wave -noupdate -radix unsigned /FSB_control_simple_testbench/wr_data_o
add wave -noupdate -radix unsigned /FSB_control_simple_testbench/rd_addr_o
add wave -noupdate /FSB_control_simple_testbench/rd_en_o
add wave -noupdate /FSB_control_simple_testbench/i
add wave -noupdate /FSB_control_simple_testbench/j
add wave -noupdate /FSB_control_simple_testbench/DUT/state_r
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {106495 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {95516 ps} {109758 ps}
