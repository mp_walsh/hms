library verilog;
use verilog.vl_types.all;
entity FSB_control is
    generic(
        addr_size_p     : integer := -1;
        ring_width_p    : integer := -1
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        finish_i        : in     vl_logic;
        input_data_i    : in     vl_logic_vector;
        v_i             : in     vl_logic;
        ready_o         : out    vl_logic;
        v_o             : out    vl_logic;
        data_o          : out    vl_logic_vector;
        yumi_i          : in     vl_logic;
        list_done_o     : out    vl_logic;
        wr_en_o         : out    vl_logic;
        wr_addr_o       : out    vl_logic_vector;
        wr_data_o       : out    vl_logic_vector(65 downto 0);
        read_data_i     : in     vl_logic_vector(65 downto 0);
        rd_addr_o       : out    vl_logic_vector;
        rd_en_o         : out    vl_logic;
        length_of_list_o: out    vl_logic_vector(31 downto 0);
        max_sorted_o    : out    vl_logic_vector(31 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
    attribute mti_svvh_generic_type of ring_width_p : constant is 1;
end FSB_control;
