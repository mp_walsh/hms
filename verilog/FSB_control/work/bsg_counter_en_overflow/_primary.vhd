library verilog;
use verilog.vl_types.all;
entity bsg_counter_en_overflow is
    generic(
        width_p         : integer := -1
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        en_i            : in     vl_logic;
        limit_i         : in     vl_logic_vector;
        counter_o       : out    vl_logic_vector;
        overflowed_o    : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of width_p : constant is 1;
end bsg_counter_en_overflow;
