onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /data_mover_from_tree_to_memory_testbench/clk_i
add wave -noupdate /data_mover_from_tree_to_memory_testbench/reset_i
add wave -noupdate -radix unsigned -childformat {{{/data_mover_from_tree_to_memory_testbench/data_i[1]} -radix unsigned} {{/data_mover_from_tree_to_memory_testbench/data_i[0]} -radix unsigned}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_testbench/data_i[1]} {-height 15 -radix unsigned} {/data_mover_from_tree_to_memory_testbench/data_i[0]} {-height 15 -radix unsigned}} /data_mover_from_tree_to_memory_testbench/data_i
add wave -noupdate -radix unsigned -childformat {{{/data_mover_from_tree_to_memory_testbench/wr_en_o[1]} -radix unsigned} {{/data_mover_from_tree_to_memory_testbench/wr_en_o[0]} -radix unsigned}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_testbench/wr_en_o[1]} {-height 15 -radix unsigned} {/data_mover_from_tree_to_memory_testbench/wr_en_o[0]} {-height 15 -radix unsigned}} /data_mover_from_tree_to_memory_testbench/wr_en_o
add wave -noupdate -radix unsigned -childformat {{{/data_mover_from_tree_to_memory_testbench/data_o[1]} -radix unsigned} {{/data_mover_from_tree_to_memory_testbench/data_o[0]} -radix unsigned}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_testbench/data_o[1]} {-height 15 -radix unsigned} {/data_mover_from_tree_to_memory_testbench/data_o[0]} {-height 15 -radix unsigned}} /data_mover_from_tree_to_memory_testbench/data_o
add wave -noupdate -radix unsigned -childformat {{{/data_mover_from_tree_to_memory_testbench/wr_addr_o[1]} -radix unsigned} {{/data_mover_from_tree_to_memory_testbench/wr_addr_o[0]} -radix unsigned}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_testbench/wr_addr_o[1]} {-radix unsigned} {/data_mover_from_tree_to_memory_testbench/wr_addr_o[0]} {-radix unsigned}} /data_mover_from_tree_to_memory_testbench/wr_addr_o
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_testbench/finish_o
add wave -noupdate /data_mover_from_tree_to_memory_testbench/i
add wave -noupdate /data_mover_from_tree_to_memory_testbench/DUT/finish
add wave -noupdate /data_mover_from_tree_to_memory_testbench/start_i
add wave -noupdate /data_mover_from_tree_to_memory_testbench/where_to_start_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {8072 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {3117 ps} {29139 ps}
