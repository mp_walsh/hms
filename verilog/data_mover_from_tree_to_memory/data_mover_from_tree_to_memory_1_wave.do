onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/clk_i
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/reset_i
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/start_i
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/current_length_i
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/where_to_start_i
add wave -noupdate -childformat {{{/data_mover_from_tree_to_memory_1_testbench/data_i[1]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_1_testbench/data_i[0]} -radix hexadecimal}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_1_testbench/data_i[1]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_1_testbench/data_i[0]} {-radix hexadecimal}} /data_mover_from_tree_to_memory_1_testbench/data_i
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/wr_en_o
add wave -noupdate -radix hexadecimal /data_mover_from_tree_to_memory_1_testbench/data_o
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_1_testbench/wr_addr_o
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/finish_o
add wave -noupdate /data_mover_from_tree_to_memory_1_testbench/pause_the_tree_o
add wave -noupdate -radix hexadecimal /data_mover_from_tree_to_memory_1_testbench/DUT/data_received
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {17066 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 189
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {13992 ps} {24195 ps}
