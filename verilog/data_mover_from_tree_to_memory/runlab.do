# Create work library
vlib work

# Compile Verilog
#     All Verilog files that are part of this design should have
#     their own "vlog" line below.
vlog "./bsg_parallel_in_serial_out.v"
vlog "./bsg_mux.v"
vlog "./data_mover_from_tree_to_memory_parallelized.v"


# Call vsim to invoke simulator
#     Make sure the last item on the line is the name of the
#     testbench module you want to execute.
vsim -voptargs="+acc" -t 1ps -lib work data_mover_from_tree_to_memory_parallelized_testbench

# Source the wave do file
#     This should be the file that sets up the signal window for
#     the module you are testing.
do data_mover_from_tree_to_memory_parallelized_wave.do

# Set the window types
view wave
view structure
view signals

# Run the simulation
run -all

# End
