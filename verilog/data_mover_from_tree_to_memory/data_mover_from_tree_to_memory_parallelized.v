//This module moves data from the tree to the memory. It gets a start ,where to start, and length and uses that to determine how much valid data it needs to get out of the tree and where in memory it should start putting it. It does not put invalid or finishes into the memory. It also pulses a finish signal when it has gotten the full list. 
module data_mover_from_tree_to_memory_parallelized #(parameter addr_size_p = -1, e_height_p = 8, e_height_lp = $clog2(e_height_p))
(input  clk_i
,input reset_i

,input start_i
,input [addr_size_p - 1:0] where_to_start_i

,input [addr_size_p - 1:0] current_length_i
,input [addr_size_p - 1:0] full_length_i

,input [e_height_p - 1:0][65:0] data_i

,output [e_height_p - 1:0] wr_en_o
,output [e_height_p - 1:0][65:0] data_o
//,output [e_height_p - 1:0][addr_size_p - 1:0] wr_addr_o
,output finish_o
);
  //the counter logic
  //logic list_done_r;
  //logic [e_height_p - 1:0] starts_r;
  //logic [addr_size_p - 1:0] times_to_count_r;
  //logic [addr_size_p - 1:0] add_value;
  logic [addr_size_p - 1:0] counter_r;
  
  logic [addr_size_p - 1:0] num_of_valids;
  logic [e_height_p - 1:0][65:0] data;
  
  //add up the number of valid inputs
  assign num_of_valids = (data_i[0][64] & (~data_i[0][65])) + (data_i[1][64] & (~data_i[1][65])) + (data_i[2][64] & (~data_i[2][65])) + (data_i[3][64] & (~data_i[3][65])) + (data_i[4][64] & (~data_i[4][65])) + (data_i[5][64] & (~data_i[5][65])) + (data_i[6][64] & (~data_i[6][65])) + (data_i[7][64] & (~data_i[7][65]));
  
  //build of valid chains
  logic [e_height_p - 1:0] valids;
  assign valids[0] = (data_i[0][64] & (~data_i[0][65]));
  assign valids[1] = (data_i[1][64] & (~data_i[1][65]));
  assign valids[2] = (data_i[2][64] & (~data_i[2][65])) ;
  assign valids[3] = (data_i[3][64] & (~data_i[3][65]));
  assign valids[4] = (data_i[4][64] & (~data_i[4][65]));
  assign valids[5] = (data_i[5][64] & (~data_i[5][65]));
  assign valids[6] = (data_i[6][64] & (~data_i[6][65]));
  assign valids[7] = (data_i[7][64] & (~data_i[7][65]));
  
  //get valid values
  logic [e_height_p - 1:0][addr_size_p - 1:0] validNums;
  assign validNums[0] = valids[0];
  assign validNums[1] = valids[0] + valids[1];
  assign validNums[2] = valids[0] + valids[1] + valids[2];
  assign validNums[3] = valids[0] + valids[1] + valids[2] + valids[3];
  assign validNums[4] = valids[0] + valids[1] + valids[2] + valids[3] + valids[4];
  assign validNums[5] = valids[0] + valids[1] + valids[2] + valids[3] + valids[4] + valids[5];
  assign validNums[6] = valids[0] + valids[1] + valids[2] + valids[3] + valids[4] + valids[5] + valids[6];
  assign validNums[7] = valids[0] + valids[1] + valids[2] + valids[3] + valids[4] + valids[5] + valids[6] + valids[7];
  
  //depending on what data is valid change what gets written to memory
  integer i;
  always_comb begin
    for (i = 0; i < e_height_p; i = i + 1) begin
      if (validNums[0] == i + 1) begin
        data[i] = data_i[0];
      end
      else if (validNums[1] == i + 1) begin
        data[i] = data_i[1];
      end
      else if (validNums[2] == i + 1) begin
        data[i] = data_i[2];
      end
      else if (validNums[3] == i + 1) begin
        data[i] = data_i[3];
      end
      else if (validNums[4] == i + 1) begin
        data[i] = data_i[4];
      end
      else if (validNums[5] == i + 1) begin
        data[i] = data_i[5];
      end
      else if (validNums[6] == i + 1) begin
        data[i] = data_i[6];
      end
      else begin
        data[i] = data_i[7];
      end
    end
  end
  
  
  //assign data[e_height_p - 1][65:0] = data_i[e_height_p - 1][65:0];
  logic [e_height_p - 1:0] wr_ens;
  assign wr_ens[0] = (num_of_valids >= 1);
  assign wr_ens[1] = (num_of_valids >= 2);
  assign wr_ens[2] = (num_of_valids >= 3);
  assign wr_ens[3] = (num_of_valids >= 4);
  assign wr_ens[4] = (num_of_valids >= 5);
  assign wr_ens[5] = (num_of_valids >= 6);
  assign wr_ens[6] = (num_of_valids >= 7);
  assign wr_ens[7] = (num_of_valids >= 8);
  
  //values to use for the adder
  //need to update e_height parameter later, make a clog variable for here
  wire finish = (current_length_i + where_to_start_i == counter_r);
  wire full_finish = (counter_r == full_length_i);
   
  //the adder to update counter
  wire [addr_size_p - 1:0] next_point = counter_r + num_of_valids;
  
  //the counter
  always_ff @(posedge clk_i)
  begin
    if (reset_i) begin //reset case
      counter_r <= 0;
    end
    else if (start_i) begin//start case
      counter_r <= where_to_start_i;
    end
    else if (~finish) begin //looping case
        counter_r <= next_point;
    end
    else begin //waiting case
      counter_r <= counter_r;
    end
  end
  
  //the counter for which fifo to select
  logic [e_height_lp - 1:0] fifoCounter;
  wire [addr_size_p - 1:0] next_fifo_point = fifoCounter + num_of_valids;
  always_ff @(posedge clk_i)
  begin
    if (reset_i) begin //reset case
      fifoCounter <= 0;
    end
    else if (start_i & full_finish) begin//start case
      fifoCounter <= 0;
    end
    else if (~finish) begin //looping case
        fifoCounter <= next_fifo_point;
    end
    else begin //waiting case
      fifoCounter <= fifoCounter;
    end
  end
  
  //piles of muxes to direct output
  bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux0
  (.data_i({data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[0]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[0]));
  
   bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux1
  (.data_i({data[2], data[3], data[4], data[5], data[6], data[7], data[0], data[1]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[1]));
  
   bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux2
  (.data_i({data[3], data[4], data[5], data[6], data[7], data[0], data[1], data[2]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[2]));
  
   bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux3
  (.data_i({data[4], data[5], data[6], data[7], data[0], data[1], data[2], data[3]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[3]));
  
   bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux4
  (.data_i({data[5], data[6], data[7], data[0], data[1], data[2], data[3], data[4]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[4]));
  
   bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux5
  (.data_i({data[6], data[7], data[0], data[1], data[2], data[3], data[4], data[5]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[5]));
  
   bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux6
  (.data_i({data[7], data[0], data[1], data[2], data[3], data[4], data[5], data[6]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[6]));
  
   bsg_mux #(.width_p(66), .els_p(e_height_p)) out_mux7
  (.data_i({data[0], data[1], data[2], data[3], data[2], data[5], data[6], data[7]})
  ,.sel_i(fifoCounter)
  ,.data_o(data_o[7]));
  
  //wr_en
  bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en0
  (.data_i({wr_ens[1], wr_ens[2], wr_ens[3], wr_ens[4], wr_ens[5], wr_ens[6], wr_ens[7], wr_ens[0]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[0]));
  
   bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en1
  (.data_i({wr_ens[2], wr_ens[3], wr_ens[4], wr_ens[5], wr_ens[6], wr_ens[7], wr_ens[0], wr_ens[1]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[1]));
  
   bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en2
  (.data_i({wr_ens[3], wr_ens[4], wr_ens[5], wr_ens[6], wr_ens[7], wr_ens[0], wr_ens[1], wr_ens[2]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[2]));
  
   bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en3
  (.data_i({wr_ens[4], wr_ens[5], wr_ens[6], wr_ens[7], wr_ens[0], wr_ens[1], wr_ens[2], wr_ens[3]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[3]));
  
   bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en4
  (.data_i({wr_ens[5], wr_ens[6], wr_ens[7], wr_ens[0], wr_ens[1], wr_ens[2], wr_ens[3], wr_ens[4]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[4]));
  
   bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en5
  (.data_i({wr_ens[6], wr_ens[7], wr_ens[0], wr_ens[1], wr_ens[2], wr_ens[3], wr_ens[4], wr_ens[5]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[5]));
  
   bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en6
  (.data_i({wr_ens[7], wr_ens[0], wr_ens[1], wr_ens[2], wr_ens[3], wr_ens[4], wr_ens[5], wr_ens[6]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[6]));
  
   bsg_mux #(.width_p(1), .els_p(e_height_p)) out_mux_en7
  (.data_i({wr_ens[0], wr_ens[1], wr_ens[2], wr_ens[3], wr_ens[4], wr_ens[5], wr_ens[6], wr_ens[7]})
  ,.sel_i(fifoCounter)
  ,.data_o(wr_en_o[7]));
  
  
  
  
  //assign wr_addr_o[0][addr_size_p - 1:0] = counter_r;
  //assign wr_addr_o[1][addr_size_p - 1:0] = counter_r + 1;
  //assign wr_en_o[0] = (num_of_valids >= 1);
  //assign wr_en_o[1] = (num_of_valids >= 2);
  assign finish_o = finish;
  //assign data_o = data;

endmodule

module data_mover_from_tree_to_memory_parallelized_testbench();
  //addr size = 8, e height = 2
  logic clk_i, reset_i, start_i;
  logic [7:0] current_length_i, where_to_start_i, full_length_i;
  logic [7:0][65:0] data_i;
  logic [7:0] wr_en_o;
  logic [7:0][65:0] data_o;
  //logic [1:0][7:0] wr_addr_o;
  logic finish_o;
  
  data_mover_from_tree_to_memory_parallelized #(.addr_size_p(8), .e_height_p(8)) DUT
  (.clk_i
  ,.reset_i
  ,.start_i
  ,.where_to_start_i
  ,.current_length_i
  ,.full_length_i
  ,.data_i
  ,.wr_en_o
  ,.data_o
  //,.wr_addr_o
  ,.finish_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  integer i;
	
  initial begin
	reset_i <= 1; start_i <= 0;
	current_length_i <= 32;
  full_length_i <= 64;
	data_i[0][65:0] <= 0;
	data_i[1][65:0] <= 0;
	i <= 0;
									@(posedge clk_i);
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
  data_i[0][65:0] <= 66'h00000000000000000;
	data_i[1][65:0] <= 66'h00000000000000000;
  data_i[2][65:0] <= 66'h00000000000000000;
	data_i[3][65:0] <= 66'h00000000000000000;    
  data_i[4][65:0] <= 66'h00000000000000000;
	data_i[5][65:0] <= 66'h00000000000000000;    
  data_i[6][65:0] <= 66'h00000000000000000;
	data_i[7][65:0] <= 66'h00000000000000000;    
  @(posedge clk_i);
	where_to_start_i <= 0; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
	repeat(2) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 7;    
	i <= i+8;@(posedge clk_i); end
  
  data_i[0][65:0] <= 66'h00000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i;
  data_i[2][65:0] <= 66'h00000000000000000 + i;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 1;    
  data_i[4][65:0] <= 66'h00000000000000000 + i;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h00000000000000000 + i;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
  data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h00000000000000000 + i;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 1;
	data_i[3][65:0] <= 66'h00000000000000000 + i;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[5][65:0] <= 66'h00000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 3;
	data_i[7][65:0] <= 66'h00000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
	data_i[0][65:0] <= 66'h10000000000000000 + i;
  data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h30000000000000000;
  data_i[1][65:0] <= 66'h30000000000000000;
  data_i[2][65:0] <= 66'h30000000000000000;
	data_i[3][65:0] <= 66'h30000000000000000;    
  data_i[4][65:0] <= 66'h30000000000000000;
	data_i[5][65:0] <= 66'h30000000000000000;  
  data_i[6][65:0] <= 66'h30000000000000000;@(posedge clk_i);
  
  @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	
	where_to_start_i <= 32; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
	repeat(2) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 7;    
	i <= i+8;@(posedge clk_i); end
  
  data_i[0][65:0] <= 66'h00000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i;
  data_i[2][65:0] <= 66'h00000000000000000 + i;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 1;    
  data_i[4][65:0] <= 66'h00000000000000000 + i;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h00000000000000000 + i;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
  data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h00000000000000000 + i;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 1;
	data_i[3][65:0] <= 66'h00000000000000000 + i;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[5][65:0] <= 66'h00000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 3;
	data_i[7][65:0] <= 66'h00000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
	data_i[0][65:0] <= 66'h10000000000000000 + i;
  data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h30000000000000000;
  data_i[1][65:0] <= 66'h30000000000000000;
  data_i[2][65:0] <= 66'h30000000000000000;
	data_i[3][65:0] <= 66'h30000000000000000;    
  data_i[4][65:0] <= 66'h30000000000000000;
	data_i[5][65:0] <= 66'h30000000000000000;  
  data_i[6][65:0] <= 66'h30000000000000000;@(posedge clk_i);
  
  
  
  i <= 0;
									@(posedge clk_i);
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
  data_i[0][65:0] <= 66'h00000000000000000;
	data_i[1][65:0] <= 66'h00000000000000000;
  data_i[2][65:0] <= 66'h00000000000000000;
	data_i[3][65:0] <= 66'h00000000000000000;    
  data_i[4][65:0] <= 66'h00000000000000000;
	data_i[5][65:0] <= 66'h00000000000000000;    
  data_i[6][65:0] <= 66'h00000000000000000;
	data_i[7][65:0] <= 66'h00000000000000000;    
  @(posedge clk_i);
	where_to_start_i <= 0; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
	repeat(2) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 7;    
	i <= i+8;@(posedge clk_i); end
  
  data_i[0][65:0] <= 66'h00000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i;
  data_i[2][65:0] <= 66'h00000000000000000 + i;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 1;    
  data_i[4][65:0] <= 66'h00000000000000000 + i;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h00000000000000000 + i;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
  data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h00000000000000000 + i;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 1;
	data_i[3][65:0] <= 66'h00000000000000000 + i;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[5][65:0] <= 66'h00000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 3;
	data_i[7][65:0] <= 66'h00000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
	data_i[0][65:0] <= 66'h10000000000000000 + i;
  data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h30000000000000000;
  data_i[1][65:0] <= 66'h30000000000000000;
  data_i[2][65:0] <= 66'h30000000000000000;
	data_i[3][65:0] <= 66'h30000000000000000;    
  data_i[4][65:0] <= 66'h30000000000000000;
	data_i[5][65:0] <= 66'h30000000000000000;  
  data_i[6][65:0] <= 66'h30000000000000000;@(posedge clk_i);
  
  @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	
	where_to_start_i <= 32; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
	repeat(2) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 7;    
	i <= i+8;@(posedge clk_i); end
  
  data_i[0][65:0] <= 66'h00000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i;
  data_i[2][65:0] <= 66'h00000000000000000 + i;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 1;    
  data_i[4][65:0] <= 66'h00000000000000000 + i;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h00000000000000000 + i;
	data_i[7][65:0] <= 66'h10000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
  data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h00000000000000000 + i;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 1;
	data_i[3][65:0] <= 66'h00000000000000000 + i;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[5][65:0] <= 66'h00000000000000000 + i + 2;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 3;
	data_i[7][65:0] <= 66'h00000000000000000 + i + 3;  
  i <= i+4;@(posedge clk_i);
  
	data_i[0][65:0] <= 66'h10000000000000000 + i;
  data_i[1][65:0] <= 66'h10000000000000000 + i + 1;
  data_i[2][65:0] <= 66'h10000000000000000 + i + 2;
	data_i[3][65:0] <= 66'h10000000000000000 + i + 3;    
  data_i[4][65:0] <= 66'h10000000000000000 + i + 4;
	data_i[5][65:0] <= 66'h10000000000000000 + i + 5;    
  data_i[6][65:0] <= 66'h10000000000000000 + i + 6;
	data_i[7][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h30000000000000000;
  data_i[1][65:0] <= 66'h30000000000000000;
  data_i[2][65:0] <= 66'h30000000000000000;
	data_i[3][65:0] <= 66'h30000000000000000;    
  data_i[4][65:0] <= 66'h30000000000000000;
	data_i[5][65:0] <= 66'h30000000000000000;  
  data_i[6][65:0] <= 66'h30000000000000000;@(posedge clk_i);

	
				
		$stop(); // end the simulation
  end
  
  
endmodule