//This module moves data from the tree to the memory. It gets a start ,where to start, and length and uses that to determine how much valid data it needs to get out of the tree and where in memory it should start putting it. It does not put invalid or finishes into the memory. It also pulses a finish signal when it has gotten the full list. 
module data_mover_from_tree_to_memory_1 #(parameter addr_size_p = -1, e_height_p = 2)
(input  clk_i
,input reset_i
,input tree_reset_i

,input start_i
,input [addr_size_p - 1:0] where_to_start_i

,input [addr_size_p - 1:0] current_length_i

,input [addr_size_p - 1:0] full_length_i

//,input [e_height_p - 1:0][65:0] data_i
,input [65:0] data_0i
,input [65:0] data_1i

,output wr_en_o
,output [65:0] data_o
,output [addr_size_p - 1:0] wr_addr_o
,output finish_o
,output pause_the_tree_o
);
  //the counter logic
  //logic list_done_r;
  //logic [e_height_p - 1:0] starts_r;
  //logic [addr_size_p - 1:0] times_to_count_r;
  //logic [addr_size_p - 1:0] add_value;
  
  //parallel to serial converter
  logic valid_back;
  logic [65:0] data_received;
  logic ready_for_next_set;

  //patch to avoid wire inputs
  logic [e_height_p - 1:0][65:0] data_r;
  always_ff @(posedge clk_i) begin
    if (reset_i | tree_reset_i) begin
      data_r[0] <= 0;
      data_r[1] <= 0;
    end
    else begin
      data_r[0] <= data_0i;
      data_r[1] <= data_1i;
    end
  end

  //wire have_valid_data = (data_r[0][64] & (~data_r[0][65])) | (data_r[1][64] & (~data_r[1][65]));

  bsg_parallel_in_serial_out #(.width_p(66), .els_p(e_height_p)) parallel_to_serial
  (.clk_i(clk_i)
  ,.reset_i(reset_i | tree_reset_i)
  ,.valid_i(ready_for_next_set) //may be fine at 1, had as have_valid_data
  ,.data_i(data_r)
  ,.ready_o(ready_for_next_set)
  ,.valid_o(valid_back)
  ,.data_o(data_received)
  ,.yumi_i(valid_back));
  
  
  logic [addr_size_p - 1:0] counter_r;
  
  logic [addr_size_p - 1:0] num_of_valids;
  logic [e_height_p - 1:0][65:0] data;
  
  //add up the number of valid inputs
  assign num_of_valids = data_received[64] & (~data_received[65]);
  
  //depending on what data is valid change what gets written to memory
  /**always_comb begin
    if (data_i[0][64]) begin
	  data[0][65:0] = data_i[0][65:0];
	end
	else begin
	  data[0][65:0] = data_i[1][65:0];
	end
  end
  
  assign data[1][65:0] = data_i[1][65:0];
  **/
  //values to use for the adder
  logic waitToStartAgain_r;
  //need to update e_height parameter later, make a clog variable for here
  logic start_r;
  always_ff @(posedge clk_i) begin
    start_r <= start_i;
  end
  wire finish = (current_length_i + where_to_start_i == counter_r) & (~waitToStartAgain_r);
  //wire full_finish = (counter_r == full_length_i);
   
  //the adder to update counter
  wire [addr_size_p - 1:0] next_point = counter_r + num_of_valids;
  
  //determine if the next point is equal to or less than the full length
  //wire next = finish_i & (next_point < full_length_i);
  //wire done = finish_i & (next_point == full_length_i);

  //the counter
  always_ff @(posedge clk_i)
  begin
        if (reset_i) begin //reset case
          counter_r <= 0;
          waitToStartAgain_r <= 1;
        end
	else if (start_i) begin//start case
	  counter_r <= where_to_start_i;
   	  waitToStartAgain_r <= 0;
	end
	else if (~finish /**& valid_back**/) begin //looping case
          counter_r <= next_point;
	  waitToStartAgain_r <= waitToStartAgain_r;
	end
	else if (finish) begin
	  counter_r <= counter_r;
          waitToStartAgain_r <= 1;
	end
	else begin //waiting case
	  counter_r <= counter_r;
	  waitToStartAgain_r <= waitToStartAgain_r;
	end
  end
  
  assign wr_addr_o[addr_size_p - 1:0] = counter_r;
  //assign wr_addr_o[1][addr_size_p - 1:0] = counter_r + 1;
  assign wr_en_o = (num_of_valids == 1);
  //assign wr_en_o[1] = (num_of_valids >= 2);
  assign finish_o = finish;
  assign data_o = data_received;
  assign pause_the_tree_o = ~ ready_for_next_set;

endmodule

module data_mover_from_tree_to_memory_1_testbench();
  //addr size = 8, e height = 2
  logic clk_i, reset_i, start_i;
  logic [7:0] current_length_i, where_to_start_i;
  logic [1:0][65:0] data_i;
  logic wr_en_o;
  logic [65:0] data_o;
  logic [7:0] wr_addr_o;
  logic finish_o, pause_the_tree_o;
  
  data_mover_from_tree_to_memory_1 #(.addr_size_p(8), .e_height_p(2)) DUT
  (.clk_i
  ,.reset_i
  ,.start_i
  ,.where_to_start_i
  ,.current_length_i
  ,.data_i
  ,.wr_en_o
  ,.data_o
  ,.wr_addr_o
  ,.finish_o
  ,.pause_the_tree_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  integer i;
	
  initial begin
	reset_i <= 1; start_i <= 0;
	current_length_i <= 16;
	data_i[0][65:0] <= 0;
	data_i[1][65:0] <= 0;
	i <= 0;
									@(posedge clk_i);
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	where_to_start_i <= 0; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
                                               @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
  @(posedge clk_i);
	repeat(7) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;    
	i <= i+1;@(posedge clk_i); 
  @(posedge clk_i);end
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	
	where_to_start_i <= 16; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
  @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
	repeat(7) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;    
	i <= i+1;@(posedge clk_i); end
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h30000000000000000;@(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end

	
				
		$stop(); // end the simulation
  end
  
  
endmodule
