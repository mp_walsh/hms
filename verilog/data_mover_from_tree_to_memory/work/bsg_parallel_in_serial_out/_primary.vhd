library verilog;
use verilog.vl_types.all;
entity bsg_parallel_in_serial_out is
    generic(
        width_p         : integer := -1;
        els_p           : integer := -1
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        valid_i         : in     vl_logic;
        data_i          : in     vl_logic_vector;
        ready_o         : out    vl_logic;
        valid_o         : out    vl_logic;
        data_o          : out    vl_logic_vector;
        yumi_i          : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of width_p : constant is 1;
    attribute mti_svvh_generic_type of els_p : constant is 1;
end bsg_parallel_in_serial_out;
