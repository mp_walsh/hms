library verilog;
use verilog.vl_types.all;
entity data_mover_from_tree_to_memory_parallelized is
    generic(
        addr_size_p     : integer := -1;
        e_height_p      : integer := 8;
        e_height_lp     : vl_notype
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        start_i         : in     vl_logic;
        where_to_start_i: in     vl_logic_vector;
        current_length_i: in     vl_logic_vector;
        full_length_i   : in     vl_logic_vector;
        data_i          : in     vl_logic_vector;
        wr_en_o         : out    vl_logic_vector;
        data_o          : out    vl_logic_vector;
        finish_o        : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
    attribute mti_svvh_generic_type of e_height_p : constant is 1;
    attribute mti_svvh_generic_type of e_height_lp : constant is 3;
end data_mover_from_tree_to_memory_parallelized;
