library verilog;
use verilog.vl_types.all;
entity data_mover_from_tree_to_memory_parallelized_testbench is
    generic(
        ClockDelay      : integer := 1000
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ClockDelay : constant is 1;
end data_mover_from_tree_to_memory_parallelized_testbench;
