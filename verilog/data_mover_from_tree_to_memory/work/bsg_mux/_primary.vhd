library verilog;
use verilog.vl_types.all;
entity bsg_mux is
    generic(
        width_p         : string  := "inv";
        els_p           : integer := 1;
        harden_p        : integer := 0;
        balanced_p      : integer := 0;
        lg_els_lp       : vl_notype
    );
    port(
        data_i          : in     vl_logic_vector;
        sel_i           : in     vl_logic_vector;
        data_o          : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of width_p : constant is 1;
    attribute mti_svvh_generic_type of els_p : constant is 1;
    attribute mti_svvh_generic_type of harden_p : constant is 1;
    attribute mti_svvh_generic_type of balanced_p : constant is 1;
    attribute mti_svvh_generic_type of lg_els_lp : constant is 3;
end bsg_mux;
