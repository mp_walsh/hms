//This module moves data from the tree to the memory. It gets a start ,where to start, and length and uses that to determine how much valid data it needs to get out of the tree and where in memory it should start putting it. It does not put invalid or finishes into the memory. It also pulses a finish signal when it has gotten the full list. 
module data_mover_from_tree_to_memory #(parameter addr_size_p = -1, e_height_p = 2)
(input  clk_i
,input reset_i

,input start_i
,input [addr_size_p - 1:0] where_to_start_i

,input [addr_size_p - 1:0] current_length_i

,input [e_height_p - 1:0][65:0] data_i

,output [e_height_p - 1:0] wr_en_o
,output [e_height_p - 1:0][65:0] data_o
,output [e_height_p - 1:0][addr_size_p - 1:0] wr_addr_o
,output finish_o
);
  //the counter logic
  //logic list_done_r;
  //logic [e_height_p - 1:0] starts_r;
  //logic [addr_size_p - 1:0] times_to_count_r;
  //logic [addr_size_p - 1:0] add_value;
  logic [addr_size_p - 1:0] counter_r;
  
  logic [addr_size_p - 1:0] num_of_valids;
  logic [e_height_p - 1:0][65:0] data;
  
  //add up the number of valid inputs
  assign num_of_valids = (data_i[0][64] & (~data_i[0][65])) + (data_i[1][64] & (~data_i[1][65]));
  
  //depending on what data is valid change what gets written to memory
  always_comb begin
    if (data_i[0][64]) begin
	  data[0][65:0] = data_i[0][65:0];
	end
	else begin
	  data[0][65:0] = data_i[1][65:0];
	end
  end
  
  assign data[1][65:0] = data_i[1][65:0];
  
  //values to use for the adder
  //need to update e_height parameter later, make a clog variable for here
  wire finish = (current_length_i + where_to_start_i == counter_r);
   
  //the adder to update counter
  wire [addr_size_p - 1:0] next_point = counter_r + num_of_valids;
  
  //determine if the next point is equal to or less than the full length
  //wire next = finish_i & (next_point < full_length_i);
  //wire done = finish_i & (next_point == full_length_i);
  
  //the counter
  always_ff @(posedge clk_i)
  begin
    if (reset_i) begin //reset case
      counter_r <= 0;
	end
	else if (start_i) begin//start case
	  counter_r <= where_to_start_i;
	end
	else if (~finish) begin //looping case
      counter_r <= next_point;
	end
	else begin //waiting case
	  counter_r <= counter_r;
	end
  end
  
  assign wr_addr_o[0][addr_size_p - 1:0] = counter_r;
  assign wr_addr_o[1][addr_size_p - 1:0] = counter_r + 1;
  assign wr_en_o[0] = (num_of_valids >= 1);
  assign wr_en_o[1] = (num_of_valids >= 2);
  assign finish_o = finish;
  assign data_o = data;

endmodule

module data_mover_from_tree_to_memory_testbench();
  //addr size = 8, e height = 2
  logic clk_i, reset_i, start_i;
  logic [7:0] current_length_i, where_to_start_i;
  logic [1:0][65:0] data_i;
  logic [1:0] wr_en_o;
  logic [1:0][65:0] data_o;
  logic [1:0][7:0] wr_addr_o;
  logic finish_o;
  
  data_mover_from_tree_to_memory #(.addr_size_p(8), .e_height_p(2)) DUT
  (.clk_i
  ,.reset_i
  ,.start_i
  ,.where_to_start_i
  ,.current_length_i
  ,.data_i
  ,.wr_en_o
  ,.data_o
  ,.wr_addr_o
  ,.finish_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  integer i;
	
  initial begin
	reset_i <= 1; start_i <= 0;
	current_length_i <= 16;
	data_i[0][65:0] <= 0;
	data_i[1][65:0] <= 0;
	i <= 0;
									@(posedge clk_i);
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	where_to_start_i <= 0; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
	repeat(7) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;    
	i <= i+1;@(posedge clk_i); end
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h20000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h20000000000000000;@(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	
	where_to_start_i <= 16; start_i <= 1; @(posedge clk_i);
	start_i <= 0;                                @(posedge clk_i);
	data_i[1][65:0] <= 66'h10000000000000000 + i; i<= i+1; @(posedge clk_i);
	repeat(7) begin 
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h10000000000000000 + i + 1;    
	i <= i+1;@(posedge clk_i); end
	data_i[0][65:0] <= 66'h10000000000000000 + i;
	data_i[1][65:0] <= 66'h20000000000000000;@(posedge clk_i);
	data_i[0][65:0] <= 66'h20000000000000000;@(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end

	
				
		$stop(); // end the simulation
  end
  
  
endmodule