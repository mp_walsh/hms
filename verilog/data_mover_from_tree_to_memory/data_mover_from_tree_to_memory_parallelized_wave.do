onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/clk_i
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/reset_i
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/start_i
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_parallelized_testbench/current_length_i
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_parallelized_testbench/where_to_start_i
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_parallelized_testbench/full_length_i
add wave -noupdate -radix hexadecimal -childformat {{{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[7]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[6]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[5]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[4]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[3]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[2]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[1]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[0]} -radix hexadecimal}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_parallelized_testbench/data_i[7]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_i[6]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_i[5]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_i[4]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_i[3]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_i[2]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_i[1]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_i[0]} {-height 15 -radix hexadecimal}} /data_mover_from_tree_to_memory_parallelized_testbench/data_i
add wave -noupdate -expand /data_mover_from_tree_to_memory_parallelized_testbench/wr_en_o
add wave -noupdate -radix hexadecimal -childformat {{{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[7]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[6]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[5]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[4]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[3]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[2]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[1]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_parallelized_testbench/data_o[7]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_o[6]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_o[5]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_o[4]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_o[3]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_o[2]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_o[1]} {-height 15 -radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/data_o[0]} {-height 15 -radix hexadecimal}} /data_mover_from_tree_to_memory_parallelized_testbench/data_o
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/finish_o
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_parallelized_testbench/DUT/counter_r
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/DUT/fifoCounter
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_parallelized_testbench/DUT/num_of_valids
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/DUT/full_finish
add wave -noupdate -radix unsigned /data_mover_from_tree_to_memory_parallelized_testbench/DUT/next_fifo_point
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/DUT/wr_ens
add wave -noupdate -radix hexadecimal -childformat {{{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[7]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[6]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[5]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[4]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[3]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[2]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[1]} -radix hexadecimal} {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[0]} -radix hexadecimal}} -expand -subitemconfig {{/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[7]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[6]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[5]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[4]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[3]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[2]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[1]} {-radix hexadecimal} {/data_mover_from_tree_to_memory_parallelized_testbench/DUT/data[0]} {-radix hexadecimal}} /data_mover_from_tree_to_memory_parallelized_testbench/DUT/data
add wave -noupdate /data_mover_from_tree_to_memory_parallelized_testbench/DUT/valids
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9255 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 179
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {28875 ps}
