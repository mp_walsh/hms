//all the modules put together
module hms_full 
import hms_pkg::*;
#(parameter memory_size_p = -1, addr_size_lp = $clog2(memory_size_p+1), addr_size_mem_lp = addr_size_lp - 1, ring_width_p = -1, id_p="inv", e_height_p = -1)
(input  clk_i
,input reset_i
,input en_i

,input v_i
,input [ring_width_p - 1:0] data_i
,output ready_o

,output                    v_o
,output [ring_width_p-1:0] data_o
,input                     yumi_i
);

  //the FSB control manager
  logic finish_sorting, list_done, wr_en_FSB;
  logic [addr_size_lp - 1:0] wr_addr_FSB;
  logic [65:0] wr_data_FSB;
  logic [31:0] length_of_list, log_of_list;
  
  //logic [65:0] read_data_FSB;
  logic [addr_size_lp - 1:0] rd_addr_FSB;
  logic rd_en_FSB;
  logic [65:0] rd_data_outgoing;
  logic FSB_reset;
  FSB_control #(.addr_size_p(addr_size_lp), .ring_width_p(ring_width_p)) in_and_out_control
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.finish_i(finish_sorting)
  ,.input_data_i(data_i)
  ,.v_i(v_i)
  ,.ready_o(ready_o)
  ,.v_o(v_o)
  ,.data_o(data_o)
  ,.yumi_i(yumi_i)
  ,.list_done_o(list_done)
  ,.FSB_reset_o(FSB_reset)
  ,.wr_en_o(wr_en_FSB)
  ,.wr_addr_o(wr_addr_FSB)
  ,.wr_data_o(wr_data_FSB)
  ,.read_data_i(rd_data_outgoing)
  ,.rd_addr_o(rd_addr_FSB)
  ,.rd_en_o(rd_en_FSB)
  ,.length_of_list_o(length_of_list)
  ,.max_sorted_o(log_of_list));
  
  //manager module to do a full round of sort
  logic [addr_size_lp - 1:0] max_sorted;
  logic done_a_round, moved, start_move, new_iteration;
  logic [addr_size_lp - 1:0] next_sorted_length;
  full_sort_manager #(.addr_size_p(addr_size_lp)) high_manager
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.list_done_i(list_done)
  ,.FSB_reset_i(FSB_reset)
  ,.done_a_round_i(done_a_round)
  ,.log_of_list_i(log_of_list[addr_size_lp - 1:0])
  ,.next_sorted_length_i(next_sorted_length)
  ,.moved_i(moved)
  ,.start_move_o(start_move)
  ,.new_iteration_o(new_iteration)
  ,.max_sorted_o(max_sorted)
  ,.finished_sorting_o(finish_sorting));
  
  //entrance memory bank
  logic [e_height_p - 1:0][addr_size_lp - 1:0] r_addr_incoming;
  logic [e_height_p - 1:0][addr_size_mem_lp - 1:0] r_addr_incoming_between;
  assign r_addr_incoming_between[0] = r_addr_incoming[0][addr_size_mem_lp - 1:0];
  assign r_addr_incoming_between[1] = r_addr_incoming[1][addr_size_mem_lp - 1:0];
  logic [e_height_p - 1:0][65:0] read_data_incoming;
  logic [e_height_p - 1:0] rd_en;
  
  logic wr_en_incoming;
  logic [addr_size_lp - 1:0] wr_addr_incoming;
  logic [65:0] wr_data_incoming;
  logic [e_height_p - 1:0][5:0] rd_data_incoming_extra;
  bsg_mem_2r1w_sync #(.width_p(72), .els_p(memory_size_p), .read_write_same_addr_p(1)) incoming_memory
   (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.w_v_i(wr_en_incoming)
   ,.w_addr_i(wr_addr_incoming[addr_size_mem_lp - 1:0])
   ,.w_data_i({6'b0, wr_data_incoming})
   ,.r0_v_i(rd_en[0])
   ,.r0_addr_i(r_addr_incoming_between[0])
   ,.r0_data_o({rd_data_incoming_extra[0], read_data_incoming[0]})
   ,.r1_v_i(rd_en[1])
   ,.r1_addr_i(r_addr_incoming_between[1])
   ,.r1_data_o({rd_data_incoming_extra[1], read_data_incoming[1]}));
  
  
  //single loop over manager
  logic finish, reset_tree;
  logic [e_height_p - 1:0] starts;
  logic [e_height_p - 1:0][addr_size_lp - 1:0] whereToStarts;
  sub_iter_manager #(.addr_size_p(addr_size_lp), .e_height_p(2)) sub_manager
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.finish_i(finish)
  ,.full_length_i(length_of_list[addr_size_lp - 1:0])
  ,.max_sorted_i(max_sorted[addr_size_lp - 1:0])
  ,.new_iteration_i(new_iteration)
  ,.starts_o(starts)
  ,.whereToStarts_o(whereToStarts)
  ,.done_o(done_a_round)
  ,.reset_tree_o(reset_tree)
  ,.next_sorted_length_o(next_sorted_length));
  
  genvar i;
  //generate a collection of e extractors
  logic [e_height_p - 1:0] fifo_full;
  logic [e_height_p - 1:0] fifo_en;
  logic [e_height_p - 1:0][65:0] data_to_fifo;
  generate
	for(i=0; i<e_height_p; i++) begin : eachExtractor
	  sorted_list_extracter_slave #(.addr_size_p(addr_size_lp)) an_extractor
      (.clk_i(clk_i)
      ,.reset_i(reset_i)
      ,.start_i(starts[i])
      ,.max_sorted_i(max_sorted[addr_size_lp - 1:0])
      ,.where_to_start_i(whereToStarts[i][addr_size_lp - 1:0])
      ,.fifo_full_i(~fifo_full[i])//change to not full
      ,.read_data_i(read_data_incoming[i][65:0])
      ,.read_addr_o(r_addr_incoming[i][addr_size_lp - 1:0])
      ,.fifo_en_o(fifo_en[i])
      ,.rd_en_o(rd_en[i])
      ,.data_out_o(data_to_fifo[i][65:0]));
	end
  endgenerate
  
  genvar j;
  //generate a collection of e couplers
  logic [1:0][e_height_p - 1:0][65:0] paired_data;
  logic [e_height_p - 1:0][e_height_p - 1:0] paired_data_valid;
  logic [e_height_p - 1:0][e_height_p - 1:0] tree_ready;
  logic [e_height_p - 1:0] tree_ready_between;
  generate
	for(j=0; j<e_height_p; j++) begin : eachCoupler
	  bsg_serial_in_parallel_out #(.width_p(66), .els_p(2), .out_els_p(e_height_p)) a_coupler
     (.clk_i(clk_i)
     ,.reset_i(reset_i | reset_tree)
     ,.valid_i(fifo_en[j])
     ,.data_i(data_to_fifo[j]/*[65:0]*/)
     ,.ready_o(fifo_full[j])
     ,.valid_o(paired_data_valid[j])
	 // if we are just trying to get the parallel out, shouldn't it just be .data_o(paired_data[j])?
     ,.data_o(paired_data[j])//[e_height_p - 1:0][65:0])
     ,.yumi_cnt_i(tree_ready[j]));
	end
  endgenerate
  
  //join the tree_readys together
  assign tree_ready[0][1] = (~tree_ready_between[0])&(paired_data_valid[0][0] & paired_data_valid[0][1]);
  assign tree_ready[0][0] = 1'b0;
  assign tree_ready[1][0] = 1'b0;
  assign tree_ready[1][1] = (~tree_ready_between[1])&(paired_data_valid[1][0] & paired_data_valid[1][1]);
  
  //join valid outs from couplers together
  logic [e_height_p - 1:0] paired_data_valid_between;
  assign paired_data_valid_between[0] = paired_data_valid[0][1] & paired_data_valid[0][0];
  assign paired_data_valid_between[1] = paired_data_valid[1][1] & paired_data_valid[1][0];
  
  // convert buses to struct form 
  e_s [e_height_p-1:0] a_data_n;
  e_s [e_height_p-1:0] b_data_n; 
  genvar x;
  for(x = 0; x < e_height_p; x = x+1) begin 
	assign a_data_n[x].key = paired_data[0][x][65:32];
	assign a_data_n[x].data = paired_data[0][x][31:0];
	assign b_data_n[x].key = paired_data[1][x][65:32];
	assign b_data_n[x].data = paired_data[1][x][31:0];
  end
  
  e_s [e_height_p-1:0] data_out_of_tree_n;
  
  //the merge logic when it is ready
  //should be connected to paired_data, paired_data_valid, tree_ready, reset_tree, and data_from_tree
  logic pause_the_tree;
  merge_logic #(.e_height_p(e_height_p)) the_merge_logic
  (.clk_i(clk_i)
  ,.reset_i(reset_i | reset_tree)
  ,.full_i(pause_the_tree)
  ,.a_data_i(a_data_n)
  ,.b_data_i(b_data_n)
  ,.a_v_i(paired_data_valid_between[0])
  ,.b_v_i(paired_data_valid_between[1])
  ,.b_full_o(tree_ready_between[1])
  ,.a_full_o(tree_ready_between[0])
  ,.data_o(data_out_of_tree_n));
  
  
  
  //data mover from tree to the output memory
  logic [e_height_p - 1:0][65:0] data_from_tree;
  logic wr_en_outgoing;
  logic [65:0] wr_data_outgoing;
  logic [addr_size_lp - 1:0] wr_addr_outgoing;
  
  genvar y;
  for(y = 0; y < e_height_p; y = y+1) begin 
	assign data_from_tree[y][31:0] = data_out_of_tree_n[y].data;
    assign data_from_tree[y][65:32] = data_out_of_tree_n[y].key;
  end
  
  data_mover_from_tree_to_memory_1 #(.addr_size_p(addr_size_lp), .e_height_p(2)) data_mover_new
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.tree_reset_i(reset_tree)
  ,.start_i(starts[0])
  ,.where_to_start_i(whereToStarts[0])
  ,.current_length_i(next_sorted_length)
  ,.full_length_i(length_of_list[addr_size_lp - 1: 0])
  //,.data_i(data_from_tree)
  ,.data_0i(data_from_tree[0])
  ,.data_1i(data_from_tree[1])
  ,.wr_en_o(wr_en_outgoing)
  ,.data_o(wr_data_outgoing)
  ,.wr_addr_o(wr_addr_outgoing)
  ,.finish_o(finish)
  ,.pause_the_tree_o(pause_the_tree));
  
  //exit memory bank
  //logic [e_height_p - 1:0][addr_size_lp - 1:0] r_addr_incoming;
  //logic [e_height_p - 1:0][65:0] read_data_incoming;
  logic [addr_size_mem_lp - 1:0] wr_addr_outgoing_between;
  assign wr_addr_outgoing_between = wr_addr_outgoing[addr_size_mem_lp - 1:0];
  
  logic rd_en_outgoing;
  logic [addr_size_lp - 1:0] rd_addr_outgoing;
  logic [5:0] rd_data_outgoing_extra;
  //logic [65:0] rd_data_outgoing;
  bsg_mem_1r1w_sync #(.width_p(72), .els_p(memory_size_p), .read_write_same_addr_p(1)) outgoing_memory
   (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.w_v_i(wr_en_outgoing)
   ,.w_addr_i(wr_addr_outgoing_between)
   ,.w_data_i({6'b0, wr_data_outgoing})
   ,.r_v_i(rd_en_outgoing)
   ,.r_addr_i(rd_addr_outgoing[addr_size_mem_lp - 1:0])
   ,.r_data_o({rd_data_outgoing_extra, rd_data_outgoing}));
  
  logic rd_en_mem_to_mem, wr_en_mem_to_mem;
  logic [addr_size_lp - 1:0] rd_addr_mem_to_mem, wr_addr_mem_to_mem;
  logic [65:0] wr_data_mem_to_mem;
  data_mover_from_memory_to_memory #(.addr_size_p(addr_size_lp)) data_mover_mem
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.start_move_i(start_move)
  ,.length_of_list_i(length_of_list[addr_size_lp - 1: 0])
  ,.rd_en_o(rd_en_mem_to_mem)
  ,.mem_data_i(rd_data_outgoing)
  ,.rd_addr_o(rd_addr_mem_to_mem)
  ,.wr_en_o(wr_en_mem_to_mem)
  ,.mem_data_o(wr_data_mem_to_mem)
  ,.wr_addr_o(wr_addr_mem_to_mem)
  ,.moved_o(moved));
  
  
  //if we are moving data from memory to memory, connect incoming write to the memory to memory module
  always_comb begin
    if (wr_en_mem_to_mem) begin
      wr_en_incoming <= wr_en_mem_to_mem;
      wr_addr_incoming <= wr_addr_mem_to_mem;
      wr_data_incoming <= wr_data_mem_to_mem;
    end
    else begin
      wr_en_incoming <= wr_en_FSB;
      wr_addr_incoming <= wr_addr_FSB;
      wr_data_incoming <= wr_data_FSB;
    end
  end
  
  //if we are moving data from memory to memory, connect outgoing read to memory to memory module
  always_comb begin
    if (rd_en_mem_to_mem) begin
      rd_en_outgoing <= rd_en_mem_to_mem;
      rd_addr_outgoing <= rd_addr_mem_to_mem;
    end
    else begin
      rd_en_outgoing <= rd_en_FSB;
      rd_addr_outgoing <= rd_addr_FSB;
    end
  end
  
endmodule


module hms_full_testbench #(parameter memory_size_p = 128, ring_width_p = 75, id_p = 0, e_height_p = 2);
  logic  clk_i, reset_i, en_i, v_i;
  logic [ring_width_p - 1:0] data_i;
  logic ready_o, v_o;
  logic [ring_width_p-1:0] data_o;
  logic yumi_i;
  
  //the module
  hms_full #(.memory_size_p(memory_size_p), .ring_width_p(ring_width_p), .id_p(id_p), .e_height_p(e_height_p)) DUT
  (.clk_i
  ,.reset_i
  ,.en_i
  ,.v_i
  ,.data_i
  ,.ready_o
  ,.v_o
  ,.data_o
  ,.yumi_i);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  initial begin
	reset_i <= 0; en_i <= 1;
	data_i <= 0;
	v_i <= 0;
	yumi_i <= 0;
									@(posedge clk_i);
	reset_i <= 1;					@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	                                @(posedge clk_i);
	v_i <= 1; data_i[74:64] <= 11'b0000000__0000;
	data_i[63:32] = 32'b00000000000000000000000000000100;
	data_i[31:0] = 32'b00000000000000000000000000010000 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
			  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000001_00000000000000000000000000000001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000110_00000000000000000000000000000110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001001_00000000000000000000000000001001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001010_00000000000000000000000000001010 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000010_00000000000000000000000000000010 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000100_00000000000000000000000000000100 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000101_00000000000000000000000000000101 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001000_00000000000000000000000000001000 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000011_00000000000000000000000000000011 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000110_00000000000000000000000000000110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000111_00000000000000000000000000000111 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001011_00000000000000000000000000001011 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000101_00000000000000000000000000000101 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001001_00000000000000000000000000001001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001100_00000000000000000000000000001100 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001110_00000000000000000000000000001110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);			  
			  
	
	repeat(1000) begin					@(posedge clk_i); end
	repeat(16) begin yumi_i <= 1; @(posedge clk_i);
	                 yumi_i <= 0;             @(posedge clk_i); end
    @(posedge clk_i);
	@(posedge clk_i);
	
  /**
	v_i <= 1; data_i[74:64] <= 11'b0000000__0000;
	data_i[63:32] = 32'b00000000000000000000000000000100;
	data_i[31:0] = 32'b00000000000000000000000000010000 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
			  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000011_00000000000000000000000000000011 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000110_00000000000000000000000000000110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000111_00000000000000000000000000000111 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001011_00000000000000000000000000001011 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000101_00000000000000000000000000000101 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001001_00000000000000000000000000001001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001100_00000000000000000000000000001100 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001110_00000000000000000000000000001110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
			  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000001_00000000000000000000000000000001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000110_00000000000000000000000000000110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001001_00000000000000000000000000001001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001010_00000000000000000000000000001010 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000010_00000000000000000000000000000010 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000100_00000000000000000000000000000100 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000101_00000000000000000000000000000101 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001000_00000000000000000000000000001000 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		

				  
			  
	
	repeat(200) begin					@(posedge clk_i); end
	repeat(16) begin yumi_i <= 1; @(posedge clk_i);
	                 yumi_i <= 0;              @(posedge clk_i); end
    @(posedge clk_i);
	**/
	
				
	$stop(); // end the simulation
  end


endmodule
