module merge_tree_8
  import hms_pkg::*;
  (input clk_i, 
   input reset_i,
   input full_i, 
   // these go into couplers as pairs [0,1] [2,3] and so on
   // 0.key < 1.key , 2.key < 3.key and so on 
   input e_s [7:0]data_i, 
   input [7:0] v_i,
   output e_s [7:0] data_o,
   // these are the full outputs for the first stage of FIFOs in the tree 
   output  [7:0] full_o  
  );
  
 // full nets from 8 stage to 4 stage 
 logic a_full_n, b_full_n; 
 e_s [3:0] a_data_n, b_data_n;
 
 merge_tree_4 A
  (.clk_i(clk_i) 
  ,.reset_i(reset_i)
  ,.full_i(a_full_n) 
  ,.a_data_i(data_i[1:0]) // lower bus bits are smaller values 
  ,.a_v_i(v_i[1:0])
  ,.b_data_i(data_i[3:2]) 
  ,.b_v_i(v_i[3:2]) 
  ,.data_o(a_data_n)
  ,.full_o(full_o[3:0]) 
  );
  
 merge_tree_4 B
  (.clk_i(clk_i) 
  ,.reset_i(reset_i)
  ,.full_i(b_full_n) 
  ,.a_data_i(data_i[5:4]) // lower bus bits are smaller values 
  ,.a_v_i(v_i[5:4])
  ,.b_data_i(data_i[7:6]) 
  ,.b_v_i(v_i[7:6]) 
  ,.data_o(b_data_n)
  ,.full_o(full_o[7:4]) 
  );
  
  logic a_v_n, b_v_n;
  assign a_v_n =  a_data_n[0].key[e_key_width_p-2] | a_data_n[1].key[e_key_width_p-2] | a_data_n[2].key[e_key_width_p-2] | a_data_n[3].key[e_key_width_p-2];
  assign b_v_n =  b_data_n[0].key[e_key_width_p-2] | b_data_n[1].key[e_key_width_p-2] | b_data_n[2].key[e_key_width_p-2] | b_data_n[3].key[e_key_width_p-2];
  
  merge_tree_leaf #(.e_height_p(8)) F
  (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.a_v_i(a_v_n) 
   ,.b_v_i(b_v_n)
   ,.full_i(full_i) 
   ,.a_data_i(a_data_n)
   ,.b_data_i(b_data_n)
   ,.data_o(data_o)
   ,.a_full_o(a_full_n)
   ,.b_full_o(b_full_n)
  );  
  
  
  
endmodule 

module merge_tree_8_tb 
    import hms_pkg::*; ();
    //inputs 
    logic clk_i, reset_i, full_i;
    e_s [7:0] data_i;
    logic [7:0] v_i;
   
    // outputs 
    e_s [7:0] data_o; 
    logic [7:0] full_o;  
   integer a_value = 1;
   integer b_value = 2;
   merge_tree_8 DUT
   (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.full_i(full_i)
   ,.data_i(data_i)
   ,.v_i(v_i)
   ,.data_o(data_o)
   ,.full_o(full_o)
   ); 
      
   initial begin
    clk_i <= 1'b0; 
    reset_i <= 1'b1; 
    full_i <= 1'b0; 
    v_i <= 8'b0000_0000;
    forever #5 clk_i <= ~clk_i; 
   end 
   
   initial begin 
    @(posedge clk_i) reset_i <= 1'b0; 
    
    @(posedge clk_i)  
      v_i <= 8'b1111_1111; 
      for(int x = 0; x < 4; x = x+1) begin 
        data_i[x].key <= (34'h1_0000_0000 | a_value);
        data_i[x].data <= (32'h0000_0000  | a_value);
        a_value = a_value + 1; 
      end 
      for(int y = 4; y < 8; y = y +1) begin 
        data_i[y].key <= (34'h1_0000_0000 | b_value);
        data_i[y].data <= (32'h0000_0000  | b_value);
        b_value = b_value + 1; 
      end 
    @(posedge clk_i) v_i = 8'b0000_0000; 
   
   @(posedge clk_i) v_i = 8'b1111_1111;  
      for(int x = 0; x < 4; x = x+1) begin 
        data_i[x].key <= (34'h1_0000_0000 | a_value);
        data_i[x].data <= (32'h0000_0000  | a_value);
        a_value = a_value + 1; 
      end 
      for(int y = 4; y < 8; y = y +1) begin 
        data_i[y].key <= (34'h1_0000_0000 | b_value);
        data_i[y].data <= (32'h0000_0000  | b_value);
        b_value = b_value + 1; 
      end 
    @(posedge clk_i) v_i = 8'b0000_0000; 
    
    @(posedge clk_i) v_i = 8'b1111_1111;  
      for(int x = 0; x < 4; x = x+1) begin 
        data_i[x].key <= (34'h1_0000_0000 | a_value);
        data_i[x].data <= (32'h0000_0000  | a_value);
        a_value = a_value + 1; 
      end 
      for(int y = 4; y < 8; y = y +1) begin 
        data_i[y].key <= (34'h1_0000_0000 | b_value);
        data_i[y].data <= (32'h0000_0000  | b_value);
        b_value = b_value + 1; 
      end 
    @(posedge clk_i) v_i = 8'b0000_0000; 
    
    
    // Flush with Finishes 
    repeat (50) begin 
      @(posedge clk_i) v_i = 8'b1111_1111;  
      for(int x = 0; x < 8; x = x+1) begin 
        data_i[x].key <= 34'h3_0000_0000;
        data_i[x].data <= 32'h0000_0000 ;
      end 
      @(posedge clk_i) v_i = 8'b0000_0000; 
    end // close repeat 
   $stop; 
   end 
endmodule