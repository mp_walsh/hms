module merge_tree_leaf
  import hms_pkg::*;
  #(parameter e_height_p =-1) // needs to be a multiple of 2!!! 
  (input clk_i, 
   input reset_i,
   input a_v_i,
   input b_v_i, 
   input full_i, 
   input e_s [((e_height_p/2)-1):0] a_data_i,
   input e_s [((e_height_p/2)-1):0] b_data_i,
   output e_s [e_height_p-1:0] data_o,
   output  a_full_o,
   output  b_full_o
  );
  
  /*  
  if(reset_i == 0) begin 
    assert(a_ready_n == 1'b1);
    assert(b_ready_n == 1'b1);
  end */  
  
  parameter yumi_width_p = 2; 
  logic a_ready_n, b_ready_n;
  logic [1:0] a_valid_n, b_valid_n; 
  e_s [e_height_p-1:0] a_data_n, b_data_n; 
  
  bsg_serial_in_parallel_out #(.width_p(e_width_p * (e_height_p/2))
                              ,.els_p(2)) coupler_a
   (.clk_i(clk_i) 
   ,.reset_i(reset_i) 
   ,.valid_i(a_v_i) 
   ,.data_i(a_data_i) 
   ,.ready_o(a_ready_n) 
   ,.valid_o(a_valid_n)
   ,.data_o(a_data_n)
   // not full & bus wide and of valid. This is to prevent readout on empty
   ,.yumi_cnt_i({(~a_full_o & (&(a_valid_n))), 1'b0})
   );
   
  bsg_serial_in_parallel_out #(.width_p(e_width_p * (e_height_p/2))
                              ,.els_p(2)) coupler_b
   (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.valid_i(b_v_i) 
   ,.data_i(b_data_i) 
   ,.ready_o(b_ready_n) 
   ,.valid_o(b_valid_n)
   ,.data_o(b_data_n)
   ,.yumi_cnt_i({(~b_full_o & (&(b_valid_n))), {(yumi_width_p-1){1'b0}}})
   );
   
  logic a_coup_valid_n, b_coup_valid_n;
  assign a_coup_valid_n = &a_valid_n;
  assign b_coup_valid_n = &b_valid_n;

    
  merge_logic #(.e_height_p(e_height_p)) leaf 
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.full_i(full_i)
    ,.a_data_i(a_data_n)
    ,.b_data_i(b_data_n)  
    ,.a_v_i(a_coup_valid_n)
    ,.b_v_i(b_coup_valid_n) 
    ,.b_full_o(b_full_o)
    ,.a_full_o(a_full_o)
    ,.data_o(data_o) 
    );
    
   /* assert((~a_full_o)&&(a_ready_n == 1'b1));
    assert((~b_full_o)&&(b_ready_n == 1'b1));  */  
    
endmodule 

task toggle_valid(input clk_i, input a_v_i, input b_v_i);
   @(posedge clk_i)  a_v_i <= 1'b1; b_v_i <= 1'b1; 
   @(posedge clk_i)  a_v_i <= 1'b0; b_v_i <= 1'b0;
   $stop; 
endtask 

// tests for smalles possible merge tree, 2 2 input leaves to a 4 input. 
module merge1_tree_leaf_tb import hms_pkg::*;(); 
  parameter e_height_p = 2; 
  // inputs 
  logic clk_i, reset_i, a_v_i, b_v_i, full_i;
  e_s [(e_height_p/2)-1:0] a_data_i, b_data_i; 
  
  // outputs 
  e_s [e_height_p-1:0] data_o; 
  logic a_full_o, b_full_o; 
  merge_tree_leaf #(.e_height_p(e_height_p)) DUT 
  (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.a_v_i(a_v_i)
   ,.b_v_i(b_v_i)
   ,.full_i(full_i) 
   ,.a_data_i(a_data_i)
   ,.b_data_i(b_data_i)
   ,.data_o(data_o)
   ,.a_full_o(a_full_o)
   ,.b_full_o(b_full_o)
  );
  
  initial begin 
    clk_i <= 1'b0; reset_i <= 1'b1; 
    a_v_i <= 1'b0; b_v_i <= 1'b0; 
    full_i <= 1'b0; 
    a_data_i[0].key <= 34'h0_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h0_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    forever #5 clk_i <= ~clk_i; 
  end 
  
  initial begin 
  // Basic Test. Normal operation, no edge cases 
  @(posedge clk_i) reset_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0001;
    a_data_i[0].data <= 32'h0000_0001;
    b_data_i[0].key <= 34'h1_0000_0002;
    b_data_i[0].data <= 32'h0000_0002;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0002;
    a_data_i[0].data <= 32'h0000_0002;
    b_data_i[0].key <= 34'h1_0000_0003;
    b_data_i[0].data <= 32'h0000_0003;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0009;
    a_data_i[0].data <= 32'h0000_0009;
    b_data_i[0].key <= 34'h1_0000_0008;
    b_data_i[0].data <= 32'h0000_0008;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_000C;
    a_data_i[0].data <= 32'h0000_000C;
    b_data_i[0].key <= 34'h1_0000_000F;
    b_data_i[0].data <= 32'h0000_000F;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  repeat (20) @(posedge clk_i);
  @(posedge clk_i) reset_i <= 1'b1;
  
  
  /* Edge Case, Two already sorted lists where every element in b is > a!
  @(posedge clk_i) reset_i <= 1'b0;  
  @(posedge clk_i)  a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0001;
    a_data_i[0].data <= 32'h0000_0001;
    b_data_i[0].key <= 34'h1_0000_0002;
    b_data_i[0].data <= 32'h0000_0002;
  // uncomment to test this out, it runs forever... 
  //repeat (32) begin toggle_valid(clk_i, a_v_i, b_v_i); end */ 
  
  
  /* // Edge Case, Two already sorted lists where every element in a is > b!
  @(posedge clk_i) reset_i <= 1'b0;  
  @(posedge clk_i)  a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0002;
    a_data_i[0].data <= 32'h0000_0002;
    b_data_i[0].key <= 34'h1_0000_0001;
    b_data_i[0].data <= 32'h0000_0001;
  // uncomment to test this out, it runs forever...
  repeat (32) begin toggle_valid(clk_i, a_v_i, b_v_i); end */ 
  
  
  // Same case as before but with a bunch of stalls at random cycles 
  @(posedge clk_i) reset_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0001;
    a_data_i[0].data <= 32'h0000_0001;
    b_data_i[0].key <= 34'h1_0000_0002;
    b_data_i[0].data <= 32'h0000_0002;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; full_i <= 1'b1; 
  repeat(3) @(posedge clk_i); 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; full_i <= 1'b0; 
    a_data_i[0].key <= 34'h1_0000_0002;
    a_data_i[0].data <= 32'h0000_0002;
    b_data_i[0].key <= 34'h1_0000_0003;
    b_data_i[0].data <= 32'h0000_0003;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;  full_i <= 1'b1; 
  @(posedge clk_i); 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; full_i <= 1'b0; 
    a_data_i[0].key <= 34'h1_0000_0009;
    a_data_i[0].data <= 32'h0000_0009;
    b_data_i[0].key <= 34'h1_0000_0008;
    b_data_i[0].data <= 32'h0000_0008;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;  full_i <= 1'b1; 
  repeat (10) @(posedge clk_i); 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; full_i <= 1'b0; 
    a_data_i[0].key <= 34'h1_0000_000C;
    a_data_i[0].data <= 32'h0000_000C;
    b_data_i[0].key <= 34'h1_0000_000F;
    b_data_i[0].data <= 32'h0000_000F;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  repeat (20) @(posedge clk_i);
  @(posedge clk_i) reset_i <= 1'b1;
  $stop; 
  end 
endmodule



// tests first larger parameter of 4 input couplers to 8 output merge logic  
module merge2_tree_leaf_tb import hms_pkg::*;(); 
  parameter e_height_p = 4; 
  // inputs 
  logic clk_i, reset_i, a_v_i, b_v_i, full_i;
  e_s [(e_height_p/2)-1:0] a_data_i, b_data_i; 
  
  // outputs 
  e_s [e_height_p-1:0] data_o; 
  logic a_full_o, b_full_o; 
  merge_tree_leaf #(.e_height_p(e_height_p)) DUT 
  (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.a_v_i(a_v_i)
   ,.b_v_i(b_v_i)
   ,.full_i(full_i) 
   ,.a_data_i(a_data_i)
   ,.b_data_i(b_data_i)
   ,.data_o(data_o)
   ,.a_full_o(a_full_o)
   ,.b_full_o(b_full_o)
  );
  
  initial begin 
    clk_i <= 1'b0; reset_i <= 1'b1; 
    a_v_i <= 1'b0; b_v_i <= 1'b0; 
    full_i <= 1'b0; 
    a_data_i[0].key <= 34'h0_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h0_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h0_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h0_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
    forever #5 clk_i <= ~clk_i; 
  end 
  
  initial begin 
  // Basic Test. Normal operation
  @(posedge clk_i) reset_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0001;
    a_data_i[0].data <= 32'h0000_0001;
    b_data_i[0].key <= 34'h1_0000_0002;
    b_data_i[0].data <= 32'h0000_0002;
    a_data_i[1].key <= 34'h1_0000_0003;
    a_data_i[1].data <= 32'h0000_0003;
    b_data_i[1].key <= 34'h1_0000_0004;
    b_data_i[1].data <= 32'h0000_0004;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0010;
    a_data_i[0].data <= 32'h0000_0010;
    b_data_i[0].key <= 34'h1_0000_000A;
    b_data_i[0].data <= 32'h0000_000A;
    a_data_i[1].key <= 34'h1_0000_0011;
    a_data_i[1].data <= 32'h0000_0011;
    b_data_i[1].key <= 34'h1_0000_000B;
    b_data_i[1].data <= 32'h0000_000B;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0013;
    a_data_i[0].data <= 32'h0000_0013;
    b_data_i[0].key <= 34'h1_0000_0100;
    b_data_i[0].data <= 32'h0000_0100;
    a_data_i[1].key <= 34'h1_0000_0014;
    a_data_i[1].data <= 32'h0000_0014;
    b_data_i[1].key <= 34'h1_0000_0110;
    b_data_i[1].data <= 32'h0000_0110;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0015;
    a_data_i[0].data <= 32'h0000_0015;
    b_data_i[0].key <= 34'h1_0000_1000;
    b_data_i[0].data <= 32'h0000_1000;
    a_data_i[1].key <= 34'h1_0000_0016;
    a_data_i[1].data <= 32'h0000_1016;
    b_data_i[1].key <= 34'h1_0000_1001;
    b_data_i[1].data <= 32'h0000_1001;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;  full_i <= 1'b1; 
  repeat (10) @(posedge clk_i);  
  @(posedge clk_i) full_i <=1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_10F0_0000;
    a_data_i[0].data <= 32'h10F0_0000;
    b_data_i[0].key <= 34'h1_0000_1009;
    b_data_i[0].data <= 32'h0000_1009;
    a_data_i[1].key <= 34'h1_10F0_0001;
    a_data_i[1].data <= 32'h10F0_0001;
    b_data_i[1].key <= 34'h1_0000_100A;
    b_data_i[1].data <= 32'h0000_100A;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
   @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
   @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
   @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  repeat (20) @(posedge clk_i);
  @(posedge clk_i) reset_i <= 1'b1;
  @(posedge clk_i) reset_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0001;
    a_data_i[0].data <= 32'h0000_0001;
    a_data_i[1].key <= 34'h1_0000_0002;
    a_data_i[1].data <= 32'h0000_0002;
    
    b_data_i[0].key <= 34'h1_0000_0001;
    b_data_i[0].data <= 32'h0000_0001;
    b_data_i[1].key <= 34'h1_0000_0002;
    b_data_i[1].data <= 32'h0000_0002;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0003;
    a_data_i[0].data <= 32'h0000_0003;
    a_data_i[1].key <= 34'h1_0000_0004;
    a_data_i[1].data <= 32'h0000_0004;
    
    b_data_i[0].key <= 34'h1_0000_0003;
    b_data_i[0].data <= 32'h0000_0003;
    b_data_i[1].key <= 34'h1_0000_0004;
    b_data_i[1].data <= 32'h0000_0004;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0005;
    a_data_i[0].data <= 32'h0000_0005;
    a_data_i[1].key <= 34'h1_0000_0006;
    a_data_i[1].data <= 32'h0000_0006;
    
    b_data_i[0].key <= 34'h1_0000_0005;
    b_data_i[0].data <= 32'h0000_0005;
    b_data_i[1].key <= 34'h1_0000_0006;
    b_data_i[1].data <= 32'h0000_0006;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
   @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0007;
    a_data_i[0].data <= 32'h0000_0007;
    a_data_i[1].key <= 34'h1_0000_0008;
    a_data_i[1].data <= 32'h0000_0008;
    
    b_data_i[0].key <= 34'h1_0000_0007;
    b_data_i[0].data <= 32'h0000_0007;
    b_data_i[1].key <= 34'h1_0000_0008;
    b_data_i[1].data <= 32'h0000_0008;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;
  repeat (20) begin 
    @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
    @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;
  end 
  
   // Basic Test. Normal operation, with stalls 
  @(posedge clk_i) reset_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0001;
    a_data_i[0].data <= 32'h0000_0001;
    b_data_i[0].key <= 34'h1_0000_0002;
    b_data_i[0].data <= 32'h0000_0002;
    a_data_i[1].key <= 34'h1_0000_0003;
    a_data_i[1].data <= 32'h0000_0003;
    b_data_i[1].key <= 34'h1_0000_0004;
    b_data_i[1].data <= 32'h0000_0004;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0010;
    a_data_i[0].data <= 32'h0000_0010;
    b_data_i[0].key <= 34'h1_0000_000A;
    b_data_i[0].data <= 32'h0000_000A;
    a_data_i[1].key <= 34'h1_0000_0011;
    a_data_i[1].data <= 32'h0000_0011;
    b_data_i[1].key <= 34'h1_0000_000B;
    b_data_i[1].data <= 32'h0000_000B;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0013;
    a_data_i[0].data <= 32'h0000_0013;
    b_data_i[0].key <= 34'h1_0000_0100;
    b_data_i[0].data <= 32'h0000_0100;
    a_data_i[1].key <= 34'h1_0000_0014;
    a_data_i[1].data <= 32'h0000_0014;
    b_data_i[1].key <= 34'h1_0000_0110;
    b_data_i[1].data <= 32'h0000_0110;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0015;
    a_data_i[0].data <= 32'h0000_0015;
    b_data_i[0].key <= 34'h1_0000_1000;
    b_data_i[0].data <= 32'h0000_1000;
    a_data_i[1].key <= 34'h1_0000_0016;
    a_data_i[1].data <= 32'h0000_1016;
    b_data_i[1].key <= 34'h1_0000_1001;
    b_data_i[1].data <= 32'h0000_1001;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;  full_i <= 1'b1; 
  repeat (10) @(posedge clk_i);  
  @(posedge clk_i) full_i <=1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_10F0_0000;
    a_data_i[0].data <= 32'h10F0_0000;
    b_data_i[0].key <= 34'h1_0000_1009;
    b_data_i[0].data <= 32'h0000_1009;
    a_data_i[1].key <= 34'h1_10F0_0001;
    a_data_i[1].data <= 32'h10F0_0001;
    b_data_i[1].key <= 34'h1_0000_100A;
    b_data_i[1].data <= 32'h0000_100A;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
   @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
   @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
   @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  repeat (20) @(posedge clk_i);
  @(posedge clk_i) reset_i <= 1'b1;
  @(posedge clk_i) reset_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0001;
    a_data_i[0].data <= 32'h0000_0001;
    a_data_i[1].key <= 34'h1_0000_0002;
    a_data_i[1].data <= 32'h0000_0002;
    
    b_data_i[0].key <= 34'h1_0000_0001;
    b_data_i[0].data <= 32'h0000_0001;
    b_data_i[1].key <= 34'h1_0000_0002;
    b_data_i[1].data <= 32'h0000_0002;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; full_i <= 1'b1;
    a_data_i[0].key <= 34'h1_0000_0003;
    a_data_i[0].data <= 32'h0000_0003;
    a_data_i[1].key <= 34'h1_0000_0004;
    a_data_i[1].data <= 32'h0000_0004;
    
    b_data_i[0].key <= 34'h1_0000_0003;
    b_data_i[0].data <= 32'h0000_0003;
    b_data_i[1].key <= 34'h1_0000_0004;
    b_data_i[1].data <= 32'h0000_0004;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
  @(posedge clk_i) full_i <= 1'b0; 
  @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0005;
    a_data_i[0].data <= 32'h0000_0005;
    a_data_i[1].key <= 34'h1_0000_0006;
    a_data_i[1].data <= 32'h0000_0006;
    
    b_data_i[0].key <= 34'h1_0000_0005;
    b_data_i[0].data <= 32'h0000_0005;
    b_data_i[1].key <= 34'h1_0000_0006;
    b_data_i[1].data <= 32'h0000_0006;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0; 
   @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h1_0000_0007;
    a_data_i[0].data <= 32'h0000_0007;
    a_data_i[1].key <= 34'h1_0000_0008;
    a_data_i[1].data <= 32'h0000_0008;
    
    b_data_i[0].key <= 34'h1_0000_0007;
    b_data_i[0].data <= 32'h0000_0007;
    b_data_i[1].key <= 34'h1_0000_0008;
    b_data_i[1].data <= 32'h0000_0008;
  @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;
  repeat (20) begin 
    @(posedge clk_i) a_v_i <= 1'b1; b_v_i <= 1'b1; 
    a_data_i[0].key <= 34'h3_0000_0000;
    a_data_i[0].data <= 32'h0000_0000;
    a_data_i[1].key <= 34'h3_0000_0000;
    a_data_i[1].data <= 32'h0000_0000;
    
    b_data_i[0].key <= 34'h3_0000_0000;
    b_data_i[0].data <= 32'h0000_0000;
    b_data_i[1].key <= 34'h3_0000_0000;
    b_data_i[1].data <= 32'h0000_0000;
    @(posedge clk_i) a_v_i <= 1'b0; b_v_i <= 1'b0;
  end 
  $stop; 
  end 
endmodule






