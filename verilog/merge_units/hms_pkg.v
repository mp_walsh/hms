package hms_pkg;


parameter e_key_width_p     = 34;
parameter e_data_width_p    = 32; 
parameter e_width_p = (e_key_width_p + e_data_width_p);
// "element" struct 
typedef struct packed {
	// bit 33 of key is finish
	// bit 32 of key is valid_not_dummy
	logic[e_key_width_p-1:0] key;
	logic[e_data_width_p-1:0] data; 
} e_s; 

endpackage


