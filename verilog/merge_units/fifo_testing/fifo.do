# Create work library
vlib work

# Compile Verilog
#     All Verilog files that are part of this design should have
#     their own "vlog" line below.
vlog "../hms_pkg.v"
vlog "../bsg_defines.v"
vlog "../bsg_circular_ptr.v"
vlog "../bsg_fifo_tracker.v"
vlog "../bsg_mem_1r1w_synth.v"
vlog "../bsg_mem_1r1w.v"
vlog "../bsg_fifo_1r1w_small.v"
# Call vsim to invoke simulator
#     Make sure the last item on the line is the name of the
#     testbench module you want to execute.
vsim -voptargs="+acc" -t 1ps -lib work bsg_fifo_1r1w_small_tb

# Source the wave do file
#     This should be the file that sets up the signal window for
#     the module you are testing.
do fifo_wave.do

# Set the window types
view wave
view structure
view signals

# Run the simulation
run -all

# End
