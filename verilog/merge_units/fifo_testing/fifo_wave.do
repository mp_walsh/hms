onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /bsg_fifo_1r1w_small_tb/clk_i
add wave -noupdate /bsg_fifo_1r1w_small_tb/deque_n
add wave -noupdate /bsg_fifo_1r1w_small_tb/empty_n
add wave -noupdate /bsg_fifo_1r1w_small_tb/full_o
add wave -noupdate /bsg_fifo_1r1w_small_tb/v_i
add wave -noupdate /bsg_fifo_1r1w_small_tb/reset_i
add wave -noupdate /bsg_fifo_1r1w_small_tb/ready_o
add wave -noupdate /bsg_fifo_1r1w_small_tb/v_n
add wave -noupdate -radix hexadecimal -childformat {{{/bsg_fifo_1r1w_small_tb/data_i[3]} -radix hexadecimal} {{/bsg_fifo_1r1w_small_tb/data_i[2]} -radix hexadecimal} {{/bsg_fifo_1r1w_small_tb/data_i[1]} -radix hexadecimal} {{/bsg_fifo_1r1w_small_tb/data_i[0]} -radix hexadecimal}} -expand -subitemconfig {{/bsg_fifo_1r1w_small_tb/data_i[3]} {-height 15 -radix hexadecimal} {/bsg_fifo_1r1w_small_tb/data_i[2]} {-height 15 -radix hexadecimal} {/bsg_fifo_1r1w_small_tb/data_i[1]} {-height 15 -radix hexadecimal} {/bsg_fifo_1r1w_small_tb/data_i[0]} {-height 15 -radix hexadecimal}} /bsg_fifo_1r1w_small_tb/data_i
add wave -noupdate -radix hexadecimal -childformat {{{/bsg_fifo_1r1w_small_tb/data_o[3]} -radix hexadecimal} {{/bsg_fifo_1r1w_small_tb/data_o[2]} -radix hexadecimal} {{/bsg_fifo_1r1w_small_tb/data_o[1]} -radix hexadecimal} {{/bsg_fifo_1r1w_small_tb/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/bsg_fifo_1r1w_small_tb/data_o[3]} {-height 15 -radix hexadecimal} {/bsg_fifo_1r1w_small_tb/data_o[2]} {-height 15 -radix hexadecimal} {/bsg_fifo_1r1w_small_tb/data_o[1]} {-height 15 -radix hexadecimal} {/bsg_fifo_1r1w_small_tb/data_o[0]} {-height 15 -radix hexadecimal}} /bsg_fifo_1r1w_small_tb/data_o
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/clk_i
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/reset_i
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/v_i
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/ready_o
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/data_i
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/v_o
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/data_o
add wave -noupdate /bsg_fifo_1r1w_small_tb/DUT/yumi_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {92 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1 ns}
