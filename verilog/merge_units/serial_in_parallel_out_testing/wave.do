onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /bsg_serial_in_parallel_out_tb/clk_i
add wave -noupdate -radix hexadecimal /bsg_serial_in_parallel_out_tb/reset_i
add wave -noupdate -radix hexadecimal /bsg_serial_in_parallel_out_tb/valid_i
add wave -noupdate -radix hexadecimal /bsg_serial_in_parallel_out_tb/yumi_cnt_i
add wave -noupdate -radix hexadecimal /bsg_serial_in_parallel_out_tb/data_i
add wave -noupdate -radix hexadecimal -childformat {{{/bsg_serial_in_parallel_out_tb/data_o[1]} -radix hexadecimal} {{/bsg_serial_in_parallel_out_tb/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/bsg_serial_in_parallel_out_tb/data_o[1]} {-radix hexadecimal} {/bsg_serial_in_parallel_out_tb/data_o[0]} {-radix hexadecimal}} /bsg_serial_in_parallel_out_tb/data_o
add wave -noupdate -radix hexadecimal /bsg_serial_in_parallel_out_tb/valid_o
add wave -noupdate -radix hexadecimal /bsg_serial_in_parallel_out_tb/ready_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {126 ps}
