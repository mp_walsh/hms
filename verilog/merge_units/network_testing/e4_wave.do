onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /e4_merge_network_tb/e_height_tp
add wave -noupdate /e4_merge_network_tb/clk_i
add wave -noupdate /e4_merge_network_tb/reset_i
add wave -noupdate -radix hexadecimal /e4_merge_network_tb/data_i
add wave -noupdate -radix hexadecimal -childformat {{{/e4_merge_network_tb/data_o[3]} -radix hexadecimal} {{/e4_merge_network_tb/data_o[2]} -radix hexadecimal} {{/e4_merge_network_tb/data_o[1]} -radix hexadecimal} {{/e4_merge_network_tb/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/e4_merge_network_tb/data_o[3]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/data_o[2]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/data_o[1]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/data_o[0]} {-height 15 -radix hexadecimal}} /e4_merge_network_tb/data_o
add wave -noupdate -radix hexadecimal -childformat {{{/e4_merge_network_tb/DUT/stage_data_o_n[0][3]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/stage_data_o_n[0][2]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/stage_data_o_n[0][1]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/stage_data_o_n[0][0]} -radix hexadecimal}} -expand -subitemconfig {{/e4_merge_network_tb/DUT/stage_data_o_n[0][3]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/DUT/stage_data_o_n[0][2]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/DUT/stage_data_o_n[0][1]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/DUT/stage_data_o_n[0][0]} {-height 15 -radix hexadecimal}} {/e4_merge_network_tb/DUT/stage_data_o_n[0]}
add wave -noupdate -radix hexadecimal -childformat {{{/e4_merge_network_tb/DUT/stage_data_o_n[1][3]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/stage_data_o_n[1][2]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/stage_data_o_n[1][1]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/stage_data_o_n[1][0]} -radix hexadecimal}} -expand -subitemconfig {{/e4_merge_network_tb/DUT/stage_data_o_n[1][3]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/DUT/stage_data_o_n[1][2]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/DUT/stage_data_o_n[1][1]} {-height 15 -radix hexadecimal} {/e4_merge_network_tb/DUT/stage_data_o_n[1][0]} {-height 15 -radix hexadecimal}} {/e4_merge_network_tb/DUT/stage_data_o_n[1]}
add wave -noupdate -expand -group s0 -radix hexadecimal -childformat {{{/e4_merge_network_tb/DUT/data_i[3]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/data_i[2]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/data_i[1]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/data_i[0]} -radix hexadecimal}} -expand -subitemconfig {{/e4_merge_network_tb/DUT/data_i[3]} {-radix hexadecimal} {/e4_merge_network_tb/DUT/data_i[2]} {-radix hexadecimal} {/e4_merge_network_tb/DUT/data_i[1]} {-radix hexadecimal} {/e4_merge_network_tb/DUT/data_i[0]} {-radix hexadecimal}} /e4_merge_network_tb/DUT/data_i
add wave -noupdate -expand -group s0 -radix hexadecimal -childformat {{{/e4_merge_network_tb/DUT/data_o[3]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/data_o[2]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/data_o[1]} -radix hexadecimal} {{/e4_merge_network_tb/DUT/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/e4_merge_network_tb/DUT/data_o[3]} {-radix hexadecimal} {/e4_merge_network_tb/DUT/data_o[2]} {-radix hexadecimal} {/e4_merge_network_tb/DUT/data_o[1]} {-radix hexadecimal} {/e4_merge_network_tb/DUT/data_o[0]} {-radix hexadecimal}} /e4_merge_network_tb/DUT/data_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 157
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {272 ps}
