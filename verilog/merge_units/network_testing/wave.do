onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /e2_merge_network_tb/e_height_tp
add wave -noupdate /e2_merge_network_tb/clk_i
add wave -noupdate /e2_merge_network_tb/reset_i
add wave -noupdate /e2_merge_network_tb/stall_i
add wave -noupdate -radix hexadecimal /e2_merge_network_tb/data_i
add wave -noupdate -radix hexadecimal -childformat {{{/e2_merge_network_tb/data_o[1]} -radix hexadecimal} {{/e2_merge_network_tb/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/e2_merge_network_tb/data_o[1]} {-radix hexadecimal} {/e2_merge_network_tb/data_o[0]} {-radix hexadecimal}} /e2_merge_network_tb/data_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {35 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {74 ps}
