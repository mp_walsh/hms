module merge_tree_4
  import hms_pkg::*;
  (input clk_i, 
   input reset_i,
   input full_i, 
   input e_s [1:0]a_data_i, // lower bus bits are smaller values 
   input [1:0] a_v_i,
   input e_s [1:0]b_data_i, 
   input [1:0] b_v_i, 
   output e_s [3:0] data_o,
   output  [3:0] full_o  
  );
  
  
  e_s [1:0] a_data_n; 
  e_s [1:0] b_data_n; 
  logic a_full_n, b_full_n;
  
  merge_tree_leaf #(.e_height_p(2)) A
  (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.a_v_i(a_v_i[0])
   ,.b_v_i(a_v_i[1])
   ,.full_i(a_full_n) 
   ,.a_data_i(a_data_i[0])
   ,.b_data_i(a_data_i[1])
   ,.data_o(a_data_n[1:0])
   ,.a_full_o(full_o[0])
   ,.b_full_o(full_o[1])
  );
  
  merge_tree_leaf #(.e_height_p(2)) B
  (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.a_v_i(b_v_i[0])
   ,.b_v_i(b_v_i[1])
   ,.full_i(b_full_n) 
   ,.a_data_i(b_data_i[0])
   ,.b_data_i(b_data_i[1])
   ,.data_o(b_data_n)
   ,.a_full_o(full_o[2])
   ,.b_full_o(full_o[3])
  );
  
  // bitwise or the valid bit of each key to get valid signal
  logic a_v_n, b_v_n;
  assign a_v_n =  a_data_n[0].key[e_key_width_p-2] | a_data_n[1].key[e_key_width_p-2];
  assign b_v_n = b_data_n[0].key[e_key_width_p-2] | b_data_n[1].key[e_key_width_p-2];

  merge_tree_leaf #(.e_height_p(4)) F
  (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.a_v_i(a_v_n) 
   ,.b_v_i(b_v_n)
   ,.full_i(full_i) 
   ,.a_data_i(a_data_n)
   ,.b_data_i(b_data_n)
   ,.data_o(data_o)
   ,.a_full_o(a_full_n)
   ,.b_full_o(b_full_n)
  );  
endmodule 

module merge_tree_4_tb 
    import hms_pkg::*; ();
    //inputs 
    logic clk_i, reset_i, full_i;
    e_s [1:0] a_data_i, b_data_i; 
    logic [1:0] a_v_i, b_v_i;
   
    // outputs 
    e_s [3:0] data_o; 
    logic [3:0] full_o;  
   
   merge_tree_4 DUT
   (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.full_i(full_i)
   ,.a_data_i(a_data_i)
   ,.a_v_i(a_v_i)
   ,.b_data_i(b_data_i)
   ,.b_v_i(b_v_i)
   ,.data_o(data_o)
   ,.full_o(full_o)
   ); 
      
   initial begin
    clk_i <= 1'b0; 
    reset_i <= 1'b1; 
    full_i <= 1'b0; 
    a_v_i <= 2'b00;
    b_v_i <= 2'b00; 
    forever #5 clk_i <= ~clk_i; 
   end 
   
   initial begin
    // basic test, normal operation
    // lists are "nice" no stalling
    @(posedge clk_i) reset_i <= 1'b0; 
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; 
      a_data_i[0].key <= 34'h1_0000_0001;
      a_data_i[0].data <= 32'h0000_0001;
      a_data_i[1].key <= 34'h1_0000_0002; 
      a_data_i[1].data <= 34'h0000_0002; 
      
      b_data_i[0].key <= 34'h1_0000_0001;
      b_data_i[0].data <= 32'h0000_0001;
      b_data_i[1].key <= 34'h1_0000_0002; 
      b_data_i[1].data <= 34'h000_0002; 
      
    @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; 
      a_data_i[0].key <= 34'h1_0000_0003;
      a_data_i[0].data <= 32'h0000_0003;
      a_data_i[1].key <= 34'h1_0000_0004; 
      a_data_i[1].data <= 34'h0000_0004; 
      
      b_data_i[0].key <= 34'h1_0000_0003;
      b_data_i[0].data <= 32'h0000_0003;
      b_data_i[1].key <= 34'h1_0000_0004; 
      b_data_i[1].data <= 34'h0000_0004; 
      
    @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; 
      a_data_i[0].key <= 34'h1_0000_0005;
      a_data_i[0].data <= 32'h0000_0005;
      a_data_i[1].key <= 34'h1_0000_0006; 
      a_data_i[1].data <= 34'h0000_0006; 
      
      b_data_i[0].key <= 34'h1_0000_0005;
      b_data_i[0].data <= 32'h0000_0005;
      b_data_i[1].key <= 34'h1_0000_0006; 
      b_data_i[1].data <= 34'h0000_0006;
 
    @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; 
      a_data_i[0].key <= 34'h1_0000_0007;
      a_data_i[0].data <= 32'h0000_0007;
      a_data_i[1].key <= 34'h1_0000_0008; 
      a_data_i[1].data <= 34'h0000_0008; 
      
      b_data_i[0].key <= 34'h1_0000_0007;
      b_data_i[0].data <= 32'h0000_0007;
      b_data_i[1].key <= 34'h1_0000_0008; 
      b_data_i[1].data <= 34'h0000_0008;
    

    repeat (25) begin 
      @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
      @(posedge clk_i) 
        a_v_i <= 2'b11; b_v_i <= 2'b11; 
        a_data_i[0].key <= 34'h3_0000_0000;
        a_data_i[0].data <= 32'h0000_0000;
        a_data_i[1].key <= 34'h3_0000_0000; 
        a_data_i[1].data <= 34'h0000_0000; 
      
        b_data_i[0].key <= 34'h3_0000_0000;
        b_data_i[0].data <= 32'h0000_0000;
        b_data_i[1].key <= 34'h3_0000_0000; 
        b_data_i[1].data <= 34'h0000_0000;
    end 
  
  
  // same test as before, but with frequent stalls
  @(posedge clk_i) reset_i <= 1'b1;  a_v_i <= 2'b00; b_v_i <= 2'b00; 
  @(posedge clk_i) reset_i <= 1'b0; 
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; 
      a_data_i[0].key <= 34'h1_0000_0001;
      a_data_i[0].data <= 32'h0000_0001;
      a_data_i[1].key <= 34'h1_0000_0002; 
      a_data_i[1].data <= 34'h0000_0002; 
      
      b_data_i[0].key <= 34'h1_0000_0001;
      b_data_i[0].data <= 32'h0000_0001;
      b_data_i[1].key <= 34'h1_0000_0002; 
      b_data_i[1].data <= 34'h000_0002; 
      
    @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; 
      a_data_i[0].key <= 34'h1_0000_0003;
      a_data_i[0].data <= 32'h0000_0003;
      a_data_i[1].key <= 34'h1_0000_0004; 
      a_data_i[1].data <= 34'h0000_0004; 
      
      b_data_i[0].key <= 34'h1_0000_0003;
      b_data_i[0].data <= 32'h0000_0003;
      b_data_i[1].key <= 34'h1_0000_0004; 
      b_data_i[1].data <= 34'h0000_0004; 
      
    @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; full_i <= 1'b1; 
      a_data_i[0].key <= 34'h1_0000_0005;
      a_data_i[0].data <= 32'h0000_0005;
      a_data_i[1].key <= 34'h1_0000_0006; 
      a_data_i[1].data <= 34'h0000_0006; 
      
      b_data_i[0].key <= 34'h1_0000_0005;
      b_data_i[0].data <= 32'h0000_0005;
      b_data_i[1].key <= 34'h1_0000_0006; 
      b_data_i[1].data <= 34'h0000_0006; 
    @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
    @(posedge clk_i) full_i <= 1'b0; 
    @(posedge clk_i) full_i <= 1'b1; 
    @(posedge clk_i) 
    @(posedge clk_i) 
      a_v_i <= 2'b11; b_v_i <= 2'b11; 
      a_data_i[0].key <= 34'h1_0000_0007;
      a_data_i[0].data <= 32'h0000_0007;
      a_data_i[1].key <= 34'h1_0000_0008; 
      a_data_i[1].data <= 34'h0000_0008; 
      
      b_data_i[0].key <= 34'h1_0000_0007;
      b_data_i[0].data <= 32'h0000_0007;
      b_data_i[1].key <= 34'h1_0000_0008; 
      b_data_i[1].data <= 34'h0000_0008;
    @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
    repeat (25) @(posedge clk_i);
    @(posedge clk_i) full_i <= 1'b0; 
    repeat (25) begin 
      @(posedge clk_i)  a_v_i <= 2'b00; b_v_i <= 2'b00;  
      @(posedge clk_i) 
        a_v_i <= 2'b11; b_v_i <= 2'b11; 
        a_data_i[0].key <= 34'h3_0000_0000;
        a_data_i[0].data <= 32'h0000_0000;
        a_data_i[1].key <= 34'h3_0000_0000; 
        a_data_i[1].data <= 34'h0000_0000; 
      
        b_data_i[0].key <= 34'h3_0000_0000;
        b_data_i[0].data <= 32'h0000_0000;
        b_data_i[1].key <= 34'h3_0000_0000; 
        b_data_i[1].data <= 34'h0000_0000;
    end 
   $stop; 
   end 
      
endmodule