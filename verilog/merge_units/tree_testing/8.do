# Create work library
vlib work

# Compile Verilog
#     All Verilog files that are part of this design should have
#     their own "vlog" line below.
vlog "../hms_pkg.v"
vlog "../bsg_defines.v"
vlog "../bsg_mux.v"
vlog "../bsg_circular_ptr.v"
vlog "../bsg_mem_1r1w_synth.v"
vlog "../bsg_mem_1r1w.v"
vlog "../bsg_fifo_tracker.v"
vlog "../bsg_fifo_1r1w_small.v"
vlog "../bsg_serial_in_parallel_out.v"
vlog "../merge_network_stage.v"
vlog "../merge_network.v"
vlog "../merge_logic.v"
vlog "../merge_tree_leaf.v"
vlog "../merge_tree_4.v" 
vlog "../merge_tree_8.v"

# Call vsim to invoke simulator
#     Make sure the last item on the line is the name of the
#     testbench module you want to execute.
vsim -voptargs="+acc" -t 1ps -lib work merge_tree_8_tb

# Source the wave do file
#     This should be the file that sets up the signal window for
#     the module you are testing.
do 8_wave.do

# Set the window types
view wave
view structure
view signals

# Run the simulation
run -all

# End
