onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /merge_tree_8_tb/clk_i
add wave -noupdate /merge_tree_8_tb/reset_i
add wave -noupdate /merge_tree_8_tb/full_i
add wave -noupdate -radix hexadecimal -childformat {{{/merge_tree_8_tb/data_i[7]} -radix hexadecimal} {{/merge_tree_8_tb/data_i[6]} -radix hexadecimal} {{/merge_tree_8_tb/data_i[5]} -radix hexadecimal} {{/merge_tree_8_tb/data_i[4]} -radix hexadecimal} {{/merge_tree_8_tb/data_i[3]} -radix hexadecimal} {{/merge_tree_8_tb/data_i[2]} -radix hexadecimal} {{/merge_tree_8_tb/data_i[1]} -radix hexadecimal} {{/merge_tree_8_tb/data_i[0]} -radix hexadecimal}} -expand -subitemconfig {{/merge_tree_8_tb/data_i[7]} {-height 15 -radix hexadecimal} {/merge_tree_8_tb/data_i[6]} {-height 15 -radix hexadecimal} {/merge_tree_8_tb/data_i[5]} {-height 15 -radix hexadecimal} {/merge_tree_8_tb/data_i[4]} {-height 15 -radix hexadecimal} {/merge_tree_8_tb/data_i[3]} {-height 15 -radix hexadecimal} {/merge_tree_8_tb/data_i[2]} {-height 15 -radix hexadecimal} {/merge_tree_8_tb/data_i[1]} {-height 15 -radix hexadecimal} {/merge_tree_8_tb/data_i[0]} {-height 15 -radix hexadecimal}} /merge_tree_8_tb/data_i
add wave -noupdate /merge_tree_8_tb/v_i
add wave -noupdate /merge_tree_8_tb/data_o
add wave -noupdate /merge_tree_8_tb/full_o
add wave -noupdate /merge_tree_8_tb/a_value
add wave -noupdate /merge_tree_8_tb/b_value
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/clk_i
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/reset_i
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/a_v_i
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/b_v_i
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/full_i
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/a_data_i
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/b_data_i
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/data_o
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/a_full_o
add wave -noupdate -expand -group F /merge_tree_8_tb/DUT/F/b_full_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1 ns}
