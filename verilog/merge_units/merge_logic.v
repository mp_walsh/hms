/* HMS merge network
 * Merge logic topology based on HPHMS 2017 FCCM
 * See figure 10
 * Author: Michael Walsh mike0698@uw.edu
 * Date: 2/24/2018 
 */
 

module merge_logic 
	import hms_pkg::*;
  #(parameter e_height_p = -1)
  (input                       clk_i,
	 input                       reset_i,
   input                       full_i,
   input e_s   [e_height_p-1:0]a_data_i,
   input e_s   [e_height_p-1:0]b_data_i,  
   input                       a_v_i,
   input                       b_v_i, 
   output                      b_full_o,
   output                      a_full_o,
   output e_s  [e_height_p-1:0]data_o 
  );
   parameter els_p = 16; 
   
   e_s [e_height_p-1:0] a_bus_premux_n, a_bus_stage1_n, a_bus_stage1_r, a_bus_stage2_r;
   e_s [e_height_p-1:0] b_bus_premux_n, b_bus_stage1_n, b_bus_stage1_r, b_bus_stage2_r;
   e_s [e_height_p-1:0] merge_bus_n, data_o_pre_mux_n;
   logic deque_n, deque_r, stall_r;  
   logic a_not_empty_n, b_not_empty_n, a_ready_n, b_ready_n; 
   assign a_full_o = ~a_ready_n; 
   assign b_full_o = ~b_ready_n; 
   
   bsg_fifo_1r1w_small #(.width_p(e_height_p * e_width_p)
                            , .els_p(els_p)
                            , .ready_THEN_valid_p(0)
                            ) fifo_a 
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.v_i(a_v_i)
    ,.ready_o(a_ready_n)
    ,.data_i(a_data_i)
    ,.v_o(a_not_empty_n)
    ,.data_o(a_bus_premux_n)
    ,.yumi_i(deque_r & a_not_empty_n & ~stall_r)
    );
    
    bsg_fifo_1r1w_small #(.width_p(e_height_p * e_width_p)
                            , .els_p(els_p)
                            , .ready_THEN_valid_p(0)
                            ) fifo_b 
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.v_i(b_v_i)
    ,.ready_o(b_ready_n)
    ,.data_i(b_data_i)
    ,.v_o(b_not_empty_n)
    ,.data_o(b_bus_premux_n)
    ,.yumi_i(~deque_r & b_not_empty_n & ~stall_r)
    );
    
    bsg_mux #(.width_p(e_height_p * e_width_p)
                                   ,.els_p(2)) a_sel_mux 
    (.data_i({a_bus_premux_n, {(e_height_p*e_width_p){1'b0}}})
    ,.sel_i(a_not_empty_n)
    ,.data_o(a_bus_stage1_n)
    );
    
    bsg_mux #(.width_p(e_height_p * e_width_p)
                                   ,.els_p(2)) b_sel_mux 
    (.data_i({b_bus_premux_n, {(e_height_p*e_width_p){1'b0}}})
    ,.sel_i(b_not_empty_n)
    ,.data_o(b_bus_stage1_n)
    );
    
    bsg_mux #(.width_p(1)
             ,.els_p(2)) deque_select_mux
    (.data_i({(a_bus_stage1_r[0].key <= b_bus_stage2_r[0].key), (a_bus_stage2_r[0].key <= b_bus_stage1_r[0].key)})
    ,.sel_i(deque_r)
    ,.data_o(deque_n)
    );
    
    
    bsg_mux #(.width_p(e_height_p * e_width_p)
             ,.els_p(2)) merge_bus_select_mux 
    (.data_i({a_bus_stage2_r, b_bus_stage2_r})
    ,.sel_i(deque_r)
    ,.data_o(merge_bus_n)
    );
    
    

    merge_network #(.e_height_p(e_height_p)) merge_n 
    (.clk_i(clk_i)
	  ,.reset_i(reset_i)
    ,.stall_i(stall_r)
	  ,.data_i(merge_bus_n)
    ,.data_o(data_o_pre_mux_n)
    );
    
    bsg_mux #(.width_p(e_height_p * e_width_p)
             ,.els_p(2)) output_mux  
    (.data_i({{(e_height_p * e_width_p){1'b0}}, data_o_pre_mux_n})
    ,.sel_i(stall_r)
    ,.data_o(data_o)
    );
    
    
    always_ff @(posedge clk_i) begin 
      if(reset_i) begin
        a_bus_stage1_r <= {(e_width_p){1'b0}}; 
        a_bus_stage2_r <= {(e_width_p){1'b0}}; 
        b_bus_stage1_r <= {(e_width_p){1'b0}};
        b_bus_stage2_r <= {(e_width_p){1'b0}};
        deque_r <= 1'b0; 
        stall_r <= 1'b0; 
      end else if (deque_r && !stall_r) begin 
        a_bus_stage1_r <= a_bus_stage1_n;
        a_bus_stage2_r <= a_bus_stage1_r; 
        deque_r <= deque_n; 
        stall_r <= full_i; 
      end else if (!deque_r && !stall_r) begin
        b_bus_stage1_r <= b_bus_stage1_n;
        b_bus_stage2_r <= b_bus_stage1_r; 
        deque_r <= deque_n; 
        stall_r <= full_i; 
      end else begin 
        stall_r <= full_i; 
      end 
    end 
endmodule 

module merge_logic_tb import hms_pkg::*; (); 
  // inputs 
  parameter e_height_p = 4;
  logic clk_i, reset_i, full_i;
  e_s   [e_height_p-1:0]a_data_i;
  e_s   [e_height_p-1:0]b_data_i;  
  logic a_v_i, b_v_i, a_v_tmp, b_v_tmp;
  assign a_v_i = a_v_tmp; //a_data_i[0].key[e_key_width_p-2] & a_v_tmp;
  assign b_v_i = b_v_tmp; //b_data_i[0].key[e_key_width_p-2] & b_v_tmp;
  // outputs 
  logic b_full_o, a_full_o;
  e_s  [e_height_p-1:0]data_o;

  merge_logic #(.e_height_p(e_height_p)) DUT 
  (.clk_i(clk_i)
	,.reset_i(reset_i)
  ,.full_i(full_i)
  ,.a_data_i(a_data_i)
  ,.b_data_i(b_data_i)  
  ,.a_v_i(a_v_i)
  ,.b_v_i(b_v_i) 
  ,.b_full_o(b_full_o)
  ,.a_full_o(a_full_o)
  ,.data_o(data_o) 
  );
  
  initial begin
    clk_i <= 1'b0; reset_i <= 1'b1; full_i <= 1'b0; 
    a_v_tmp <= 1'b0; 
    b_v_tmp <= 1'b0; 
    a_data_i[0].key  <= 34'h0_0000_0000; 
    a_data_i[0].data <= 32'h0000_0000; 
    a_data_i[1].key  <= 34'h0_0000_0000; 
    a_data_i[1].data <= 32'h0000_0000; 
    a_data_i[2].key  <= 34'h0_0000_0000; 
    a_data_i[2].data <= 32'h0000_0000; 
    a_data_i[3].key  <= 34'h0_0000_0000; 
    a_data_i[3].data <= 32'h0000_0000; 
    
    b_data_i[0].key  <= 34'h0_0000_0000; 
    b_data_i[0].data <= 32'h0000_0000; 
    b_data_i[1].key  <= 34'h0_0000_0000; 
    b_data_i[1].data <= 32'h0000_0000; 
    b_data_i[2].key  <= 34'h0_0000_0000; 
    b_data_i[2].data <= 32'h0000_0000; 
    b_data_i[3].key  <= 34'h0_0000_0000; 
    b_data_i[3].data <= 32'h0000_0000;
    
    forever #5 clk_i <= ~clk_i; 
  end 
  
  initial begin
    @(posedge clk_i); reset_i <= 1'b0;
    repeat (20) @(posedge clk_i); 
    @(posedge clk_i); 
    a_v_tmp <= 1'b1; 
    b_v_tmp <= 1'b1; 
    a_data_i[0].key  <= 34'h1_0000_0001; 
    a_data_i[0].data <= 32'h0000_0001; 
    a_data_i[1].key  <= 34'h1_0000_0003; 
    a_data_i[1].data <= 32'h0000_0003; 
    a_data_i[2].key  <= 34'h1_0000_0005; 
    a_data_i[2].data <= 32'h0000_0005; 
    a_data_i[3].key  <= 34'h1_0000_0007; 
    a_data_i[3].data <= 32'h0000_0007; 
    
    b_data_i[0].key  <= 34'h1_0000_0002; 
    b_data_i[0].data <= 32'h0000_0002; 
    b_data_i[1].key  <= 34'h1_0000_0004; 
    b_data_i[1].data <= 32'h0000_0004; 
    b_data_i[2].key  <= 34'h1_0000_0006; 
    b_data_i[2].data <= 32'h0000_0006; 
    b_data_i[3].key  <= 34'h1_0000_0008; 
    b_data_i[3].data <= 32'h0000_0008;
    @(posedge clk_i); 
    full_i <= 1'b1;
    a_v_tmp <= 1'b0; 
    b_v_tmp <=  1'b0;
    repeat (10) @(posedge clk_i); 
    @(posedge clk_i);
    full_i <= 1'b0; 
    a_v_tmp <= 1'b1; 
    b_v_tmp <= 1'b1; 
    a_data_i[0].key  <= 34'h1_0000_000A; 
    a_data_i[0].data <= 32'h0000_000A; 
    a_data_i[1].key  <= 34'h1_0000_000C; 
    a_data_i[1].data <= 32'h0000_000C; 
    a_data_i[2].key  <= 34'h1_0000_000E; 
    a_data_i[2].data <= 32'h0000_000E; 
    a_data_i[3].key  <= 34'h1_0000_0010; 
    a_data_i[3].data <= 32'h0000_0010; 
    
    b_data_i[0].key  <= 34'h1_0000_0009; 
    b_data_i[0].data <= 32'h0000_0009; 
    b_data_i[1].key  <= 34'h1_0000_000B; 
    b_data_i[1].data <= 32'h0000_000B; 
    b_data_i[2].key  <= 34'h1_0000_000D; 
    b_data_i[2].data <= 32'h0000_000D; 
    b_data_i[3].key  <= 34'h1_0000_000F; 
    b_data_i[3].data <= 32'h0000_000F;
    @(posedge clk_i); 
    a_v_tmp <= 1'b0; 
    b_v_tmp <=  1'b0; 
    @(posedge clk_i);
    a_v_tmp <= 1'b1; 
    b_v_tmp <=  1'b1; 
    a_data_i[0].key  <= 34'h3_0000_0000; 
    a_data_i[0].data <= 32'h0000_0000; 
    a_data_i[1].key  <= 34'h3_0000_0000; 
    a_data_i[1].data <= 32'h0000_0000; 
    a_data_i[2].key  <= 34'h3_0000_0000; 
    a_data_i[2].data <= 32'h0000_0000; 
    a_data_i[3].key  <= 34'h3_0000_0000; 
    a_data_i[3].data <= 32'h0000_0000; 
    
    b_data_i[0].key  <= 34'h3_0000_0000; 
    b_data_i[0].data <= 32'h0000_0000; 
    b_data_i[1].key  <= 34'h3_0000_0000; 
    b_data_i[1].data <= 32'h0000_0000; 
    b_data_i[2].key  <= 34'h3_0000_0000; 
    b_data_i[2].data <= 32'h0000_0000; 
    b_data_i[3].key  <= 34'h3_0000_0000; 
    b_data_i[3].data <= 32'h0000_0000;
    @(posedge clk_i); 
    a_v_tmp <= 1'b0; 
    b_v_tmp <=  1'b0; 
    repeat(100) @(posedge clk_i); 
    $stop; 
  end 
endmodule 

module merge_logic2_tb import hms_pkg::*; (); 
  // inputs 
  parameter e_height_p = 2;
  logic clk_i, reset_i, full_i;
  e_s   [e_height_p-1:0]a_data_i;
  e_s   [e_height_p-1:0]b_data_i;  
  logic a_v_i, b_v_i, a_v_tmp, b_v_tmp;
  assign a_v_i = a_v_tmp; //a_data_i[0].key[e_key_width_p-2] & a_v_tmp;
  assign b_v_i = b_v_tmp; //b_data_i[0].key[e_key_width_p-2] & b_v_tmp;
  // outputs 
  logic b_full_o, a_full_o;
  e_s  [e_height_p-1:0]data_o;

  merge_logic #(.e_height_p(e_height_p)) DUT 
  (.clk_i(clk_i)
	,.reset_i(reset_i)
  ,.full_i(full_i)
  ,.a_data_i(a_data_i) // bitwise or the valid bit of each key on the bus 
  ,.b_data_i(b_data_i)  
  ,.a_v_i(a_v_i)
  ,.b_v_i(b_v_i) 
  ,.b_full_o(b_full_o)
  ,.a_full_o(a_full_o)
  ,.data_o(data_o) 
  );
  
  initial begin
    clk_i <= 1'b0; reset_i <= 1'b1; full_i <= 1'b0; 
    a_v_tmp <= 1'b0; 
    b_v_tmp <= 1'b0; 
    a_data_i[0].key  <= 34'h0_0000_0000; 
    a_data_i[0].data <= 32'h0000_0000; 
    a_data_i[1].key  <= 34'h0_0000_0000; 
    a_data_i[1].data <= 32'h0000_0000; 
  
    
    b_data_i[0].key  <= 34'h0_0000_0000; 
    b_data_i[0].data <= 32'h0000_0000; 
    b_data_i[1].key  <= 34'h0_0000_0000; 
    b_data_i[1].data <= 32'h0000_0000; 
    forever #5 clk_i <= ~clk_i; 
  end 
  
  initial begin
    @(posedge clk_i); reset_i <= 1'b0;
    repeat (20) @(posedge clk_i); 
    @(posedge clk_i); 
    a_v_tmp <= 1'b1; 
    b_v_tmp <= 1'b1; 
    a_data_i[0].key  <= 34'h1_0000_0001; 
    a_data_i[0].data <= 32'h0000_0001; 
    a_data_i[1].key  <= 34'h1_0000_0006; 
    a_data_i[1].data <= 32'h0000_0006; 

    b_data_i[0].key  <= 34'h1_0000_0002; 
    b_data_i[0].data <= 32'h0000_0002; 
    b_data_i[1].key  <= 34'h1_0000_0004; 
    b_data_i[1].data <= 32'h0000_0004; 
    
    @(posedge clk_i)
    a_v_tmp <= 1'b0; 
    b_v_tmp <=  1'b0;
    @(posedge clk_i); 
    a_v_tmp <= 1'b1; 
    b_v_tmp <= 1'b1; 
    a_data_i[0].key  <= 34'h1_0000_000A; 
    a_data_i[0].data <= 32'h0000_000A; 
    a_data_i[1].key  <= 34'h1_0000_000C; 
    a_data_i[1].data <= 32'h0000_000C; 
    
    b_data_i[0].key  <= 34'h1_0000_0009; 
    b_data_i[0].data <= 32'h0000_0009; 
    b_data_i[1].key  <= 34'h1_0000_000B; 
    b_data_i[1].data <= 32'h0000_000B; 
  
    @(posedge clk_i); 
    a_v_tmp <= 1'b0; 
    b_v_tmp <=  1'b0; 
    @(posedge clk_i);
    a_v_tmp <= 1'b1; 
    b_v_tmp <=  1'b1; 
    a_data_i[0].key  <= 34'h3_0000_0000; 
    a_data_i[0].data <= 32'h0000_0000; 
    a_data_i[1].key  <= 34'h3_0000_0000; 
    a_data_i[1].data <= 32'h0000_0000; 

    
    b_data_i[0].key  <= 34'h3_0000_0000; 
    b_data_i[0].data <= 32'h0000_0000; 
    b_data_i[1].key  <= 34'h3_0000_0000; 
    b_data_i[1].data <= 32'h0000_0000; 

    @(posedge clk_i); 
    a_v_tmp <= 1'b0; 
    b_v_tmp <=  1'b0; 
    repeat(100) @(posedge clk_i); 
    $stop; 
  end 
endmodule