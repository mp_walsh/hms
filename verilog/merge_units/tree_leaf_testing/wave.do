onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /merge2_tree_leaf_tb/reset_i
add wave -noupdate /merge2_tree_leaf_tb/full_i
add wave -noupdate /merge2_tree_leaf_tb/e_height_p
add wave -noupdate -radix hexadecimal -childformat {{{/merge2_tree_leaf_tb/data_o[3]} -radix hexadecimal} {{/merge2_tree_leaf_tb/data_o[2]} -radix hexadecimal} {{/merge2_tree_leaf_tb/data_o[1]} -radix hexadecimal} {{/merge2_tree_leaf_tb/data_o[0]} -radix hexadecimal}} -subitemconfig {{/merge2_tree_leaf_tb/data_o[3]} {-radix hexadecimal} {/merge2_tree_leaf_tb/data_o[2]} {-radix hexadecimal} {/merge2_tree_leaf_tb/data_o[1]} {-radix hexadecimal} {/merge2_tree_leaf_tb/data_o[0]} {-radix hexadecimal}} /merge2_tree_leaf_tb/data_o
add wave -noupdate /merge2_tree_leaf_tb/clk_i
add wave -noupdate /merge2_tree_leaf_tb/b_v_i
add wave -noupdate /merge2_tree_leaf_tb/b_full_o
add wave -noupdate -radix hexadecimal -childformat {{{/merge2_tree_leaf_tb/b_data_i[1]} -radix hexadecimal} {{/merge2_tree_leaf_tb/b_data_i[0]} -radix hexadecimal}} -subitemconfig {{/merge2_tree_leaf_tb/b_data_i[1]} {-height 15 -radix hexadecimal} {/merge2_tree_leaf_tb/b_data_i[0]} {-height 15 -radix hexadecimal}} /merge2_tree_leaf_tb/b_data_i
add wave -noupdate -radix hexadecimal /merge2_tree_leaf_tb/a_v_i
add wave -noupdate -radix hexadecimal /merge2_tree_leaf_tb/a_full_o
add wave -noupdate -radix hexadecimal /merge2_tree_leaf_tb/a_data_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/clk_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/reset_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/full_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/a_data_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/b_data_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/a_v_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/b_v_i
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/b_full_o
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/a_full_o
add wave -noupdate -expand -group leaf /merge2_tree_leaf_tb/DUT/leaf/data_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {756 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {335 ps} {1335 ps}
