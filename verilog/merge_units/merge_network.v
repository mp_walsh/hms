/* HMS merge network
 * Merge Network topology based on HPHMS 2017 FCCM
 * See figure 9
 * Author: Michael Walsh mike0698@uw.edu
 * Date: 2/21/2018 
 */
 

module merge_network
	import hms_pkg::*;
  #(parameter e_height_p = -1)
  (input                       clk_i,
	 input                       reset_i,
   input                       stall_i, 
	 input  e_s[e_height_p-1:0]    data_i,
   output e_s[e_height_p-1:0]    data_o
  );
  
  // note if there is confusion on "pipe_stages" from here
  // and paper, there still is 2(e-1) pipes. For module simpliticty's sake
  // there is two pipes in each "stage" 
  parameter pipe_stages_p = (e_height_p - 1);
  
  genvar i; 
  generate
    if(pipe_stages_p == 1) begin 
     merge_network_stage #(.e_height_p(e_height_p)) pipe_stage 
          (.clk_i(clk_i)
          ,.reset_i(reset_i)
          ,.stall_i(stall_i)
          ,.data_i(data_i)
          ,.data_o(data_o) 
          ); 
    end else begin 
      e_s[e_height_p-1:0] stage_data_o_n[pipe_stages_p-2:0]; 
      for(i = 0; i < pipe_stages_p; i = i + 1) begin : pipe_stage 
        if(i == 0) begin 
          merge_network_stage #(.e_height_p(e_height_p)) pipe_stage 
          (.clk_i(clk_i)
          ,.reset_i(reset_i)
          ,.stall_i(stall_i)
          ,.data_i(data_i)
          ,.data_o(stage_data_o_n[i]) 
          ); 
        end else if (i == (pipe_stages_p -1)) begin 
          merge_network_stage #(.e_height_p(e_height_p)) pipe_stage 
          (.clk_i(clk_i)
          ,.reset_i(reset_i)
          ,.stall_i(stall_i)
          ,.data_i(stage_data_o_n[i-1])
          ,.data_o(data_o) 
          );
        end else begin 
          merge_network_stage #(.e_height_p(e_height_p)) pipe_stage 
          (.clk_i(clk_i)
          ,.reset_i(reset_i)
          ,.stall_i(stall_i)
          ,.data_i(stage_data_o_n[i-1])
          ,.data_o(stage_data_o_n[i]) 
          );
        end 
      end 
    end 
  endgenerate 
endmodule 

module e4_merge_network_tb
  import hms_pkg::*;();
  parameter e_height_tp = 4; 
  logic clk_i, reset_i, stall_i;
  e_s [e_height_tp-1:0]data_i;
  e_s [e_height_tp-1:0]data_o; 
  initial begin
    reset_i <= 1;
    clk_i <= 0; 
    stall_i <= 0; 
    data_i[0].key  <= 34'h0_0000_0000; 
    data_i[0].data <= 32'h0000_0000; 
    data_i[1].key  <= 34'h0_0000_0000; 
    data_i[1].data <= 32'h0000_0000; 
    data_i[2].key  <= 34'h0_0000_0000; 
    data_i[2].data <= 32'h0000_0000; 
    data_i[3].key  <= 34'h0_0000_0000; 
    data_i[3].data <= 32'h0000_0000; 
    forever #5 clk_i <= ~clk_i; 
  end 
  
  merge_network #(.e_height_p(e_height_tp)) DUT 
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.stall_i(stall_i)
    ,.data_i(data_i)
    ,.data_o(data_o)
  );
  
  
  
  initial begin
    // sort two lists of four element each, then send in "done"
    @(posedge clk_i);
      reset_i <= 0;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0001; 
    data_i[0].data <= 32'h0000_0001; 
    data_i[1].key  <= 34'h1_0000_0003; 
    data_i[1].data <= 32'h0000_0003; 
    data_i[2].key  <= 34'h1_0000_0005; 
    data_i[2].data <= 32'h0000_0005; 
    data_i[3].key  <= 34'h1_0000_0007; 
    data_i[3].data <= 32'h0000_0007;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h0_0000_0000; 
    data_i[0].data <= 32'h0000_0000; 
    data_i[1].key  <= 34'h0_0000_0000; 
    data_i[1].data <= 32'h0000_0000; 
    data_i[2].key  <= 34'h0_0000_0000; 
    data_i[2].data <= 32'h0000_0000; 
    data_i[3].key  <= 34'h0_0000_0000; 
    data_i[3].data <= 32'h0000_0000; 
    @(posedge clk_i);
    data_i[0].key  <= 34'h1_0000_0002; 
    data_i[0].data <= 32'h0000_0002; 
    data_i[1].key  <= 34'h1_0000_0004; 
    data_i[1].data <= 32'h0000_0004; 
    data_i[2].key  <= 34'h1_0000_0006; 
    data_i[2].data <= 32'h0000_0006; 
    data_i[3].key  <= 34'h1_0000_0008; 
    data_i[3].data <= 32'h0000_0008;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0009; 
    data_i[0].data <= 32'h0000_0009; 
    data_i[1].key  <= 34'h1_0000_000B; 
    data_i[1].data <= 32'h0000_000B; 
    data_i[2].key  <= 34'h1_0000_000D; 
    data_i[2].data <= 32'h0000_000D; 
    data_i[3].key  <= 34'h1_0000_000F; 
    data_i[3].data <= 32'h0000_000F;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_000A; 
    data_i[0].data <= 32'h0000_000A; 
    data_i[1].key  <= 34'h1_0000_000C; 
    data_i[1].data <= 32'h0000_000C; 
    data_i[2].key  <= 34'h1_0000_000E; 
    data_i[2].data <= 32'h0000_000E; 
    data_i[3].key  <= 34'h1_0000_0010; 
    data_i[3].data <= 32'h0000_0010; 
    @(posedge clk_i);
    data_i[0].key  <= 34'h3_0000_0000; 
    data_i[0].data <= 32'h0000_0000; 
    data_i[1].key  <= 34'h3_0000_0000; 
    data_i[1].data <= 32'h0000_0000; 
    data_i[2].key  <= 34'h3_0000_0000; 
    data_i[2].data <= 32'h0000_0000; 
    data_i[3].key  <= 34'h3_0000_0000; 
    data_i[3].data <= 32'h0000_0000; 
    repeat (50) @(posedge clk_i);
    $stop;
  end 
endmodule 

module e2_merge_network_tb
  import hms_pkg::*;();
  parameter e_height_tp = 2; 
  logic clk_i, reset_i, stall_i;
  e_s [e_height_tp-1:0]data_i;
  e_s [e_height_tp-1:0]data_o; 
  initial begin
    reset_i <= 1;
    clk_i <= 0; 
    stall_i <= 0; 
    data_i[0].key  <= 34'h0_0000_0000; 
    data_i[0].data <= 32'h0000_0000; 
    data_i[1].key  <= 34'h0_0000_0000; 
    data_i[1].data <= 32'h0000_0000; 
    forever #5 clk_i <= ~clk_i; 
  end 
  
  merge_network #(.e_height_p(e_height_tp)) DUT 
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.stall_i(stall_i)
    ,.data_i(data_i)
    ,.data_o(data_o)
  );
  
  
  
  initial begin
    // sort two lists of four element each, then send in "done"
    @(posedge clk_i);
      reset_i <= 0;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0001; 
    data_i[0].data <= 32'h0000_0001; 
    data_i[1].key  <= 34'h1_0000_0009; 
    data_i[1].data <= 32'h0000_0009; 
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0002; 
    data_i[0].data <= 32'h0000_0002; 
    data_i[1].key  <= 34'h0_0000_0004; 
    data_i[1].data <= 32'h0000_0004; 
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0009; 
    data_i[0].data <= 32'h0000_0009; 
    data_i[1].key  <= 34'h1_0000_000B; 
    data_i[1].data <= 32'h0000_000B; 
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_000A; 
    data_i[0].data <= 32'h0000_000A; 
    data_i[1].key  <= 34'h1_0000_000C; 
    data_i[1].data <= 32'h0000_000C; 
    @(posedge clk_i);
    data_i[0].key  <= 34'h3_0000_0000; 
    data_i[0].data <= 32'h0000_0000; 
    data_i[1].key  <= 34'h3_0000_0000; 
    data_i[1].data <= 32'h0000_0000; 
    repeat (50) @(posedge clk_i);
    $stop;
  end 
endmodule 


