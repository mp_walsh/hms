/* HMS merge network stage 
 * Single stage of an E column merge network
 * topology based on HPHMS 2017 FCCM (see figure 9) 
 * Author: Michael Walsh mike0698@uw.edu
 * Date: 2/21/2018 
 */

// A single comparison stage within a merge network
// See Fig 9 in High Preformance Hardware Merge Sort
// Stage A is represented by white registers in figure
// Stage B is represented by grey registers  in figure 
module merge_network_stage
  import hms_pkg::*;
  #(parameter e_height_p=(-1))
  (input      clk_i,
   input      reset_i,
   input      stall_i,
   input  e_s [e_height_p-1:0]data_i,
   output e_s [e_height_p-1:0]data_o 
  );
  // Stage A Registers and nets
  logic [e_data_width_p-1:0]    a_data_r[e_height_p-1:0];
  logic [e_key_width_p-1:0]     a_key_r[e_height_p-1:0];
  logic [e_key_width_p-1:0]     a_key_feedback_r; 
  logic [e_key_width_p-1:0]     b_key_feedback_r; 
  logic [e_key_width_p-1:0]     key_feedback_n; 
  logic [e_data_width_p-1:0]    data_feedback_r;
  logic [e_data_width_p-1:0]    data_feedback_n; 
  // Stage B Registers and nets 
  logic [e_data_width_p-1:0]    b_data_r[e_height_p-1:0];
  logic [e_key_width_p-1:0]     b_key_r[e_height_p-1:0];
  // Key < Feedback Comparater
  logic[e_height_p-1:0]         comp_n;
  logic[e_height_p-1:0]         comp_r; 
  // Comparisons between each input and feedback register 
  genvar i;
  for(i = 0; i < e_height_p; i = i +1) begin
    assign comp_n[i] = a_key_r[i] < a_key_feedback_r;
  end 
  
  assign key_feedback_n  = (comp_n[e_height_p-1]) ? a_key_feedback_r : a_key_r[e_height_p-1];
  assign data_feedback_n = (comp_r[e_height_p-1]) ? data_feedback_r  : b_data_r[e_height_p-1]; 
  
  always_ff @(posedge clk_i) begin
    if(reset_i) begin
      a_key_feedback_r  <= {(e_key_width_p){1'b0}};
      b_key_feedback_r  <= {(e_key_width_p){1'b0}};
      data_feedback_r <= {(e_data_width_p){1'b0}}; 
      for(int k = 0; k < e_height_p; k = k + 1) begin 
        a_key_r[k]  <= {e_key_width_p{1'b0}};
        a_data_r[k] <= {e_data_width_p{1'b0}};
        b_key_r[k]  <= {e_key_width_p{1'b0}};
        b_data_r[k] <= {e_data_width_p{1'b0}};
        comp_r[k]   <= 1'b0; 
      end 
    end else if(!stall_i) begin 
      for(int j = 0; j < e_height_p; j = j + 1) begin 
        a_key_r[j]  <= data_i[j].key; 
        a_data_r[j] <= data_i[j].data; 
      end 
      // feedback updates 
      a_key_feedback_r  <= key_feedback_n; 
      b_key_feedback_r  <= a_key_feedback_r; 
      data_feedback_r <= data_feedback_n; 
      // stage b updates 
      b_key_r  <= a_key_r;
      b_data_r <= a_data_r; 
      // comparators 
      comp_r  <= comp_n; 
    end 
  end 
  
  // Output Mux Network 
  
  // intial key output mux 
  bsg_mux #(.width_p(e_key_width_p), .els_p(2)) key_mux_0 
   (.data_i({b_key_r[0], // sel_i == 1 
            b_key_feedback_r}) // sel_i == 0 
   ,.sel_i(comp_r[0])
   ,.data_o(data_o[0].key)
   );    
   
   // initial data output mux
   bsg_mux #(.width_p(e_data_width_p), .els_p(2)) data_mux_0 
   (.data_i({b_data_r[0], // sel_i == 1 
            data_feedback_r}) // sel_i == 0 
   ,.sel_i(comp_r[0])
   ,.data_o(data_o[0].data)
   );    
   
   // key mux output networks 
   genvar x;
   generate 
    for(x = 1; x < e_height_p; x = x + 1) begin
      bsg_mux #(.width_p(e_key_width_p), .els_p(4)) key_mux
      (.data_i({b_key_r[x], // 11
               {(e_key_width_p){1'b1}}, //10 this should never happen
               {b_key_feedback_r}, // 01 
               b_key_r[x-1]}) // 00 
      ,.sel_i({comp_r[x], comp_r[x-1]}) // comp_r[x] is higher comparison bit
      ,.data_o(data_o[x].key)
      ); 
      
      
      bsg_mux #(.width_p(e_data_width_p), .els_p(4)) data_mux
      (.data_i({b_data_r[x], // 11
               {(e_data_width_p){1'b1}}, //10 this should never happen
               {data_feedback_r}, // 01 
               b_data_r[x-1]}) // 00 
      ,.sel_i({comp_r[x], comp_r[x-1]}) // comp_r[x] is higher comparison bit
      ,.data_o(data_o[x].data)
      ); 
    end
   endgenerate 
endmodule


// Very primitive tests to test basic
// functionallity and paramter scaling appropriately  
module e4_merge_network_stage_tb
  import hms_pkg::*;
  ();
  parameter e_height_tp = 4; 
  logic clk_i, reset_i, stall_i;
  e_s [e_height_tp-1:0]data_i;
  e_s [e_height_tp-1:0]data_o; 

  initial begin
    reset_i <= 1'b1;
    clk_i   <= 1'b1; 
    stall_i <= 1'b0; 
    data_i[0].key  <= 34'h1_0000_0002; 
    data_i[0].data <= 32'h0000_0000; 
    data_i[1].key  <= 34'h1_0000_0004; 
    data_i[1].data <= 32'h0000_0001; 
    data_i[2].key  <= 34'h1_0000_0006; 
    data_i[2].data <= 32'h0000_0002; 
    data_i[3].key  <= 34'h1_0000_0008; 
    data_i[3].data <= 32'h0000_0003; 
    forever #5 clk_i <= ~clk_i; 
  end
  
  merge_network_stage #(.e_height_p(e_height_tp)) DUT
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.data_i(data_i)
    ,.data_o(data_o)
    ,.stall_i(stall_i)
    );
    
  initial begin
    @(posedge clk_i);
    @(posedge clk_i); reset_i <= 1'b0;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0007; 
    data_i[0].data <= 32'h0000_0004; 
    data_i[1].key  <= 34'h1_0000_0009; 
    data_i[1].data <= 32'h0000_0005; 
    data_i[2].key  <= 34'h1_0000_000A; 
    data_i[2].data <= 32'h0000_0006; 
    data_i[3].key  <= 34'h1_0000_000B; 
    data_i[3].data <= 32'h0000_0007; 
    @(posedge clk_i); 
    repeat (50) @(posedge clk_i); 
    $stop;
  end 
endmodule 

module e2_merge_network_stage_tb
  import hms_pkg::*;
  ();
  parameter e_height_tp = 2; 
  logic clk_i, reset_i, stall_i;
  e_s [e_height_tp-1:0]data_i;
  e_s [e_height_tp-1:0]data_o; 

  initial begin
    reset_i <= 1'b1;
    clk_i   <= 1'b1; 
    stall_i <= 1'b0; 
    data_i[0].key  <= 34'h1_0000_0002; 
    data_i[0].data <= 32'h0000_000A; 
    data_i[1].key  <= 34'h1_0000_0008; 
    data_i[1].data <= 32'h0000_000B; 
    forever #5 clk_i <= ~clk_i; 
  end
  
  merge_network_stage #(.e_height_p(e_height_tp)) DUT
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.data_i(data_i)
    ,.data_o(data_o)
    ,.stall_i(stall_i)
    );
    
  initial begin
    @(posedge clk_i);
    @(posedge clk_i); reset_i <= 1'b0;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0007; 
    data_i[0].data <= 32'h0000_000C; 
    data_i[1].key  <= 34'h1_0000_0009; 
    data_i[1].data <= 32'h0000_000D; 
    repeat (50) @(posedge clk_i); 
    $stop;
  end 
endmodule 


module e8_merge_network_stage_tb
  import hms_pkg::*;
  ();
  parameter e_height_tp = 8; 
  logic clk_i, reset_i, stall_i;
  e_s [e_height_tp-1:0]data_i;
  e_s [e_height_tp-1:0]data_o; 

  initial begin
    reset_i <= 1'b1;
    clk_i   <= 1'b1; 
    stall_i <= 1'b0; 
    data_i[0].key  <= 34'h1_0000_0001; 
    data_i[0].data <= 32'h0000_000A; 
    data_i[1].key  <= 34'h1_0000_0003; 
    data_i[1].data <= 32'h0000_000B; 
    data_i[2].key  <= 34'h1_0000_0005; 
    data_i[2].data <= 32'h0000_000C; 
    data_i[3].key  <= 34'h1_0000_0007; 
    data_i[3].data <= 32'h0000_000D;
    data_i[4].key  <= 34'h1_0000_0009; 
    data_i[4].data <= 32'h0000_000E; 
    data_i[5].key  <= 34'h1_0000_000B; 
    data_i[5].data <= 32'h0000_000F; 
    data_i[6].key  <= 34'h1_0000_000D; 
    data_i[6].data <= 32'h0000_0010; 
    data_i[7].key  <= 34'h1_0000_000F; 
    data_i[7].data <= 32'h0000_0011;     
    forever #5 clk_i <= ~clk_i; 
  end
  
  merge_network_stage #(.e_height_p(e_height_tp)) DUT
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.stall_i(stall_i)
    ,.data_i(data_i)
    ,.data_o(data_o)
    );
    
  initial begin
    @(posedge clk_i);
    @(posedge clk_i); reset_i <= 1'b0;
    @(posedge clk_i); 
    data_i[0].key  <= 34'h1_0000_0002; 
    data_i[0].data <= 32'h0000_0012; 
    data_i[1].key  <= 34'h1_0000_0004; 
    data_i[1].data <= 32'h0000_0013; 
    data_i[2].key  <= 34'h1_0000_0006; 
    data_i[2].data <= 32'h0000_0014; 
    data_i[3].key  <= 34'h1_0000_0008; 
    data_i[3].data <= 32'h0000_0015;
    data_i[4].key  <= 34'h1_0000_000A; 
    data_i[4].data <= 32'h0000_0016; 
    data_i[5].key  <= 34'h1_0000_000C; 
    data_i[5].data <= 32'h0000_0017; 
    data_i[6].key  <= 34'h1_0000_000E; 
    data_i[6].data <= 32'h0000_0018; 
    data_i[7].key  <= 34'h1_0000_0010; 
    data_i[7].data <= 32'h0000_0019;  
    @(posedge clk_i); 
    repeat (50) @(posedge clk_i); 
    $stop;
  end 
endmodule 
   
   
 	




   