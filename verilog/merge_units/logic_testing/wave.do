onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /merge_logic_tb/e_height_p
add wave -noupdate /merge_logic_tb/clk_i
add wave -noupdate /merge_logic_tb/reset_i
add wave -noupdate /merge_logic_tb/full_i
add wave -noupdate /merge_logic_tb/DUT/stall_r
add wave -noupdate -radix hexadecimal -childformat {{{/merge_logic_tb/a_data_i[3]} -radix hexadecimal} {{/merge_logic_tb/a_data_i[2]} -radix hexadecimal} {{/merge_logic_tb/a_data_i[1]} -radix hexadecimal} {{/merge_logic_tb/a_data_i[0]} -radix hexadecimal}} -expand -subitemconfig {{/merge_logic_tb/a_data_i[3]} {-radix hexadecimal} {/merge_logic_tb/a_data_i[2]} {-radix hexadecimal} {/merge_logic_tb/a_data_i[1]} {-radix hexadecimal} {/merge_logic_tb/a_data_i[0]} {-radix hexadecimal}} /merge_logic_tb/a_data_i
add wave -noupdate -radix hexadecimal /merge_logic_tb/b_data_i
add wave -noupdate /merge_logic_tb/a_v_i
add wave -noupdate /merge_logic_tb/b_v_i
add wave -noupdate /merge_logic_tb/b_full_o
add wave -noupdate /merge_logic_tb/a_full_o
add wave -noupdate -radix hexadecimal -childformat {{{/merge_logic_tb/data_o[3]} -radix hexadecimal} {{/merge_logic_tb/data_o[2]} -radix hexadecimal} {{/merge_logic_tb/data_o[1]} -radix hexadecimal} {{/merge_logic_tb/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/merge_logic_tb/data_o[3]} {-height 15 -radix hexadecimal} {/merge_logic_tb/data_o[2]} {-height 15 -radix hexadecimal} {/merge_logic_tb/data_o[1]} {-height 15 -radix hexadecimal} {/merge_logic_tb/data_o[0]} {-height 15 -radix hexadecimal}} /merge_logic_tb/data_o
add wave -noupdate -group a_dpath -radix hexadecimal /merge_logic_tb/DUT/a_bus_stage1_n
add wave -noupdate -group a_dpath -radix hexadecimal /merge_logic_tb/DUT/a_bus_stage1_r
add wave -noupdate -group a_dpath -radix hexadecimal /merge_logic_tb/DUT/a_bus_stage2_r
add wave -noupdate -expand -group b_dpath -radix hexadecimal /merge_logic_tb/DUT/b_bus_stage1_n
add wave -noupdate -expand -group b_dpath -radix hexadecimal /merge_logic_tb/DUT/b_bus_stage1_r
add wave -noupdate -expand -group b_dpath -radix hexadecimal /merge_logic_tb/DUT/b_bus_stage2_r
add wave -noupdate /merge_logic_tb/DUT/deque_r
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/clk_i
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/reset_i
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/v_i
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/ready_o
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/data_i
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/v_o
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/data_o
add wave -noupdate -group fifo_a /merge_logic_tb/DUT/fifo_a/yumi_i
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/clk_i
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/reset_i
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/v_i
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/ready_o
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/data_i
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/v_o
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/data_o
add wave -noupdate -group fifo_b /merge_logic_tb/DUT/fifo_b/yumi_i
add wave -noupdate -group b_sel_mux /merge_logic_tb/DUT/b_sel_mux/data_i
add wave -noupdate -group b_sel_mux /merge_logic_tb/DUT/b_sel_mux/sel_i
add wave -noupdate -group b_sel_mux /merge_logic_tb/DUT/b_sel_mux/data_o
add wave -noupdate -expand -group merge_n -radix hexadecimal -childformat {{{/merge_logic_tb/DUT/merge_n/data_i[3]} -radix hexadecimal} {{/merge_logic_tb/DUT/merge_n/data_i[2]} -radix hexadecimal} {{/merge_logic_tb/DUT/merge_n/data_i[1]} -radix hexadecimal} {{/merge_logic_tb/DUT/merge_n/data_i[0]} -radix hexadecimal}} -subitemconfig {{/merge_logic_tb/DUT/merge_n/data_i[3]} {-height 15 -radix hexadecimal} {/merge_logic_tb/DUT/merge_n/data_i[2]} {-height 15 -radix hexadecimal} {/merge_logic_tb/DUT/merge_n/data_i[1]} {-height 15 -radix hexadecimal} {/merge_logic_tb/DUT/merge_n/data_i[0]} {-height 15 -radix hexadecimal}} /merge_logic_tb/DUT/merge_n/data_i
add wave -noupdate -expand -group merge_n -radix hexadecimal -childformat {{{/merge_logic_tb/DUT/merge_n/data_o[3]} -radix hexadecimal} {{/merge_logic_tb/DUT/merge_n/data_o[2]} -radix hexadecimal} {{/merge_logic_tb/DUT/merge_n/data_o[1]} -radix hexadecimal} {{/merge_logic_tb/DUT/merge_n/data_o[0]} -radix hexadecimal}} -subitemconfig {{/merge_logic_tb/DUT/merge_n/data_o[3]} {-height 15 -radix hexadecimal} {/merge_logic_tb/DUT/merge_n/data_o[2]} {-height 15 -radix hexadecimal} {/merge_logic_tb/DUT/merge_n/data_o[1]} {-height 15 -radix hexadecimal} {/merge_logic_tb/DUT/merge_n/data_o[0]} {-height 15 -radix hexadecimal}} /merge_logic_tb/DUT/merge_n/data_o
add wave -noupdate -group deque_sel_mux /merge_logic_tb/DUT/deque_select_mux/data_i
add wave -noupdate -group deque_sel_mux /merge_logic_tb/DUT/deque_select_mux/sel_i
add wave -noupdate -group deque_sel_mux /merge_logic_tb/DUT/deque_select_mux/data_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {229 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 224
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {152 ps} {479 ps}
