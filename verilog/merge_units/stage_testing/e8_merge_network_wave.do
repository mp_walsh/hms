onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /e8_merge_network_stage_tb/e_height_tp
add wave -noupdate -radix hexadecimal /e8_merge_network_stage_tb/clk_i
add wave -noupdate -radix hexadecimal /e8_merge_network_stage_tb/reset_i
add wave -noupdate -radix hexadecimal -childformat {{{/e8_merge_network_stage_tb/data_i[7]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_i[6]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_i[5]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_i[4]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_i[3]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_i[2]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_i[1]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_i[0]} -radix hexadecimal}} -expand -subitemconfig {{/e8_merge_network_stage_tb/data_i[7]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_i[6]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_i[5]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_i[4]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_i[3]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_i[2]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_i[1]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_i[0]} {-radix hexadecimal}} /e8_merge_network_stage_tb/data_i
add wave -noupdate -radix hexadecimal -childformat {{{/e8_merge_network_stage_tb/data_o[7]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_o[6]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_o[5]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_o[4]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_o[3]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_o[2]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_o[1]} -radix hexadecimal} {{/e8_merge_network_stage_tb/data_o[0]} -radix hexadecimal}} -expand -subitemconfig {{/e8_merge_network_stage_tb/data_o[7]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_o[6]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_o[5]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_o[4]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_o[3]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_o[2]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_o[1]} {-radix hexadecimal} {/e8_merge_network_stage_tb/data_o[0]} {-radix hexadecimal}} /e8_merge_network_stage_tb/data_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {851 ps}
