library verilog;
use verilog.vl_types.all;
entity master_counter_any_list is
    generic(
        addr_size_p     : integer := -1;
        e_height_p      : integer := -1;
        e_log           : vl_notype
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        finish_i        : in     vl_logic;
        full_length_i   : in     vl_logic_vector;
        max_sorted_i    : in     vl_logic_vector;
        new_iteration_i : in     vl_logic;
        counter_o       : out    vl_logic_vector;
        starts_o        : out    vl_logic_vector;
        lengths_o       : out    vl_logic_vector;
        done_o          : out    vl_logic;
        reset_tree_o    : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
    attribute mti_svvh_generic_type of e_height_p : constant is 1;
    attribute mti_svvh_generic_type of e_log : constant is 3;
end master_counter_any_list;
