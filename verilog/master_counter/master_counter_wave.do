onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /master_counter_testbench/clk_i
add wave -noupdate /master_counter_testbench/reset_i
add wave -noupdate /master_counter_testbench/finish_i
add wave -noupdate /master_counter_testbench/new_iteration_i
add wave -noupdate /master_counter_testbench/done_o
add wave -noupdate /master_counter_testbench/counter_o
add wave -noupdate /master_counter_testbench/full_length_i
add wave -noupdate /master_counter_testbench/max_sorted_i
add wave -noupdate /master_counter_testbench/starts_o
add wave -noupdate /master_counter_testbench/DUT/next
add wave -noupdate /master_counter_testbench/DUT/done
add wave -noupdate /master_counter_testbench/DUT/add_value
add wave -noupdate /master_counter_testbench/DUT/next_point
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {16515 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {60375 ps}
