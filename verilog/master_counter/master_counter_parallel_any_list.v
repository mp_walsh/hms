//This module starts when new iteration goes high. It tracks how many times the tree and extractors need to be run to do one level of merge sort.
module master_counter_parallel_any_list #(parameter addr_size_p = -1, e_height_p = -1, e_log = $clog2(e_height_p))
(input  clk_i
,input reset_i

,input finish_i

,input [addr_size_p - 1:0] full_length_i
,input [addr_size_p - 1:0] max_sorted_i
,input new_iteration_i

,output [addr_size_p - 1:0] counter_o
,output [e_height_p - 1:0] starts_o
,output [e_height_p - 1:0][addr_size_p - 1:0] lengths_o
,output done_o
,output [addr_size_p - 1:0] next_max_sorted_o
,output reset_tree_o
);
  //the counter logic
  //logic list_done_r;
  logic [e_height_p - 1:0] starts_r;
  //logic [addr_size_p - 1:0] times_to_count_r;
  logic [addr_size_p - 1:0] add_value;
  logic [addr_size_p - 1:0] counter_r;
  
  //values to use for the adder
  //need to update e_height parameter later, make a clog variable for here
   assign add_value = max_sorted_i << e_log;
   
  //the adder to update counter
  wire [addr_size_p - 1:0] next_point = counter_o + add_value;
  
  //determine if the next point is equal to or less than the full length
  wire next = finish_i & (next_point < full_length_i);
  wire done = finish_i & (next_point >= full_length_i);
  
  logic reset_tree_r;
  //the counter
  always_ff @(posedge clk_i)
  begin
    if (reset_i) begin //reset case
      counter_r <= 15;
	    starts_r <= {e_height_p{1'b0}};// or 0
	    reset_tree_r <= 0;
    end
    else if (new_iteration_i) begin //reset case
        counter_r <= 0;
        starts_r <= {e_height_p{1'b1}};// or ~0
        reset_tree_r <= 1;
    end
    else if (done) begin //got to the end, stop until told to start again
      counter_r <= counter_r;
      starts_r <= {e_height_p{1'b0}};// or 0
      reset_tree_r <= 0;
    end
    else if (next) begin //keep looping
      counter_r <= next_point;
      starts_r <= {e_height_p{1'b1}};// or ~0
      reset_tree_r <= 1;
    end
    else begin //waiting case
      counter_r <= counter_r;
      starts_r <= {e_height_p{1'b0}};// or ~0
      reset_tree_r <= 0;
    end
  end
  
  //determine if we don't have enough list to do basic behavior
  wire [addr_size_p - 1:0] remaining = full_length_i - counter_r;
  wire special = remaining < add_value; 
  
  logic [e_height_p - 1:0] start_specials;
  logic [e_height_p - 1:0][addr_size_p - 1:0] lengths_special;
  
  //scale up max_sorted
  logic [e_height_p - 1:0][addr_size_p - 1:0] scaled_max_sorted;
  assign scaled_max_sorted[0] = 0;
  assign scaled_max_sorted[1] = max_sorted_i;
  assign scaled_max_sorted[2] = 2*max_sorted_i;
  assign scaled_max_sorted[3] = 3*max_sorted_i;
  assign scaled_max_sorted[4] = 4*max_sorted_i;
  assign scaled_max_sorted[5] = 5*max_sorted_i;
  assign scaled_max_sorted[6] = 6*max_sorted_i;
  assign scaled_max_sorted[7] = 7*max_sorted_i;
  
  assign start_specials[1] = (remaining > scaled_max_sorted[1]);
  assign start_specials[2] = (remaining > scaled_max_sorted[2]);
  assign start_specials[3] = (remaining > scaled_max_sorted[3]);
  assign start_specials[4] = (remaining > scaled_max_sorted[4]);
  assign start_specials[5] = (remaining > scaled_max_sorted[5]);
  assign start_specials[6] = (remaining > scaled_max_sorted[6]);
  assign start_specials[7] = (remaining > scaled_max_sorted[7]);
  
  
  //determine the length to send to the first mover
  logic [e_height_p - 1:0][addr_size_p - 1:0] lengths_between;
  always_comb begin
    if (~special | start_specials[1]) begin
      lengths_between[0] = max_sorted_i;
    end
    else begin 
      lengths_between[0] = remaining;
    end
  end
  
  //determine the length to send to the other movers
  //determine if the second mover should start
  logic [e_height_p - 1:0] start_specials_between;
  genvar i;
  for (i = 1; i < e_height_p - 1; i = i + 1) begin
    always_comb begin
      if (~special | start_specials[i+1]) begin
        lengths_between[i] = max_sorted_i;
      end
      else if(remaining <= scaled_max_sorted[i]) begin
        lengths_between[i] = 0;
      end
      else begin
        lengths_between[i] = remaining - scaled_max_sorted[i];
      end
    end
    
    
    
    always_comb begin
      if (special) begin
        start_specials_between[i] = start_specials[i] & starts_r[i];
      end
      else begin 
        start_specials_between[i] = starts_r[i];
      end
    end
  end
  
  //top case
  always_comb begin
    if (~special) begin
      lengths_between[7] = max_sorted_i;
    end
    else if(remaining <= scaled_max_sorted[7]) begin
      lengths_between[7] = 0;
    end
    else begin
      lengths_between[7] = remaining - scaled_max_sorted[7];
    end
  end
    
  
  always_comb begin
    if (special) begin
      start_specials_between[7] = start_specials[7] & starts_r[7];
    end
    else begin 
      start_specials_between[7] = starts_r[7];
    end
  end
  
  assign counter_o = counter_r;
  assign starts_o[0] = starts_r[0];
  assign starts_o[e_height_p - 1:1] = start_specials_between[e_height_p - 1:1];
  assign done_o = done;
  assign reset_tree_o = reset_tree_r;
  assign lengths_o = lengths_between;
  assign next_max_sorted_o = add_value;

endmodule

module master_counter_parallel_any_list_testbench();
  //addr size is set to 6, e = 4
  logic  clk_i, reset_i, finish_i, new_iteration_i, done_o, reset_tree_o;
  logic [7:0] counter_o, full_length_i, max_sorted_i, next_max_sorted_o;
  logic [7:0] starts_o;
  logic [7:0][7:0] lengths_o;
  
  master_counter_parallel_any_list #(.addr_size_p(8), .e_height_p(8)) DUT
  (.clk_i
  ,.reset_i
  ,.finish_i
  ,.full_length_i
  ,.max_sorted_i
  ,.new_iteration_i
  ,.counter_o
  ,.starts_o
  ,.lengths_o
  ,.done_o
  ,.next_max_sorted_o
  ,.reset_tree_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
	
  initial begin
	reset_i <= 1; finish_i <= 0; new_iteration_i <= 0; @(posedge clk_i);
	full_length_i <= 128; max_sorted_i <= 2; 
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	/*new_iteration_i <= 1; @(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(32) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end*/
  
  new_iteration_i <= 1; full_length_i <= 63; max_sorted_i <= 1; @(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(8) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
	
  new_iteration_i <= 1; full_length_i <= 63; max_sorted_i <= 8; @(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(1) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
  
  new_iteration_i <= 1; full_length_i <= 65; max_sorted_i <= 1; @(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(9) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
	
  new_iteration_i <= 1; full_length_i <= 65; max_sorted_i <= 8; @(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(2) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
  
				
		$stop(); // end the simulation
  end
  
endmodule
  
