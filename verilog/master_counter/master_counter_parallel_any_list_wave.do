onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /master_counter_parallel_any_list_testbench/clk_i
add wave -noupdate /master_counter_parallel_any_list_testbench/reset_i
add wave -noupdate /master_counter_parallel_any_list_testbench/finish_i
add wave -noupdate /master_counter_parallel_any_list_testbench/new_iteration_i
add wave -noupdate /master_counter_parallel_any_list_testbench/done_o
add wave -noupdate /master_counter_parallel_any_list_testbench/reset_tree_o
add wave -noupdate -radix unsigned /master_counter_parallel_any_list_testbench/counter_o
add wave -noupdate -radix unsigned /master_counter_parallel_any_list_testbench/full_length_i
add wave -noupdate -radix unsigned /master_counter_parallel_any_list_testbench/max_sorted_i
add wave -noupdate -radix unsigned -childformat {{{/master_counter_parallel_any_list_testbench/starts_o[7]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/starts_o[6]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/starts_o[5]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/starts_o[4]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/starts_o[3]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/starts_o[2]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/starts_o[1]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/starts_o[0]} -radix unsigned}} -expand -subitemconfig {{/master_counter_parallel_any_list_testbench/starts_o[7]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/starts_o[6]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/starts_o[5]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/starts_o[4]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/starts_o[3]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/starts_o[2]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/starts_o[1]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/starts_o[0]} {-height 15 -radix unsigned}} /master_counter_parallel_any_list_testbench/starts_o
add wave -noupdate -radix unsigned -childformat {{{/master_counter_parallel_any_list_testbench/lengths_o[7]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/lengths_o[6]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/lengths_o[5]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/lengths_o[4]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/lengths_o[3]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/lengths_o[2]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/lengths_o[1]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/lengths_o[0]} -radix unsigned}} -expand -subitemconfig {{/master_counter_parallel_any_list_testbench/lengths_o[7]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/lengths_o[6]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/lengths_o[5]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/lengths_o[4]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/lengths_o[3]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/lengths_o[2]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/lengths_o[1]} {-height 15 -radix unsigned} {/master_counter_parallel_any_list_testbench/lengths_o[0]} {-height 15 -radix unsigned}} /master_counter_parallel_any_list_testbench/lengths_o
add wave -noupdate -radix unsigned /master_counter_parallel_any_list_testbench/next_max_sorted_o
add wave -noupdate -radix unsigned -childformat {{{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[7]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[6]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[5]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[4]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[3]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[2]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[1]} -radix unsigned} {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[0]} -radix unsigned}} -expand -subitemconfig {{/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[7]} {-radix unsigned} {/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[6]} {-radix unsigned} {/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[5]} {-radix unsigned} {/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[4]} {-radix unsigned} {/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[3]} {-radix unsigned} {/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[2]} {-radix unsigned} {/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[1]} {-radix unsigned} {/master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted[0]} {-radix unsigned}} /master_counter_parallel_any_list_testbench/DUT/scaled_max_sorted
add wave -noupdate /master_counter_parallel_any_list_testbench/DUT/remaining
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {20345 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {2194 ps} {83569 ps}
