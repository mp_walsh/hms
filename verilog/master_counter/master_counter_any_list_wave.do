onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /master_counter_any_list_testbench/DUT/clk_i
add wave -noupdate /master_counter_any_list_testbench/DUT/reset_i
add wave -noupdate /master_counter_any_list_testbench/DUT/finish_i
add wave -noupdate /master_counter_any_list_testbench/DUT/full_length_i
add wave -noupdate /master_counter_any_list_testbench/DUT/max_sorted_i
add wave -noupdate /master_counter_any_list_testbench/DUT/new_iteration_i
add wave -noupdate /master_counter_any_list_testbench/DUT/counter_o
add wave -noupdate /master_counter_any_list_testbench/DUT/starts_o
add wave -noupdate /master_counter_any_list_testbench/DUT/lengths_o
add wave -noupdate /master_counter_any_list_testbench/DUT/done_o
add wave -noupdate /master_counter_any_list_testbench/DUT/reset_tree_o
add wave -noupdate /master_counter_any_list_testbench/DUT/list_done_r
add wave -noupdate /master_counter_any_list_testbench/DUT/starts_r
add wave -noupdate /master_counter_any_list_testbench/DUT/times_to_count_r
add wave -noupdate /master_counter_any_list_testbench/DUT/add_value
add wave -noupdate /master_counter_any_list_testbench/DUT/counter_r
add wave -noupdate /master_counter_any_list_testbench/DUT/next_point
add wave -noupdate /master_counter_any_list_testbench/DUT/next
add wave -noupdate /master_counter_any_list_testbench/DUT/done
add wave -noupdate /master_counter_any_list_testbench/DUT/reset_tree_r
add wave -noupdate /master_counter_any_list_testbench/DUT/remaining
add wave -noupdate /master_counter_any_list_testbench/DUT/special
add wave -noupdate /master_counter_any_list_testbench/DUT/start_1_special
add wave -noupdate /master_counter_any_list_testbench/DUT/lengths_special
add wave -noupdate /master_counter_any_list_testbench/DUT/lengths_between
add wave -noupdate /master_counter_any_list_testbench/DUT/start1_special_between
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9795 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {61032 ps}
