onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /full_sort_manager_parallelized_testbench/clk_i
add wave -noupdate /full_sort_manager_parallelized_testbench/reset_i
add wave -noupdate /full_sort_manager_parallelized_testbench/list_done_i
add wave -noupdate /full_sort_manager_parallelized_testbench/done_a_round_i
add wave -noupdate -radix unsigned /full_sort_manager_parallelized_testbench/log_of_list_i
add wave -noupdate -radix unsigned /full_sort_manager_parallelized_testbench/next_sorted_length_i
add wave -noupdate -radix unsigned /full_sort_manager_parallelized_testbench/length_of_list_i
add wave -noupdate /full_sort_manager_parallelized_testbench/moved_i
add wave -noupdate /full_sort_manager_parallelized_testbench/start_move_o
add wave -noupdate /full_sort_manager_parallelized_testbench/new_iteration_o
add wave -noupdate -radix unsigned /full_sort_manager_parallelized_testbench/max_sorted_moved_o
add wave -noupdate -radix unsigned /full_sort_manager_parallelized_testbench/max_sorted_o
add wave -noupdate /full_sort_manager_parallelized_testbench/finished_sorting_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {21080 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {46725 ps}
