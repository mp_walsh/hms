library verilog;
use verilog.vl_types.all;
entity full_sort_manager_parallelized_testbench is
    generic(
        addr_size_p     : integer := 8
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
end full_sort_manager_parallelized_testbench;
