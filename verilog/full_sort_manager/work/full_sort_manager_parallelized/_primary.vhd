library verilog;
use verilog.vl_types.all;
entity full_sort_manager_parallelized is
    generic(
        addr_size_p     : integer := -1;
        e_height_p      : integer := -1;
        e_height_lp     : vl_notype
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        list_done_i     : in     vl_logic;
        done_a_round_i  : in     vl_logic;
        log_of_list_i   : in     vl_logic_vector;
        length_of_list_i: in     vl_logic_vector;
        next_sorted_length_i: in     vl_logic_vector;
        moved_i         : in     vl_logic;
        start_move_o    : out    vl_logic;
        new_iteration_o : out    vl_logic;
        max_sorted_o    : out    vl_logic_vector;
        max_sorted_moved_o: out    vl_logic_vector;
        finished_sorting_o: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
    attribute mti_svvh_generic_type of e_height_p : constant is 1;
    attribute mti_svvh_generic_type of e_height_lp : constant is 3;
end full_sort_manager_parallelized;
