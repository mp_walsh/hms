module full_sort_manager_parallelized #(parameter addr_size_p = -1, e_height_p = -1, e_height_lp = $clog2(e_height_p))
(input  clk_i
,input reset_i
//,input FSB_reset_i

,input list_done_i
,input done_a_round_i

,input [addr_size_p - 1:0] log_of_list_i
,input [addr_size_p - 1:0] length_of_list_i
,input [addr_size_p - 1:0] next_sorted_length_i

,input moved_i

,output start_move_o

,output new_iteration_o
,output [addr_size_p - 1:0] max_sorted_o
,output [addr_size_p - 1:0] max_sorted_moved_o

,output finished_sorting_o
);
  //setup some logic
  logic [addr_size_p - 1:0] counter_r;
  wire [addr_size_p - 1:0] next_point = counter_r + 1;
  
  //the counter of iterations completed
  always_ff @(posedge clk_i) begin
    if (reset_i | list_done_i /*| FSB_reset_i*/) begin
	  counter_r <= 0;
	end
	else if (moved_i) begin
	  counter_r <= next_point;
	end
	else begin
	  counter_r <= counter_r;
	end
  end
  
  //determine is this the last needed computation
  wire last_sort = (log_of_list_i - 1 == counter_r);
  
  //determine when to start a new iteration
  logic new_iteration_r;
  always_ff @(posedge clk_i) begin
    if (reset_i) begin
	  new_iteration_r <= 0;
	end
	else if (list_done_i | (moved_i & (~last_sort))) begin
	  new_iteration_r <= 1;
	end
	else begin
	  new_iteration_r <= 0;
	end
  end

  //pulse the memory to start moving after a merge has completed
  logic start_move_r;
  always_ff @(posedge clk_i) begin
    if (reset_i) begin
	  start_move_r <= 0;
	end
	else if (done_a_round_i & (~last_sort)) begin
	  start_move_r <= 1;
	end
	else begin
	  start_move_r <= 0;
	end
  end
  
  //shift next_iteration over elog to determine if we need to clip max_sorted
  logic [addr_size_p + 2:0] next_sorted_length_shifted;
  assign next_sorted_length_shifted = next_sorted_length_i << e_height_lp;
  
  //update max sorted list after completing a merge
  logic [addr_size_p - 1:0] max_sorted_moved_r;
  always_ff @(posedge clk_i) begin
    if (reset_i | list_done_i) begin
      max_sorted_moved_r <= 1;
    end
    else if (done_a_round_i) begin
      if (next_sorted_length_shifted > {3'b000, length_of_list_i}) begin
        max_sorted_moved_r <= (length_of_list_i >> e_height_lp);//any list concern
      end
      else begin
        max_sorted_moved_r <= next_sorted_length_i;
      end
    end
    else begin
      max_sorted_moved_r <= max_sorted_moved_r;
    end
  end
  
  //max sorted update for sub iter
  logic [addr_size_p - 1:0] max_sorted_r, max_sorted_r_r;
  always_ff @(posedge clk_i) begin
    if (reset_i | list_done_i) begin
      max_sorted_r <= 1;
      max_sorted_r_r <= 1;
    end
    else if (moved_i) begin
      max_sorted_r <= max_sorted_moved_r;
      max_sorted_r_r <= max_sorted_r;
    end
    else begin
      max_sorted_r <= max_sorted_r;
      max_sorted_r_r <= max_sorted_r;
    end
  end
  
  //determine when we have finished sorting
  assign finished_sorting_o = done_a_round_i & last_sort;
  
  //assign the outputs
  assign max_sorted_o = max_sorted_r_r;
  assign max_sorted_moved_o = max_sorted_moved_r;
  assign start_move_o = start_move_r;
  assign new_iteration_o = new_iteration_r;
endmodule


module full_sort_manager_parallelized_testbench #(parameter addr_size_p = 8);
  logic clk_i, reset_i, list_done_i, done_a_round_i;
  logic [addr_size_p - 1:0] log_of_list_i, next_sorted_length_i, length_of_list_i;
  logic moved_i, start_move_o, new_iteration_o;
  logic [addr_size_p - 1:0] max_sorted_o, max_sorted_moved_o;
  logic finished_sorting_o;
  
  //the module
  full_sort_manager_parallelized #(.addr_size_p(addr_size_p), .e_height_p(8)) DUT
  (.clk_i
  ,.reset_i
  ,.list_done_i
  ,.done_a_round_i
  ,.log_of_list_i
  ,.length_of_list_i
  ,.next_sorted_length_i
  ,.moved_i
  ,.start_move_o
  ,.new_iteration_o
  ,.max_sorted_o
  ,.max_sorted_moved_o
  ,.finished_sorting_o);
  
  //shifted by log(e) where e = 2
  assign next_sorted_length_i = max_sorted_o << 3;

  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
	
  initial begin
	reset_i <= 1; list_done_i <= 0; done_a_round_i <= 0; 
	log_of_list_i <= 3; moved_i <= 0; length_of_list_i <= 128;
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	                                @(posedge clk_i);
	list_done_i <= 1; @(posedge clk_i);
	list_done_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	
	repeat(2) begin 
	done_a_round_i <= 1;    @(posedge clk_i);
	done_a_round_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end 
	moved_i <= 1;    @(posedge clk_i);
	moved_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end
	end
	
	 
	done_a_round_i <= 1;    @(posedge clk_i);
	done_a_round_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end 
	
  /**
	//start again
	list_done_i <= 1; @(posedge clk_i);
	list_done_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	
	repeat(4) begin 
	done_a_round_i <= 1;    @(posedge clk_i);
	done_a_round_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end 
	moved_i <= 1;    @(posedge clk_i);
	moved_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end 
	end
	
	done_a_round_i <= 1;    @(posedge clk_i);
	done_a_round_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end 
	**/			
		$stop(); // end the simulation
  end
endmodule
