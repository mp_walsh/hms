# Create work library
vlib work

# Compile Verilog
#     All Verilog files that are part of this design should have
#     their own "vlog" line below.
vlog "./bsg_defines.v"
vlog "./bsg_dff_reset.v"
vlog "./bsg_mux.v"
vlog "./bsg_adder_cin.v"
vlog "./sorted_list_extracter_slave_parallelized_any_list.v"
vlog "./bsg_circular_ptr.v"
vlog "./bsg_fifo_1rw_large.v"
vlog "./bsg_mem_1rw_sync.v"
vlog "./bsg_mem_1rw_sync_synth.v"

# Call vsim to invoke simulator
#     Make sure the last item on the line is the name of the
#     testbench module you want to execute.
vsim -voptargs="+acc" -t 1ps -lib work sorted_list_extracter_slave_parallelized_any_list_testbench

# Source the wave do file
#     This should be the file that sets up the signal window for
#     the module you are testing.
do sorted_list_extracter_slave_parallelized_any_list_wave.do

# Set the window types
view wave
view structure
view signals

# Run the simulation
run -all

# End
