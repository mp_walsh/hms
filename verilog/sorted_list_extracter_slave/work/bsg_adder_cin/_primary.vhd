library verilog;
use verilog.vl_types.all;
entity bsg_adder_cin is
    generic(
        width_p         : string  := "inv";
        harden_p        : integer := 1
    );
    port(
        a_i             : in     vl_logic_vector;
        b_i             : in     vl_logic_vector;
        cin_i           : in     vl_logic;
        o               : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of width_p : constant is 1;
    attribute mti_svvh_generic_type of harden_p : constant is 1;
end bsg_adder_cin;
