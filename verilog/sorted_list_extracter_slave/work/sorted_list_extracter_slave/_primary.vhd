library verilog;
use verilog.vl_types.all;
entity sorted_list_extracter_slave is
    generic(
        addr_size_p     : integer := -1
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        start_i         : in     vl_logic;
        max_sorted_i    : in     vl_logic_vector;
        where_to_start_i: in     vl_logic_vector;
        fifo_full_i     : in     vl_logic;
        read_data_i     : in     vl_logic_vector(65 downto 0);
        read_addr_o     : out    vl_logic_vector;
        fifo_en_o       : out    vl_logic;
        data_out_o      : out    vl_logic_vector(65 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
end sorted_list_extracter_slave;
