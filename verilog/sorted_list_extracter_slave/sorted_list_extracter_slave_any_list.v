//should update parameters
//This module gets a start ,where to start, and max sorted signal and uses that to access memory and send the sorted list out to the tree. Upon completed the list it will send finishes out forever. 
module sorted_list_extracter_slave_any_list #(parameter addr_size_p = -1)
(input  clk_i
,input reset_i

,input start_i
,input [addr_size_p - 1:0] max_sorted_i 
,input [addr_size_p - 1:0] where_to_start_i

,input fifo_full_i

,input [65:0] read_data_i

,output [addr_size_p - 1:0] read_addr_o

,output fifo_en_o
,output rd_en_o
,output [65:0] data_out_o
);
  //the counter logic
  logic [addr_size_p - 1:0] counter_r;
  logic done_r, all_done_r;
  wire [addr_size_p - 1:0] counter_plus_1 = counter_r + 1;
  
  //the counter
  logic start_pause_r, saw_full_r;
  always_ff @(posedge clk_i)
  begin
    if (reset_i) begin
      counter_r <= 15; //non zero starting case
	  done_r <= 1;
	  all_done_r <= 1;
    start_pause_r <= 0;
    saw_full_r <= 0;
	end
    else if (start_i) begin
      counter_r <= 0;
	  done_r <= 0;
	  all_done_r <= 0;
    start_pause_r <= 1;
    saw_full_r <= 0;
	end
    //else if (start_pause_r == 1) begin
         //counter_r <= counter_r;
	  //done_r <= done_r;
	  //all_done_r <= all_done_r;
    //start_pause_r <= 3;
    //end
    else if (fifo_full_i) begin
	  counter_r <= counter_r;
	  done_r <= done_r;
	  all_done_r <= all_done_r;
    start_pause_r <= 0;
    saw_full_r <= 1;
	end
	//else if (counter_r == max_sorted_i - 1) begin
	  //counter_r <= counter_plus_1;
	  //done_r <= 1;
	  //all_done_r <= 0;
	//end
	else if (counter_r >= max_sorted_i) begin
	  counter_r <= counter_r;
	  done_r <= 1;
	  all_done_r <= 0; //changed to make it produce infinite finishes
    start_pause_r <= 0;
    saw_full_r <= 0;
	end
	else if (~all_done_r) begin
	  counter_r <= counter_plus_1;
	  done_r <= 0;
	  all_done_r <= 0;
    start_pause_r <= 0;
    saw_full_r <= 0;
	end
	else begin
	  counter_r <= counter_r;
	  done_r <= done_r;
	  all_done_r <= all_done_r;
    start_pause_r <= 0;
    saw_full_r <= 0;
    end
  end

  //the adder to determine the current read address
  //wire [addr_size_p - 1:0] counter_r_minus_1 = counter_r - 1;
  bsg_adder_cin #(.width_p(addr_size_p)) addr_adder
  (.a_i(where_to_start_i)
  ,.b_i(counter_r)
  ,.cin_i(1'b0)
  ,.o(read_addr_o));
  
  //determine when the fifo should be enabled
  
  //buffer the done signal
  //logic done_r_r;
  //bsg_dff_reset #(.width_p(1)) finish_DFF
  //(.clock_i(clk_i)
  //,.data_i(done_r)
  //,.reset_i(reset_i)
  //,.data_o(done_r_r));
  
  assign fifo_en_o = ~ (fifo_full_i | all_done_r | start_pause_r);
  assign rd_en_o = 1;//~ fifo_full_i;
  
  //assign data out
  logic [65:0] data_out_n, data_out_r;
  bsg_mux #(.width_p(66), .els_p(2)) output_mux
  (.data_i({66'h30000000000000000, read_data_i})
  ,.sel_i(done_r)
  ,.data_o(data_out_n));
  
  
  always_ff @(posedge clk_i) begin
    if (reset_i) begin
         data_out_r <= 0; //non zero starting case
	end
    else if (saw_full_r)
       data_out_r <= data_out_r;
   else begin
     data_out_r <= data_out_n;
   end
  end

  logic [65:0] data_choice;
  always_comb begin
    if (saw_full_r)
      data_choice = data_out_r;
    else
      data_choice = data_out_n;
    end
  assign data_out_o = data_choice;
endmodule
/**
module sorted_list_extracter_slave_testbench();
  //addr size is set to 6
  logic  clk_i, reset_i, start_i, fifo_full_i, fifo_en_o;
  logic [6:0] max_sorted_i, where_to_start_i, read_addr_o;
  logic [65:0] read_data_i,  data_out_o;
  
  //fake memory
  logic [63:0][65:0] mem;
  //fill the memory
  integer i;
  initial begin
	for(i=0; i<64; i++) begin
	  mem[i][65:0] = i;
	end
  end
  
  //address control
  bsg_mux #(.width_p(66), .els_p(64)) mem_mux
  (.data_i(mem)
  ,.sel_i(read_addr_o)
  ,.data_o(read_data_i));
  
  //the module
  sorted_list_extracter_slave #(.addr_size_p(7)) dut
  (.clk_i
  ,.reset_i
  ,.start_i
  ,.max_sorted_i 
  ,.where_to_start_i
  ,.fifo_full_i
  ,.read_data_i
  ,.read_addr_o
  ,.fifo_en_o
  ,.data_out_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
	
  initial begin
	reset_i <= 1;	start_i <= 0; fifo_full_i <= 0; max_sorted_i <= 8; 
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	start_i <= 1;where_to_start_i <= 0;	@(posedge clk_i);
	start_i <= 0;					@(posedge clk_i);
	repeat(15) begin					@(posedge clk_i); end
	start_i <= 1;where_to_start_i <= 8;	@(posedge clk_i);
	start_i <= 0;					@(posedge clk_i);
	repeat(15) begin					@(posedge clk_i); end	
    start_i <= 1;where_to_start_i <= 16;	@(posedge clk_i);
	start_i <= 0;					@(posedge clk_i);
	repeat(4) begin					@(posedge clk_i); end
	fifo_full_i <= 1;               @(posedge clk_i);
	                                @(posedge clk_i);
	fifo_full_i <= 0;               @(posedge clk_i);
    repeat(11) begin					@(posedge clk_i); end
	start_i <= 1;where_to_start_i <= 0;max_sorted_i <= 64;	@(posedge clk_i);
	start_i <= 0;					@(posedge clk_i);
	repeat(70) begin					@(posedge clk_i); end
	start_i <= 1;where_to_start_i <= 0;max_sorted_i <= 1;	@(posedge clk_i);
	start_i <= 0;					@(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
				
		$stop(); // end the simulation
  end
  
endmodule**/
