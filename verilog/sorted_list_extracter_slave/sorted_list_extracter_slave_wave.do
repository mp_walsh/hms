onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /sorted_list_extracter_slave_testbench/clk_i
add wave -noupdate /sorted_list_extracter_slave_testbench/reset_i
add wave -noupdate /sorted_list_extracter_slave_testbench/start_i
add wave -noupdate /sorted_list_extracter_slave_testbench/fifo_full_i
add wave -noupdate /sorted_list_extracter_slave_testbench/fifo_en_o
add wave -noupdate -radix decimal /sorted_list_extracter_slave_testbench/max_sorted_i
add wave -noupdate -radix decimal /sorted_list_extracter_slave_testbench/where_to_start_i
add wave -noupdate -radix decimal /sorted_list_extracter_slave_testbench/read_addr_o
add wave -noupdate -radix decimal /sorted_list_extracter_slave_testbench/read_data_i
add wave -noupdate -radix decimal /sorted_list_extracter_slave_testbench/data_out_o
add wave -noupdate -radix decimal /sorted_list_extracter_slave_testbench/mem
add wave -noupdate /sorted_list_extracter_slave_testbench/dut/done_r
add wave -noupdate /sorted_list_extracter_slave_testbench/dut/all_done_r
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {55500 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {99046 ps} {118471 ps}
