onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/clk_i
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/reset_i
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/start_move_i
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/moved_o
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/length_of_list_i
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/max_sorted_i
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/data_i
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/data_o
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/v_o
add wave -noupdate /data_mover_from_memory_to_memory_parallel_tb/yumi_o
add wave -noupdate -group testbench_variables /data_mover_from_memory_to_memory_parallel_tb/x
add wave -noupdate -group testbench_variables /data_mover_from_memory_to_memory_parallel_tb/y
add wave -noupdate -group testbench_variables /data_mover_from_memory_to_memory_parallel_tb/t_value
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {2085 ps} {2191 ps}
