// Rough draft data_mover_from_memory_to_memory_parallel.v
// Moves data from outgoing memory back to input memory
// some hardcoding done to be working with an 8 stage merge tree
module data_mover_from_memory_to_memory_parallel #(parameter addr_size_p = -1)
(input clk_i
,input reset_i

,input start_move_i
,input [addr_size_p - 1:0] length_of_list_i
,input [addr_size_p - 1:0] max_sorted_i

,output rd_en_o // yumi_o back into outgoing fifo's 
,input [7:0][65:0] mem_data_i // the whole ouput of outgoing fifo's
//,output [addr_size_p - 1:0] rd_addr_o (no longer needed) 

,output wr_en_o
,output [65:0] mem_data_o
,output [addr_size_p - 1:0] wr_addr_o // address is a misnomer, this is a 
// one hot enable signal for the input fifo being written to 

,output moved_o
);

  //  ******************
  //  * High level FSM *
  //  ******************
  // Wait for Data -> Moving Data ->  Finished
  // Wait for data waits until start_move_i is pulsed
  // Moving data will write to input fifos
  // Finished lasts only a clock cycle and pulses moved_o 
  typedef enum {eWAIT_DATA, eMOVE_DATA, eFINISH} state_e; 
  state_e state_r, state_n;
  always_ff @(posedge clk_i) begin
    state_r <= (reset_i) ? eWAIT_DATA : state_n; 
  end 
  always_comb begin 
    unique case (state_r)
      eWAIT_DATA: begin 
        if(start_move_i)
          state_n = eMOVE_DATA;
        else 
          state_n = eWAIT_DATA;
      end // close eWAIT_DATA 
      eMOVE_DATA: begin
        if(write_counter_r >= max_sorted_r) // some signal to say 
          state_n = eFINISH;
        else 
          state_n = eMOVE_DATA;
      end // close eMOVE_DATA  
      eFINISH: begin 
        state_n = eWAIT_DATA; 
      end // close eFINISH
    endcase
  end // close state machine transitions logic 
  // Logic that gets set from state machine
  assign moved_o = (state_r == eFINISH); 
  
  //  ************************
  //  * Moving operation FSM *
  //  ************************
  // FSM that will convert p input from outgoing fifos back to serial data
  // for input fifos. 
  typedef enum {eWAIT_MOVE, eLATCH_DATA, eSERIAL_OUT, eINCREMENT_ADDRESS} state_e2; 
  state_e2 mov_r, mov_n; 
  always_ff @(posedge clk_i) begin 
    if(reset_i)
      mov_r <= eWAIT_MOVE;
    else 
      mov_r <= mov_n;  
  end 
  // ready_o of parrallel to serial unit 
  // used for one of the state transition edges 
  logic p_to_s_ready_n; 
  
  always_comb begin 
    unique case (mov_r)
      eWAIT_MOVE: begin 
        if(state_r == eMOVE_DATA && (p_to_s_ready_n))
          mov_n = eLATCH_DATA;
        else 
          mov_n = eWAIT_MOVE; 
      end // close ewait move 
      eLATCH_DATA: begin
        mov_n = eSERIAL_OUT; 
      end // close latch 
      eSERIAL_OUT: begin 
        if(write_counter_r == 8) // write to 8 different things 
        // there may be a bug here for off by one in terms of reading/writing 
          mov_n = eINCREMENT_ADDRESS;
        else 
          mov_n = eSERIAL_OUT;
      end // close serial out 
      eINCREMENT_ADDRESS: begin
        if(addr_r > addr_size_p-1)
          mov_n = eWAIT_MOVE;
        else 
          mov_n = eLATCH_DATA;
      end // close increment address 
    endcase
  end // close Moving FSM 
  
  // write counter, used to keep track of how many writes have been preformed
  logic[15:0] write_counter_r = 16'h00; 
  always_ff @(posedge clk_i) begin 
    if(reset_i | (mov_r == eWAIT_MOVE)) begin 
      write_counter_r <= 16'h00;
    end else if(mov_r == eSERIAL_OUT) begin 
      write_counter_r <= write_counter_r + 1; 
    end else
      write_counter_r <= write_counter_r; 
    end 
  end 
  // Register to hold max_sorted_r for whole moving process
  // uses an extra bit to detect if it was shifted over maximum value 
  logic [addr_size_p:0] max_sorted_r;
  always_ff @(posedge clk_i) begin
    else if((state_r == eWAIT_DATA) && (start_move_i)) begin // hold merge size for computation
      max_sorted_r <= max_sorted_i
    end else begin
      max_sorted_r <= max_sorted_r;
    end 
  end
  
  // 1 Hot address (to toggle v_i of 1 of the input fifo's)
  logic [addr_size_p-1:0] wr_addr_r; 
  always_ff @(posedge clk_i) begin 
    if(reset_i | (wr_addr_r == eWAIT_MOVE))
      wr_addr_r <= {(addr_size_p+1)(1'b0)}; 
    else if((mov_r == eINCREMENT_ADDRESS) && (wr_addr_r == 0))
      wr_addr_r <= {((addr_size_p+1)(1'b0)), 1'b1};
    else if((mov_r == eINCREMENT_ADDRESS))
      wr_addr_r <= (wr_addr_r << 1);
    else 
      wr_addr_r <= wr_addr_r;
  end
  
  // and each bit of wr_addr_0 with the serial state
  genvar x;
  generate 
    for(x = 0; x < addr_size_p; x = x + 1) begin 
      wr_addr_o[x] = wr_addr_r[x] & (mov_r == eSERIAL_OUT) & p_to_s_v_n;
    end 
  endgenerate 
  
  bsg_parallel_in_serial_out #(.width_p(66*8)) p_to_s 
                              ,.els_p(1)) p_to_s
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.valid_i((mov_r == eLATCH_DATA)) 
    ,.data_i(mem_data_i)
    ,.ready_o(p_to_s_ready_n) // this should not affect compuation at all
    // but is added to state machine logic to ensure robustness 
    ,.valid_o(p_to_s_v_n)
    ,.data_o(mem_data_o)
    ,.yumi_i(mov_r == eSERIAL_OUT) // always ready to output. May need to implement
                               // additional logic if reciving FIFO is full. 
    );
  assign rd_en_o = (mov_r == eLATCH_DATA); 
endmodule

/* TO:DO write parallel testbench
module data_mover_from_memory_to_memory_testbench #(parameter addr_size_p = 8);
  //addr size of 8
  logic clk_i, reset_i, start_move_i;
  logic [addr_size_p - 1:0] length_of_list_i;
  logic rd_en_o;
  logic [65:0] mem_data_i;
  logic [addr_size_p - 1:0] rd_addr_o;
  logic wr_en_o;
  logic [65:0] mem_data_o;
  logic [addr_size_p - 1:0] wr_addr_o;
  logic moved_o;
  
  //the module
  data_mover_from_memory_to_memory #(.addr_size_p(addr_size_p)) DUT
  (.clk_i
  ,.reset_i
  ,.start_move_i
  ,.length_of_list_i
  ,.rd_en_o
  ,.mem_data_i
  ,.rd_addr_o
  ,.wr_en_o
  ,.mem_data_o
  ,.wr_addr_o
  ,.moved_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  integer i;
	
  initial begin
	reset_i <= 1; start_move_i <= 0;
	length_of_list_i <= 16; mem_data_i <= 0; i <= 1;
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
  @(posedge clk_i);
	start_move_i <= 1; @(posedge clk_i);
	start_move_i <= 0; @(posedge clk_i);
  @(posedge clk_i);
	repeat(15) begin		mem_data_i <= mem_data_i + i;			@(posedge clk_i); end
	repeat(5) begin @(posedge clk_i); end
	
				
		$stop(); // end the simulation
  end
endmodule */ 
