library verilog;
use verilog.vl_types.all;
entity data_mover_from_memory_to_memory_testbench is
    generic(
        addr_size_p     : integer := 8
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
end data_mover_from_memory_to_memory_testbench;
