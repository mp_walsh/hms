library verilog;
use verilog.vl_types.all;
entity data_mover_from_memory_to_memory is
    generic(
        addr_size_p     : integer := -1
    );
    port(
        clk_i           : in     vl_logic;
        reset_i         : in     vl_logic;
        start_move_i    : in     vl_logic;
        length_of_list_i: in     vl_logic_vector;
        rd_en_o         : out    vl_logic;
        mem_data_i      : in     vl_logic_vector(65 downto 0);
        rd_addr_o       : out    vl_logic_vector;
        wr_en_o         : out    vl_logic;
        mem_data_o      : out    vl_logic_vector(65 downto 0);
        wr_addr_o       : out    vl_logic_vector;
        moved_o         : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of addr_size_p : constant is 1;
end data_mover_from_memory_to_memory;
