// Moves data from outgoing memory back to input memory
// some hardcoding done to be working with an 8 stage merge tree
module data_mover_from_memory_to_memory_parallel 
#(parameter addr_size_p = -1)
(input clk_i // control signals 
,input reset_i
,input start_move_i
,input [addr_size_p - 1:0] length_of_list_i
,input [addr_size_p - 1:0] max_sorted_i
// Data output outgoing memory Fifo's
,input [7:0][65:0] data_i
,output [65:0] data_o
,output [7:0]  yumi_o // yumi_i for each fifo 
// one hot (or all 0) output for writing to input fifo's 
,output [7:0]  v_o  
,output moved_o 
);

// very last cycle for moving data
logic last_move_n;
// moved_o reg
logic moved_r; 
assign moved_o = moved_r;
// counter for keeping track wich fifo to read from
logic[addr_size_p - 1:0] rd_fifo_cnt;
// counter for keepting track which fifo to write to 
logic[addr_size_p - 1:0] wr_fifo_cnt;
// counter for keeping track of elements written for sorted sequence
logic[addr_size_p -1:0] max_sorted_cnt; 
//counter for keeping track of total elements written
logic[addr_size_p - 1:0] els_wr_cnt;

// basic state machine for moving or not moving 
logic moving_r;

// Registers for holding onto length_of_list and max_sorted when computation runs
logic[addr_size_p -1:0] length_of_list_r;
logic[addr_size_p -1:0] max_sorted_r;


// Updates yumi going back to fifo based on rd_fifo_cnt 
bsg_decode_with_v #(.num_out_p(8)) decode_yumi
  (.i(rd_fifo_cnt)
  ,.v((moving_r))
  ,.o(yumi_o)
  );





assign last_move_n = (els_wr_cnt == (length_of_list_r - 1));
always @(posedge clk_i) begin 
  if(reset_i)
    moved_r <= 1'b0;
  else 
    moved_r <= last_move_n; 
end 


always_ff @(posedge clk_i) begin
  if(reset_i) 
    moving_r <= 1'b0; 
  else if(moving_r)
    moving_r <= ~last_move_n; 
  else
    moving_r <= start_move_i; 
end 


always_ff @(posedge clk_i) begin
  if(reset_i) begin 
    length_of_list_r <= {addr_size_p{1'b0}};
    max_sorted_r     <= {addr_size_p{1'b0}};
  end else if(~moving_r) begin
    length_of_list_r <= length_of_list_i;
    max_sorted_r     <= max_sorted_i; 
  end else begin
    length_of_list_r <= length_of_list_r;
    max_sorted_r     <= max_sorted_r;
  end 
end 

// counter reg update logic
always_ff @(posedge clk_i) begin
  if(reset_i | last_move_n) begin 
    rd_fifo_cnt    <= {addr_size_p{1'b0}};
    wr_fifo_cnt    <= {addr_size_p{1'b0}};
    max_sorted_cnt <= {addr_size_p{1'b0}};
    els_wr_cnt     <= {addr_size_p{1'b0}};
  end else if(moving_r) begin
    // elements update logic 
    els_wr_cnt <= els_wr_cnt + 1'b1; 
    // rd fifo update logic 
    if(rd_fifo_cnt == 7)
      rd_fifo_cnt <= {addr_size_p{1'b0}};
    else 
      rd_fifo_cnt <= rd_fifo_cnt + 1'b1; 
    // max sorted update logic 
    if(max_sorted_cnt == (max_sorted_r - 1)) begin 
      max_sorted_cnt <= {addr_size_p{1'b0}};
      wr_fifo_cnt    <= wr_fifo_cnt + 1; 
    end else begin 
      max_sorted_cnt <= max_sorted_cnt + 1;
      wr_fifo_cnt    <= wr_fifo_cnt; 
    end 
  end 
end // close counter update logic 



// Changing data_o to reflect current input read 
assign data_o = data_i[(rd_fifo_cnt)];

genvar x;
generate 
  for(x = 0; x < 8; x = x+ 1 ) begin
    assign v_o[x] = ((x == wr_fifo_cnt) && moving_r);
  end 
endgenerate   
endmodule

module data_mover_from_memory_to_memory_parallel_tb();
  parameter addr_size_p = 32; 
  logic clk_i, reset_i, start_move_i,  moved_o; 
  logic [addr_size_p - 1:0] length_of_list_i, max_sorted_i;
// Data output outgoing memory Fifo's
  logic  [7:0][66:0] data_i;
  logic [66:0] data_o; 
// one hot (or all 0) output for writing to input fifo's 
  logic [7:0]  v_o, yumi_o; 

  data_mover_from_memory_to_memory_parallel
#(.addr_size_p(addr_size_p)) DUT
  (.clk_i(clk_i) // control signals 
  ,.reset_i(reset_i)
  ,.start_move_i(start_move_i)
  ,.length_of_list_i(length_of_list_i)
  ,.max_sorted_i(max_sorted_i)
  ,.data_i(data_i)
  ,.data_o(data_o)
  ,.v_o(v_o)  
  ,.moved_o(moved_o)
  ,.yumi_o(yumi_o)
  );
  
  initial begin
    clk_i <= 1'b0;
    reset_i <= 1'b1; 
    start_move_i <= 1'b0;
    length_of_list_i <= 0;
    max_sorted_i <= 0;
    data_i <= 0; 
    forever #5 clk_i <= ~clk_i; 
  end 
  
  int x, y;
  logic[65:0] t_value = 66'h0_0000_0000_0000_0008;
  initial begin
  
    // Basic Case 1
    // max sorted of 8
    // list of length 64 
    @(posedge clk_i) reset_i <= 1'b0;
    start_move_i <= 1'b1; 
    max_sorted_i <= 32'h0000_0008; 
    length_of_list_i <= 32'h0000_0040; // 64 is total, so 8 in each fifo
    data_i[0] <= 66'h0_0000_0000_0000_0001;
    data_i[1] <= 66'h0_0000_0000_0000_0002;
    data_i[2] <= 66'h0_0000_0000_0000_0003;
    data_i[3] <= 66'h0_0000_0000_0000_0004;
    data_i[4] <= 66'h0_0000_0000_0000_0005;
    data_i[5] <= 66'h0_0000_0000_0000_0006;
    data_i[6] <= 66'h0_0000_0000_0000_0007;
    data_i[7] <= 66'h0_0000_0000_0000_0008;
    
    @(posedge clk_i); start_move_i <= 1'b0;// input data won't change from fifo for one clock cycle
    for(x = 0; x < 8; x = x+1) begin
      for(y = 0; y < 8; y = y +1) begin
        @(posedge clk_i);  
        t_value = t_value + 1'b1;
        data_i[y] = t_value; 
      end 
    end 
    // finish base case 1
    
    // Base Case 2
    // max sorted of 16
    // list of length 128
    @(posedge clk_i) reset_i <= 1'b0;
    start_move_i <= 1'b1; 
    max_sorted_i <= 32'h0000_0010; 
    length_of_list_i <= 32'h0000_0080; // 64 is total, so 8 in each fifo
    t_value = 66'h0_0000_0000_0000_0008;
    data_i[0] <= 66'h0_0000_0000_0000_0001;
    data_i[1] <= 66'h0_0000_0000_0000_0002;
    data_i[2] <= 66'h0_0000_0000_0000_0003;
    data_i[3] <= 66'h0_0000_0000_0000_0004;
    data_i[4] <= 66'h0_0000_0000_0000_0005;
    data_i[5] <= 66'h0_0000_0000_0000_0006;
    data_i[6] <= 66'h0_0000_0000_0000_0007;
    data_i[7] <= 66'h0_0000_0000_0000_0008;
    
    @(posedge clk_i); start_move_i <= 1'b0;// input data won't change from fifo for one clock cycle
    for(x = 0; x < 16; x = x+1) begin
      for(y = 0; y < 8; y = y +1) begin
        @(posedge clk_i);  
        t_value = t_value + 1'b1;
        data_i[y] = t_value; 
      end 
    end 
    
    
    // Case 3 
    // max sorted of 8
    // list of length 16 
    @(posedge clk_i) reset_i <= 1'b0;
    start_move_i <= 1'b1; 
    max_sorted_i <= 32'h0000_0008; 
    length_of_list_i <= 32'h0000_0010; // 64 is total, so 8 in each fifo
    t_value = 66'h0_0000_0000_0000_0008;
    data_i[0] <= 66'h0_0000_0000_0000_0001;  
    data_i[1] <= 66'h0_0000_0000_0000_0002;  
    data_i[2] <= 66'h0_0000_0000_0000_0003;  
    data_i[3] <= 66'h0_0000_0000_0000_0004;  
    data_i[4] <= 66'h0_0000_0000_0000_0005;  
    data_i[5] <= 66'h0_0000_0000_0000_0006;  
    data_i[6] <= 66'h0_0000_0000_0000_0007;  
    data_i[7] <= 66'h0_0000_0000_0000_0008;  
    
    @(posedge clk_i); start_move_i <= 1'b0;// input data won't change from fifo for one clock cycle
    for(x = 0; x < 2; x = x+1) begin
      for(y = 0; y < 8; y = y +1) begin
        @(posedge clk_i);  
        t_value = t_value + 1'b1;
        data_i[y] = t_value; 
      end 
    end 
    repeat (5) @(posedge clk_i); 
    
    
    $stop; 
  end 
endmodule 

