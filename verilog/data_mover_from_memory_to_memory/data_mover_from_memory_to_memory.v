module data_mover_from_memory_to_memory #(parameter addr_size_p = -1)
(input clk_i
,input reset_i

,input start_move_i
,input [addr_size_p - 1:0] length_of_list_i

,output rd_en_o
,input [65:0] mem_data_i
,output [addr_size_p - 1:0] rd_addr_o

,output wr_en_o
,output [65:0] mem_data_o
,output [addr_size_p - 1:0] wr_addr_o

,output moved_o
);

  //instantiate some logic
  logic [addr_size_p - 1:0] counter_r, counter_r_delayed;
  logic begin_moving_r;
  
  //determine when a move has been completed
  wire completed_move = (length_of_list_i - 1 == counter_r);
  
  //determine if the counter has been the same for two cycles
  wire both_match = (counter_r == counter_r_delayed);
  
  //the adder to update counter
  wire [addr_size_p - 1:0] next_point = counter_r + 1;
  
  //the counter
  always_ff @(posedge clk_i)
  begin
    if (reset_i | start_move_i) begin //reset case
      counter_r <= 0;
	  //counter_r_delayed <= 0;
	end
	else if (~completed_move & begin_moving_r) begin //looping case
      counter_r <= next_point;
	end
	else begin //waiting case
	  counter_r <= counter_r;
	end
  end
  
  //buffered counter
  always_ff @(posedge clk_i) begin
    if (reset_i) begin
      counter_r_delayed <= 0;
	end
	else begin
	  counter_r_delayed <= counter_r;
	end
  end
  
  //logic to determine when we should be counting
  always_ff @(posedge clk_i)
  begin
    if (reset_i) begin //reset case
      begin_moving_r <= 0;
	end
	else if (start_move_i) begin //start case
      begin_moving_r <= 1;
	end
	else if (completed_move) begin
	  begin_moving_r <= 0;
	end
	else begin //waiting case
	  begin_moving_r <= begin_moving_r;
	end
  end
  
  //buffer addresses, enables, and moved signal
  logic wr_en_r;
  logic [addr_size_p - 1:0] wr_addr_r;
  logic moved_r, moved_r_r;
  
  always_ff @(posedge clk_i) begin
    wr_en_r <= begin_moving_r;
    wr_addr_r <= counter_r;
    moved_r <= completed_move & ~both_match;
    moved_r_r <= moved_r;
  end
  
  //assign outputs
  assign wr_en_o = wr_en_r;
  assign rd_en_o = begin_moving_r;
  assign wr_addr_o = wr_addr_r;
  assign rd_addr_o = counter_r;
  assign mem_data_o = mem_data_i;
  assign moved_o = moved_r_r; //may need to buffer

endmodule

module data_mover_from_memory_to_memory_testbench #(parameter addr_size_p = 8);
  //addr size of 8
  logic clk_i, reset_i, start_move_i;
  logic [addr_size_p - 1:0] length_of_list_i;
  logic rd_en_o;
  logic [65:0] mem_data_i;
  logic [addr_size_p - 1:0] rd_addr_o;
  logic wr_en_o;
  logic [65:0] mem_data_o;
  logic [addr_size_p - 1:0] wr_addr_o;
  logic moved_o;
  
  //the module
  data_mover_from_memory_to_memory #(.addr_size_p(addr_size_p)) DUT
  (.clk_i
  ,.reset_i
  ,.start_move_i
  ,.length_of_list_i
  ,.rd_en_o
  ,.mem_data_i
  ,.rd_addr_o
  ,.wr_en_o
  ,.mem_data_o
  ,.wr_addr_o
  ,.moved_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  integer i;
	
  initial begin
	reset_i <= 1; start_move_i <= 0;
	length_of_list_i <= 16; mem_data_i <= 0; i <= 1;
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
  @(posedge clk_i);
	start_move_i <= 1; @(posedge clk_i);
	start_move_i <= 0; @(posedge clk_i);
  @(posedge clk_i);
	repeat(15) begin		mem_data_i <= mem_data_i + i;			@(posedge clk_i); end
	repeat(5) begin @(posedge clk_i); end
	
				
		$stop(); // end the simulation
  end
endmodule
