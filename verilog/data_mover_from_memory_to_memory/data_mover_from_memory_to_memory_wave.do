onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /data_mover_from_memory_to_memory_testbench/clk_i
add wave -noupdate /data_mover_from_memory_to_memory_testbench/reset_i
add wave -noupdate /data_mover_from_memory_to_memory_testbench/start_move_i
add wave -noupdate -radix unsigned /data_mover_from_memory_to_memory_testbench/length_of_list_i
add wave -noupdate /data_mover_from_memory_to_memory_testbench/rd_en_o
add wave -noupdate -radix unsigned /data_mover_from_memory_to_memory_testbench/mem_data_i
add wave -noupdate -radix unsigned /data_mover_from_memory_to_memory_testbench/rd_addr_o
add wave -noupdate /data_mover_from_memory_to_memory_testbench/wr_en_o
add wave -noupdate -radix unsigned /data_mover_from_memory_to_memory_testbench/mem_data_o
add wave -noupdate -radix unsigned /data_mover_from_memory_to_memory_testbench/wr_addr_o
add wave -noupdate /data_mover_from_memory_to_memory_testbench/moved_o
add wave -noupdate /data_mover_from_memory_to_memory_testbench/i
add wave -noupdate -radix unsigned /data_mover_from_memory_to_memory_testbench/DUT/counter_r_delayed
add wave -noupdate /data_mover_from_memory_to_memory_testbench/DUT/both_match
add wave -noupdate /data_mover_from_memory_to_memory_testbench/DUT/completed_move
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {17443 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {24675 ps}
