# Create work library
vlib work

# Compile Verilog
#     All Verilog files that are part of this design should have
#     their own "vlog" line below.

vlog "../sorted_list_extracter_slave/bsg_dff_reset.v"
vlog "../merge_units/bsg_defines.v"
vlog "../merge_units/bsg_circular_ptr.v"


vlog "../sorted_list_extracter_slave/bsg_mux.v"
vlog "../sorted_list_extracter_slave/bsg_adder_cin.v"
vlog "../sorted_list_extracter_slave/sorted_list_extracter_slave.v"

vlog "../sub_iter_manager/master_counter.v"
vlog "../sub_iter_manager/sub_iter_manager.v"

vlog "../FSB_control/FSB_control_simple.v"

vlog "../data_mover_from_tree_to_memory/data_mover_from_tree_to_memory.v"

vlog "../merge_units/hms_pkg.v"


vlog "../merge_units/bsg_mem_1r1w_synth.v"
vlog "../merge_units/bsg_mem_1r1w.v"
vlog "../merge_units/bsg_fifo_tracker.v"
vlog "../merge_units/bsg_fifo_1r1w_small.v"
vlog "../merge_units/merge_network_stage.v"
vlog "../merge_units/merge_network.v"
vlog "../merge_units/merge_logic.v"

vlog "./bsg_mem_multiport.v"
vlog "./bsg_serial_in_parallel_out.v"
vlog "./hms_simple.v"


# Call vsim to invoke simulator
#     Make sure the last item on the line is the name of the
#     testbench module you want to execute.
vsim -voptargs="+acc" -t 1ps -lib work hms_simple_testbench

# Source the wave do file
#     This should be the file that sets up the signal window for
#     the module you are testing.
do hms_simple_wave.do

# Set the window types
view wave
view structure
view signals

# Run the simulation
run -all

# End
