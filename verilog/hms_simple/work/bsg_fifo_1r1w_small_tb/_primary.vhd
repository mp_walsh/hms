library verilog;
use verilog.vl_types.all;
entity bsg_fifo_1r1w_small_tb is
    generic(
        e_height_p      : integer := 4;
        els_p           : integer := 16
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of e_height_p : constant is 1;
    attribute mti_svvh_generic_type of els_p : constant is 1;
end bsg_fifo_1r1w_small_tb;
