library verilog;
use verilog.vl_types.all;
entity bsg_dff_reset is
    generic(
        width_p         : integer := -1;
        harden_p        : integer := 0
    );
    port(
        clock_i         : in     vl_logic;
        data_i          : in     vl_logic_vector;
        reset_i         : in     vl_logic;
        data_o          : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of width_p : constant is 1;
    attribute mti_svvh_generic_type of harden_p : constant is 1;
end bsg_dff_reset;
