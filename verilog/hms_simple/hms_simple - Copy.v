//all the modules put together
module hms_simple 
import hms_pkg::*;
#(parameter memory_size_p = -1, addr_size_lp = $clog2(memory_size_p+1), addr_size_mem_lp = addr_size_lp - 1, ring_width_p = -1, id_p="inv", e_height_p = -1)
(input  clk_i
,input reset_i
,input en_i

,input v_i
,input [ring_width_p - 1:0] data_i
,output ready_o

,output                    v_o
,output [ring_width_p-1:0] data_o
,input                     yumi_i
);

  //the FSB control manager
  logic done, list_done, wr_en_incoming;
  logic [addr_size_lp - 1:0] wr_addr_incoming;
  logic [65:0] wr_data_incoming;
  logic [31:0] length_of_list, max_sorted;
  
  logic [65:0] read_data_output;
  logic [addr_size_lp - 1:0] rd_addr_output;
  logic rd_en_output;
  FSB_control #(.addr_size_p(addr_size_lp), .ring_width_p(ring_width_p)) in_and_out_control
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.finish_i(done)
  ,.input_data_i(data_i)
  ,.v_i(v_i)
  ,.ready_o(ready_o)
  ,.v_o(v_o)
  ,.data_o(data_o)
  ,.yumi_i(yumi_i)
  ,.list_done_o(list_done)
  ,.wr_en_o(wr_en_incoming)
  ,.wr_addr_o(wr_addr_incoming)
  ,.wr_data_o(wr_data_incoming)
  ,.read_data_i(read_data_output)
  ,.rd_addr_o(rd_addr_output)
  ,.rd_en_o(rd_en_output)
  ,.length_of_list_o(length_of_list)
  ,.max_sorted_o(max_sorted));
  
  //entrance memory bank
  logic [e_height_p - 1:0][addr_size_lp - 1:0] r_addr_incoming;
  logic [e_height_p - 1:0][6:0] r_addr_incoming_between;
  assign r_addr_incoming_between[0] = r_addr_incoming[0][6:0];
  assign r_addr_incoming_between[1] = r_addr_incoming[1][6:0];
  logic [e_height_p - 1:0][65:0] read_data_incoming;
  bsg_mem_multiport #(.width_p(66), .els_p(memory_size_p), .read_write_same_addr_p(1), .write_write_same_addr_p(0)
                           ,.read_ports_p(e_height_p)
                           ,.write_ports_p(1)) incoming_memory
   (.w_clk_i(clk_i)
   ,.w_reset_i(reset_i)
   ,.w_v_i(wr_en_incoming)
   ,.w_addr_i(wr_addr_incoming[6:0])
   ,.w_data_i(wr_data_incoming)
   ,.r_v_i(2'b11)
   ,.r_addr_i(r_addr_incoming_between)
   ,.r_data_o(read_data_incoming));
  
  
  //single loop over manager
  logic finish, reset_tree;
  logic [e_height_p - 1:0] starts;
  logic [e_height_p - 1:0][addr_size_lp - 1:0] whereToStarts;
  logic [addr_size_lp - 1:0] next_sorted_length;
  sub_iter_manager #(.addr_size_p(addr_size_lp), .e_height_p(2)) sub_manager
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.finish_i(finish)
  ,.full_length_i(length_of_list[addr_size_lp - 1:0])
  ,.max_sorted_i(max_sorted[addr_size_lp - 1:0])
  ,.new_iteration_i(list_done)
  ,.starts_o(starts)
  ,.whereToStarts_o(whereToStarts)
  ,.done_o(done)
  ,.reset_tree_o(reset_tree)
  ,.next_sorted_length_o(next_sorted_length));
  
  genvar i;
  //generate a collection of e extractors
  logic [e_height_p - 1:0] fifo_full;
  logic [e_height_p - 1:0] fifo_en;
  logic [e_height_p - 1:0][65:0] data_to_fifo;
  generate
	for(i=0; i<e_height_p; i++) begin : eachExtractor
	  sorted_list_extracter_slave #(.addr_size_p(addr_size_lp)) an_extractor
      (.clk_i(clk_i)
      ,.reset_i(reset_i)
      ,.start_i(starts[i])
      ,.max_sorted_i(max_sorted[addr_size_lp - 1:0])
      ,.where_to_start_i(whereToStarts[i][addr_size_lp - 1:0])
      ,.fifo_full_i(~fifo_full[i])//change to not full
      ,.read_data_i(read_data_incoming[i][65:0])
      ,.read_addr_o(r_addr_incoming[i][addr_size_lp - 1:0])
      ,.fifo_en_o(fifo_en[i])
      ,.data_out_o(data_to_fifo[i][65:0]));
	end
  endgenerate
  
  genvar j;
  //generate a collection of e couplers
  logic [1:0][e_height_p - 1:0][65:0] paired_data;
  logic [e_height_p - 1:0][e_height_p - 1:0] paired_data_valid;
  logic [e_height_p - 1:0][e_height_p - 1:0] tree_ready;
  logic [e_height_p - 1:0] tree_ready_between;
  generate
	for(j=0; j<e_height_p; j++) begin : eachCoupler
	  bsg_serial_in_parallel_out #(.width_p(66), .els_p(2), .out_els_p(e_height_p)) a_coupler
     (.clk_i(clk_i)
     ,.reset_i(reset_i | reset_tree)
     ,.valid_i(fifo_en[j])
     ,.data_i(data_to_fifo[j][65:0])
     ,.ready_o(fifo_full[j])
     ,.valid_o(paired_data_valid[j])
	 // if we are just trying to get the parallel out, shouldn't it just be .data_o(paired_data[j])?
     ,.data_o(paired_data[j])//[e_height_p - 1:0][65:0])
     ,.yumi_cnt_i(tree_ready[j]));
	end
  endgenerate
  
  //join the tree_readys together
  assign tree_ready[0][1] = (~tree_ready_between[0])&(paired_data_valid[0][0] & paired_data_valid[0][1]);
  assign tree_ready[0][0] = 1'b0;
  assign tree_ready[1][0] = 1'b0;
  assign tree_ready[1][1] = (~tree_ready_between[1])&(paired_data_valid[1][0] & paired_data_valid[1][1]);
  
  //join valid outs from couplers together
  logic [e_height_p - 1:0] paired_data_valid_between;
  assign paired_data_valid_between[0] = paired_data_valid[0][1] & paired_data_valid[0][0];
  assign paired_data_valid_between[1] = paired_data_valid[1][1] & paired_data_valid[1][0];
  
  // convert buses to struct form 
  e_s [e_height_p-1:0] a_data_n;
  e_s [e_height_p-1:0] b_data_n; 
  genvar x;
  for(x = 0; x < e_height_p; x = x+1) begin 
	assign a_data_n[x].key = paired_data[0][x][65:32];
	assign a_data_n[x].data = paired_data[0][x][31:0];
	assign b_data_n[x].key = paired_data[1][x][65:32];
	assign b_data_n[x].data = paired_data[1][x][31:0];
  end
  
  e_s [e_height_p-1:0] data_out_of_tree_n;
  
  //the merge logic when it is ready
  //should be connected to paired_data, paired_data_valid, tree_ready, reset_tree, and data_from_tree
  merge_logic #(.e_height_p(e_height_p)) the_merge_logic
  (.clk_i(clk_i)
  ,.reset_i(reset_i | reset_tree)
  ,.full_i(1'b0)
  ,.a_data_i(a_data_n)
  ,.b_data_i(b_data_n)
  ,.a_v_i(paired_data_valid_between[0])
  ,.b_v_i(paired_data_valid_between[1])
  ,.b_full_o(tree_ready_between[1])
  ,.a_full_o(tree_ready_between[0])
  ,.data_o(data_out_of_tree_n));
  
  
  
  //data mover from tree to the output memory
  logic [e_height_p - 1:0][65:0] data_from_tree;
  logic [e_height_p - 1:0] wr_en_output;
  logic [e_height_p - 1:0][65:0] wr_data_output;
  logic [e_height_p - 1:0][addr_size_lp - 1:0] wr_addr_output;
  
  genvar y;
  for(y = 0; y < e_height_p; y = y+1) begin 
	assign data_from_tree[y][31:0] = data_out_of_tree_n[y].data;
    assign data_from_tree[y][65:32] = data_out_of_tree_n[y].key;
  end
  
  data_mover_from_tree_to_memory #(.addr_size_p(addr_size_lp), .e_height_p(2)) data_mover
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.start_i(starts[0])
  ,.where_to_start_i(whereToStarts[0])
  ,.current_length_i(next_sorted_length)
  ,.data_i(data_from_tree)
  ,.wr_en_o(wr_en_output)
  ,.data_o(wr_data_output)
  ,.wr_addr_o(wr_addr_output)
  ,.finish_o(finish));
  
  //exit memory bank
  //logic [e_height_p - 1:0][addr_size_lp - 1:0] r_addr_incoming;
  //logic [e_height_p - 1:0][65:0] read_data_incoming;
  logic [e_height_p - 1:0][6:0] wr_addr_output_between;
  assign wr_addr_output_between[0] = wr_addr_output[0][6:0];
  assign wr_addr_output_between[1] = wr_addr_output[1][6:0];
  bsg_mem_multiport #(.width_p(66), .els_p(memory_size_p), .read_write_same_addr_p(1), .write_write_same_addr_p(0)
                           ,.read_ports_p(1)
                           ,.write_ports_p(e_height_p)) outgoing_memory
   (.w_clk_i(clk_i)
   ,.w_reset_i(reset_i)
   ,.w_v_i(wr_en_output)
   ,.w_addr_i(wr_addr_output_between)
   ,.w_data_i(wr_data_output)
   ,.r_v_i(rd_en_output)
   ,.r_addr_i(rd_addr_output[6:0])
   ,.r_data_o(read_data_output));
  
endmodule


module hms_simple_testbench #(parameter memory_size_p = 128, ring_width_p = 75, id_p = 0, e_height_p = 2);
  logic  clk_i, reset_i, en_i, v_i;
  logic [ring_width_p - 1:0] data_i;
  logic ready_o, v_o;
  logic [ring_width_p-1:0] data_o;
  logic yumi_i;
  
  //the module
  hms_simple #(.memory_size_p(memory_size_p), .ring_width_p(ring_width_p), .id_p(id_p), .e_height_p(e_height_p)) DUT
  (.clk_i
  ,.reset_i
  ,.en_i
  ,.v_i
  ,.data_i
  ,.ready_o
  ,.v_o
  ,.data_o
  ,.yumi_i);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
  
  initial begin
	reset_i <= 0; en_i <= 1;
	data_i <= 0;
	v_i <= 0;
	yumi_i <= 0;
									@(posedge clk_i);
	reset_i <= 1;					@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	                                @(posedge clk_i);
	v_i <= 1; data_i[74:64] <= 11'b0000000__0000;
	data_i[63:32] = 32'b00000000000000000000000000000100;
	data_i[31:0] = 32'b00000000000000000000000000010000 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
			  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000001_00000000000000000000000000000001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000110_00000000000000000000000000000110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001001_00000000000000000000000000001001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001010_00000000000000000000000000001010 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000010_00000000000000000000000000000010 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000100_00000000000000000000000000000100 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000101_00000000000000000000000000000101 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001000_00000000000000000000000000001000 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000011_00000000000000000000000000000011 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000110_00000000000000000000000000000110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000111_00000000000000000000000000000111 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001011_00000000000000000000000000001011 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);

	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000000101_00000000000000000000000000000101 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001001_00000000000000000000000000001001 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001100_00000000000000000000000000001100 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);		  
	v_i <= 1; data_i <= 75'b0000000__0001_00000000000000000000000000001110_00000000000000000000000000001110 ;  @(posedge clk_i);
	v_i <= 0; @(posedge clk_i);
	          @(posedge clk_i);			  
			  
	
	repeat(200) begin					@(posedge clk_i); end
				
	$stop(); // end the simulation
  end


endmodule