onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /sub_iter_manager_testbench/clk_i
add wave -noupdate /sub_iter_manager_testbench/reset_i
add wave -noupdate /sub_iter_manager_testbench/finish_i
add wave -noupdate /sub_iter_manager_testbench/new_iteration_i
add wave -noupdate /sub_iter_manager_testbench/done_o
add wave -noupdate /sub_iter_manager_testbench/full_length_i
add wave -noupdate /sub_iter_manager_testbench/max_sorted_i
add wave -noupdate /sub_iter_manager_testbench/starts_o
add wave -noupdate /sub_iter_manager_testbench/whereToStarts_o
add wave -noupdate /sub_iter_manager_testbench/reset_tree_o
add wave -noupdate /sub_iter_manager_testbench/next_sorted_length_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {106592 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {94302 ps} {157172 ps}
