//This module combines master counter and some shifters/adders to send out the correct values for where to start and list lengths. 
module sub_iter_manager_any_list #(parameter addr_size_p = -1, e_height_p = 2)
(input clk_i
,input reset_i

,input finish_i

,input [addr_size_p - 1:0] full_length_i
,input [addr_size_p - 1:0] max_sorted_i
,input new_iteration_i

,output [e_height_p - 1:0] starts_o
,output [e_height_p - 1:0][addr_size_p - 1:0] whereToStarts_o
,output done_o
,output [e_height_p - 1:0][addr_size_p - 1:0] lengths_o
,output reset_tree_o
,output [addr_size_p - 1:0] next_sorted_length_o
,output [addr_size_p - 1:0] next_max_sorted_length_o
);

//the brain
logic [addr_size_p - 1:0] counter_value_r, next_max_sorted;
logic [e_height_p - 1:0] starts_r;
logic [e_height_p - 1:0][addr_size_p - 1:0] lengths;
logic done_r;

master_counter_any_list #(.addr_size_p(addr_size_p), .e_height_p(e_height_p)) counter
(.clk_i(clk_i)
,.reset_i(reset_i)
,.finish_i(finish_i)
,.full_length_i(full_length_i)
,.max_sorted_i(max_sorted_i)
,.new_iteration_i(new_iteration_i)

,.counter_o(counter_value_r)
,.starts_o(starts_r)
,.lengths_o(lengths)
,.done_o(done_r)
,.next_max_sorted_o(next_max_sorted)
,.reset_tree_o(reset_tree_o)
);

logic [addr_size_p - 1:0] next_sorted_length;
assign next_sorted_length = lengths[0] + lengths[1]; //need to make parameter later

//the address generators
assign whereToStarts_o[0][addr_size_p - 1:0] = counter_value_r;
assign whereToStarts_o[1][addr_size_p - 1:0] = counter_value_r + max_sorted_i;
//assign whereToStarts_o[2][addr_size_p - 1:0] = counter_value_r + (max_sorted_i << 1);
//assign whereToStarts_o[3][addr_size_p - 1:0] = counter_value_r + (max_sorted_i << 1) + max_sorted_i;

assign starts_o = starts_r;
assign done_o = done_r;
assign next_sorted_length_o = next_sorted_length;
assign next_max_sorted_length_o = next_max_sorted;
assign lengths_o = lengths;

endmodule
/**
module sub_iter_manager_testbench();
  //addr size is set to 8, e = 2
  logic  clk_i, reset_i, finish_i, new_iteration_i, done_o, reset_tree_o;
  logic [7:0] full_length_i, max_sorted_i, next_sorted_length_o;
  logic [1:0] starts_o;
  logic [1:0][7:0] whereToStarts_o;
  
  sub_iter_manager #(.addr_size_p(8)) DUT
  (.clk_i
  ,.reset_i
  ,.finish_i
  ,.full_length_i
  ,.max_sorted_i
  ,.new_iteration_i
  ,.whereToStarts_o
  ,.starts_o
  ,.done_o
  ,.reset_tree_o
  ,.next_sorted_length_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
	
  initial begin
	reset_i <= 1; finish_i <= 0; new_iteration_i <= 0; 
	full_length_i <= 128; max_sorted_i <= 2; 
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	new_iteration_i <= 1; @(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(32) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
	new_iteration_i <= 1; max_sorted_i <= 8;@(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(8) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
	
	new_iteration_i <= 1; max_sorted_i <= 1;@(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(64) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
	
				
		$stop(); // end the simulation
  end
  
endmodule**/
