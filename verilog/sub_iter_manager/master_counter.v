
module master_counter #(parameter addr_size_p = -1, e_height_p = -1, e_log = $clog2(e_height_p))
(input  clk_i
,input reset_i

,input finish_i

,input [addr_size_p - 1:0] full_length_i
,input [addr_size_p - 1:0] max_sorted_i
,input new_iteration_i

,output [addr_size_p - 1:0] counter_o
,output [e_height_p - 1:0] starts_o
,output done_o
,output reset_tree_o
);
  //the counter logic
  logic list_done_r;
  logic [e_height_p - 1:0] starts_r;
  logic [addr_size_p - 1:0] times_to_count_r;
  logic [addr_size_p - 1:0] add_value;
  logic [addr_size_p - 1:0] counter_r;
  
  //values to use for the adder
  //need to update e_height parameter later, make a clog variable for here
   assign add_value = max_sorted_i << e_log;
   
  //the adder to update counter
  wire [addr_size_p - 1:0] next_point = counter_o + add_value;
  
  //determine if the next point is equal to or less than the full length
  wire next = finish_i & (next_point < full_length_i);
  wire done = finish_i & (next_point == full_length_i);
  
  logic reset_tree_r;
  //the counter
  always_ff @(posedge clk_i)
  begin
    if (reset_i) begin //reset case
      counter_r <= 0;
	  starts_r <= {e_height_p{1'b0}};// or 0
	  reset_tree_r <= 0;
	end
	else if (new_iteration_i) begin //reset case
      counter_r <= 0;
	  starts_r <= {e_height_p{1'b1}};// or ~0
	  reset_tree_r <= 1;
	end
	else if (done) begin //got to the end, stop until told to start again
	  counter_r <= counter_r;
	  starts_r <= {e_height_p{1'b0}};// or 0
	  reset_tree_r <= 0;
	end
	else if (next) begin //keep looping
	  counter_r <= next_point;
	  starts_r <= {e_height_p{1'b1}};// or ~0
	  reset_tree_r <= 1;
	end
	else begin //waiting case
	  counter_r <= counter_r;
	  starts_r <= {e_height_p{1'b0}};// or ~0
	  reset_tree_r <= 0;
	end
  end
  
  assign counter_o = counter_r;
  assign starts_o = starts_r;
  assign done_o = done;
  assign reset_tree_o = reset_tree_r;

endmodule

module master_counter_testbench();
  //addr size is set to 6, e = 4
  logic  clk_i, reset_i, finish_i, new_iteration_i, done_o, reset_tree_o;
  logic [7:0] counter_o, full_length_i, max_sorted_i;
  logic [3:0] starts_o;
  
  master_counter #(.addr_size_p(8), .e_height_p(4)) DUT
  (.clk_i
  ,.reset_i
  ,.finish_i
  ,.full_length_i
  ,.max_sorted_i
  ,.new_iteration_i
  ,.counter_o
  ,.starts_o
  ,.done_o
  ,.reset_tree_o);
  
  // Set up the clock
  parameter ClockDelay = 1000;
  initial begin ;
	clk_i <= 0;
	forever #(ClockDelay/2) clk_i <= ~clk_i;
  end
	
  initial begin
	reset_i <= 1; finish_i <= 0; new_iteration_i <= 0; 
	full_length_i <= 128; max_sorted_i <= 2; 
									@(posedge clk_i);
	reset_i <= 0; 					@(posedge clk_i);
	new_iteration_i <= 1; @(posedge clk_i);
	new_iteration_i <= 0; @(posedge clk_i);
	repeat(5) begin					@(posedge clk_i); end
	repeat(16) begin finish_i <= 1;    @(posedge clk_i);
	 finish_i <= 0;    @(posedge clk_i);
	repeat(5) begin                 @(posedge clk_i); end end
	
				
		$stop(); // end the simulation
  end
  
endmodule
  